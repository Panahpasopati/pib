@extends('layouts.pib')
@section('content')
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="row">
                <h2 class="page-title">Invoice Preview</h2>
                <div>
                    <div class="col-md-8">
                        <table class="table table-responsive">
                            <tbody>
                            <tr><td>Invoice No </td><td>: {{ $data->invoice_no }}</td></tr>
                            <tr><td>Date Issued </td><td>: {{ date('d M Y',strtotime($data->created_at)) }}</td></tr>
                            </tbody>
                        </table>
                        <br>
                        <table class="table table-responsive">
                            <tbody>
                            <tr><td style="font-weight: bold;">@foreach($data->Quotes->Insurers as $insurer)  {{ $insurer->Insurer->insured }} <br>@endforeach</td></tr>
                            <tr><td>Address </td><td> : {{ $data->Quotes->address }}</td></tr>
                            <tr><td>Attention </td><td> : </td></tr>
                            <tr><td>c/o </td><td> : </td></tr>
                            </tbody>
                        </table>
                        <br>
                        @php
                        $total = ($data->is_prorata) ? $data->Quotes->PremiumAmount->amount_prorata : $data->Quotes->PremiumAmount->amount_total
                        @endphp
                        @php
                        $date1=date_create($data->period_from);
                        $date2=date_create($data->period_to);
                        $diff=date_diff($date1,$date2);
                        @endphp
                        <table class="table table-responsive">
                            <tbody>
                            <tr><td>Assured </td><td  style="font-weight: bold;"> : @foreach($data->Quotes->Insurers as $insurer) {{ $insurer->Insurer->insured }} <br>@endforeach</td></tr>
                            <tr><td>Insurer</td><td> : {{ $data->Quotes->Security->assured_name }}   </td></tr>
                            <tr><td>Class</td><td> : </td></tr>
                            <tr><td>Period of Insurance</td><td> :  {{   date('d M Y',strtotime($data->period_from)).' to '.date('d M Y',strtotime($data->period_to)) }}</td></tr>
                            <tr><td>Days</td><td> :  {{ $diff->format('%a')  }}</td></tr>
                            <tr><td>Amount Due </td><td> : {{ $data->Quotes->currency }} {{ number_format($total,2) }}</td></tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-8">
                        <table style="margin-left: 20px;" class="table table-condensed table-primary table-bordered">
                            <thead>
                            <tr style="background: #ccc;">
                                <th width="30%">Installment</th>
                                <th width="30%">Due</th>
                                <th width="40%">Premium</th>
                            </tr>
                            </thead>
                            <tbody id="itemlist">
                            @foreach($data->Installment as $i => $val)
                                <tr>
                                    <td>
                                        {{ $val->installment_no }}
                                    </td>
                                    <td>
                                        {{ $val->due_date }}
                                    </td>
                                    <td>
                                        {{ $data->Quotes->currency .' '. number_format($val->amount,2) }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <style>
                        table.table-scroll>tbody{
                            display: block;max-height: 300px;overflow-y: scroll;
                        }
                        table.table-scroll>thead{display: table;width: 100%;table-layout: fixed;}
                        table.table-scroll>tfoot>tr{display: table;width: 100%;table-layout: fixed;}
                        table.table-scroll>tbody>tr{display: table;width: 100%;table-layout: fixed;}
                        table.table-scroll>thead>tr>th{text-align: center;}
                        table.table-scroll>tbody>tr>td{text-align: center;}
                    </style>
                    <div class="col-md-8">
                        <table style="margin-left: 20px;" class="table table-scroll table-condensed table-striped">
                            <thead>
                            <tr style="background: #ccc;">
                                <th>Cover</th>
                                <th>Description</th>
                                <th>Premium Type</th>
                                <th>Annual Premium</th>
                                @php $column = 4 @endphp
                                @if($discount)
                                    <th>Discount</th>
                                    @php $column += 1 @endphp
                                @endif
                                @if($data->is_prorata)
                                    <th>Prorata Premium</th>
                                    @php $column += 1 @endphp
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vessels as $v => $ves)
                                <tr>
                                    <td>
                                        {{ $ves->vessel_name }}
                                    </td>
                                    <td>
                                        {{ ($data->is_prorata) ? 'Prorata Premium' : 'Annual Premium' }}
                                    </td>
                                    <td>
                                        {{ $ves->premium_type }}
                                    </td>
                                    <td>{{ $currency }} {{ number_format($ves->annual_premium,2) }}</td>
                                    @if($discount)
                                        <td>{{ $ves->discount }}%</td>
                                    @endif
                                    @if($data->is_prorata)
                                        <td>{{ $currency .' '.number_format($ves->prorata_premium) }}</td>
                                    @endif
                                </tr>
                                @if(\App\Http\Controllers\QuotesController::getAdditionalPremium($ves->premi_id))
                                    @foreach(\App\Http\Controllers\QuotesController::getAdditionalPremium($ves->premi_id) as $x => $add)
                                        <tr>
                                            <td></td>
                                            <td>{{ $add->type_ }} </td>
                                            <td>{{ $add->premium_type }}</td>
                                            <td>{{ $currency.' '.number_format($add->annual_premium,2) }}</td>
                                            @if($discount)<td></td>@endif
                                            @if($data->is_prorata)
                                                <td>{{ $currency .' '.number_format($add->prorata_premium) }}</td>
                                                @endif
                                        </tr>
                                    @endforeach
                                    <tr style="border-bottom: 2px solid #ccc; !important;">
                                        <td colspan="{{ ($column - 2) }}"></td>
                                        <td class="text-center">SubTotal </td>
                                        <td> {{ $currency }} {{ number_format($ves->amount_total,2) }}</td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr></tr>
                            <tr style="background: #ccc;font-weight: bold;">
                                <td colspan="{{ ($column - 2) }}"></td>
                                <td class="text-center">Total</td>
                                <td> {{ $currency }} {{ number_format($total,2) }}</td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-8 alert alert-success">Commission Note <span type="button" data-toggle="collapse" data-target="#row1" class="btn-link">Show</span> </div>
                    <div class="collapse" id="row1">
                        <div id="row1">
                            <div class="col-md-8">
                                <table style="margin-left: 20px;" class="table text-center table-scroll table-striped data-table">
                                    <thead>
                                    <tr style="background: #ccc;">
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th align="center" colspan="2">Co Broke</th>
                                        <th align="center" colspan="2">Tax</th>
                                        <th></th>
                                    </tr>
                                    <tr style="background: #ccc;">
                                        <th>No.</th>
                                        <th>Cover</th>
                                        <th>Premium</th>
                                        <th>%</th>
                                        <th>Gross</th>
                                        <th>VAT</th>
                                        <th>WHT</th>
                                        <th>Net Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($commission && $data->CommissionNote)
                                        @foreach($data->CommissionNote->CommissionDetail as $c => $comm)
                                            <tr>
                                                <td>{{ ($c+1) }}</td>
                                                <td>{{ $comm->Premium->Vessel->vessel_name }}</td>
                                                <td>{{ $currency .' '.number_format($comm->Premium->amount_total,2) }}</td>
                                                <td>{{ $commission->brokerage }}%</td>
                                                <td>{{ number_format($comm->co_broke,2) }}</td>
                                                <td>{{ number_format($comm->vat,2) }}</td>
                                                <td>{{ number_format($comm->wht,2) }}</td>
                                                <td>{{ number_format($comm->net_amount,2) }}</td>
                                            </tr>
                                            @endforeach
                                        @else
                                        <tr>
                                            <td colspan="8">Tidak ada commission note.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                    @if($commission && $data->CommissionNote)
                                    <tfoot>
                                    <tr style="background: #ccc;">
                                        <td colspan="5"></td>
                                        <td class="text-center">Grand Total</td>
                                        <td> {{ $currency }} {{ number_format($data->CommissionNote->grand_total,2) }}</td>
                                    </tr>
                                    </tfoot>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
                <div class="col-md-12">
                    <hr>
                    <br>
                    <hr>
                    <div class="col-md-6">
                        <div class="btn btn-group btn-lg">
                            <a href="{{ url('quotes_edit',$data->Quotes->id) }}" class="btn btn-success btn-md" style="color:#fff; text-decoration: none;" >Edit</a>
                            <a onclick="print()" class="btn btn-danger btn-md" style="color:#fff; text-decoration: none;" >Print</a>
                            <a onclick="generateInvoice({{ $data->id }},{{ $data->Quotes->quote_type }})" class="btn btn-primary btn-md" style="color:#fff; text-decoration: none;" >Export Docx</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT -->
</div>
<script>
    function generateInvoice(id,iden){
        var req;
        if(iden === 2){
            req = 'invoicing_cargo.php';
        }else{
            req = 'invoicing.php';
        }
        $.post('{{ env('DOCUMENT_GENERATOR_URL') }}'+req,{id:id},function(data){
            alert('Generated invoice with status : success');
            window.location.href = '{{ route('invoice.index') }}';
        })
    }
</script>
<!-- END MAIN -->
@endsection
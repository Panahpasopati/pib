@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if ($session = Session::get('info'))
                        <div class="alert alert-success alert-dismissable">
                            {{ $session }}
                        </div>
                    @endif
                    <h4 class="page-title">Create Credit Note #{!! $data->our_ref !!}</h4>
                    @if($data->CreditNote)
                        <div class="quote_option" style="margin-bottom: 10px;border: 1px #ddd solid;padding: 10px;background: antiquewhite;">
                            <div class="alert alert-success alert-dismissable">
                                Sudah ada Credit Note untuk Invoice <a href="{{ url('show_invoice',$data->id) }}">#{{ $data->Invoice->invoice_no }}</a>
                            </div>
                            <div class="btn-group-sm"><a href="{{ url('invoices_list') }}" class="btn btn-info">Batalkan</a> <a onclick="event.preventDefault(); document.getElementById('action_cn_new').submit();" href="{{ url('new_cn_delete_old') }}" class="btn btn-primary">Buat CN Baru*</a> </div>
                            <small class="text-info">*Jika pilih membuat <strong>buat Credit Note baru</strong>, data lama akan dihapus.</small>
                            <form method="post" accept-charset="utf-8" id="action_cn_new" action="{{ url('inv',['notes','new_cn_delete_old',$data->id]) }}">
                                {{ csrf_field() }}
                            </form>
                        </div>
                        @else
                        <form method="post" class="form-horizontal" action="{{ url('inv',['notes','store_credit_note']) }}">
                            {{ csrf_field() }}
                            <div>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-6">{{ explode("-",$data->assured)[1] }}</label>
                                            <label class="control-label col-md-6">{{ 'CO' }}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <table class="table table-responsive table-info">
                                        <thead>
                                        <tr><th>Insured</th><th>Insurer</th><th>Period of Cover</th></tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ explode("-",$data->assured)[1] }}</td><td>{{ $data->Security->assured_name }}</td><td>{{ $data->period }}</td>
                                        </tr>
                                        </tbody>
                                        <thead>
                                        <tr><th>Class of Insurence</th><th>Our Reference</th><th>Amount Due</th></tr>
                                        </thead>
                                        <tbody>
                                        <tr><td>{{ $data->QuoteType->name }}</td><td>{{ $data->Invoice->invoice_no }}</td><td>{{ $data->currency }} {{ number_format($data->ammount_total) }}</td></tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Credit Note No</label>
                                        <input type="text" name="credit_note_no" required class="form-control">
                                        <input type="hidden" name="id_" required value="{{ $data->id }}">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Period From</label>
                                            <input type="text" class="form-control" disabled value="{{ date('d M Y',strtotime($data->period_from)) }}">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">CN Date</label>
                                            <input type="text" class="form-control" name="credit_end"  readonly value="{{ date('d M Y') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Period To</label>
                                            <input type="text" class="form-control" disabled value="{{ date('d M Y',strtotime($data->period_to)) }}">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Days</label>
                                            @php $date1=date_create(date("Y-m-d",strtotime($data->period_to))) @endphp
                                            @php $date2=date_create(date("Y-m-d")) @endphp
                                            @php $diff=date_diff($date1,$date2) @endphp
                                            @php $days = $diff->format("%a") @endphp
                                            <input type="text" class="form-control" id="credit_end" name="credit_days_count" readonly value="{{ $days  }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Vessel</label>
                                        <select class="form-control" name="credit_note_vessel[]" multiple id="credit_note_vessels">
                                            @foreach($data->Vessels as $i => $v)
                                                <option value="{{ $v->id_vessel }}">{{ $v->VesselDetail->vessel_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Policy Cost :</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control col-md-4" @if($data->currency == 'USD') readonly @endif name="policy_cost" placeholder="Hanya aktif jika menggunakan currency IDR">
                                        </div>
                                    </div>
                                    <table class="table table-responsive table-bordered" id="credit_detail">
                                        <thead>
                                        <tr>
                                            <th>Instalment Number</th>
                                            <th>Due Date</th>
                                            <th>Annual Premium</th>
                                            <th>Premium Prorate Due</th>
                                        </tr>
                                        </thead>
                                        <tbody id="credit_note_detail" class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                                <hr><br>
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="panel panel-default" id="filter">
                                            <div class="panel-body text-center">
                                                <ul class="font-md font-bold">
                                                    <button class="btn-link" type="submit" ><li class="hvr-blue">Save</li></button>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </form>
                        @endif
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <!-- /.container-fluid-->
<script>
    $("#credit_note_vessels").change(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.post('../change_credit_note_vessels',{id:$("#credit_note_vessels").val(),quote:{!! $data->id !!},days:$("#credit_end").val()},function(data){
            $("#credit_note_detail").html(data);
        })
    })
</script>
@endsection

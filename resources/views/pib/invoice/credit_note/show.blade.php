@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h4 class="page-title">Create Credit Note #{!! $data->Quote->our_ref !!}</h4>
                <div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-6">{{ explode("-",$data->Quote->assured)[1] }}</label>
                                <label class="control-label col-md-6">{{ 'CO' }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-responsive table-info">
                            <thead>
                            <tr><th>Insured</th><th>Insurer</th><th>Period of Cover</th></tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ explode("-",$data->Quote->assured)[1] }}</td><td>{{ $data->Quote->Security->assured_name }}</td><td>{{ $data->Quote->period }}</td>
                            </tr>
                            </tbody>
                            <thead>
                            <tr><th>Class of Insurence</th><th>Our Reference</th><th>Amount Due</th></tr>
                            </thead>
                            <tbody>
                            <tr><td>{{ $data->Quote->QuoteType->name }}</td><td>{{ $data->Quote->Invoice->invoice_no }}</td><td>{{ $data->Quote->currency }} {{ number_format($data->Quote->ammount_total) }}</td></tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Credit Note No</label>
                            <label class="control-label">{{ $data->credit_note_no }}</label>
                        </div>
                        <table class="table table-responsive table-bordered" id="credit_detail">
                            <thead>
                            <tr>
                                <th>
                                    Credit Note Details <br/>
                                </th>
                                <th>Commision</th>
                                <th>PPN</th>
                                <th>With Holding Tax</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data->Details as $i => $v)
                            @if($v->is_all_vessels)
                                All Vessels <input type="checkbox" name="credit_note_all_vessel" checked readonly>
                                @endif
                                <tr>
                                    <td class="text-center">
                                        {{ $v->Vessel->vessel_name }}
                                    </td>
                                    <td>
                                        Rate : {{ $v->commission_rate }} %<br/>
                                        Amount : ({{ $data->Quote->currency }}) {{ number_format($v->commission_amount) }}
                                    </td>
                                    <td>
                                        Rate :  {{ $v->ppn_rate }} % <br>
                                        Amount : ({{ $data->Quote->currency }}) {{ number_format($v->ppn_amount) }}
                                    </td>
                                    <td>
                                        Rate : {{ $v->tax_rate }}<br/>
                                        Amount : ({{ $data->Quote->currency }}) {{ number_format($v->tax_amount) }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr><br>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel panel-default" id="filter">
                                <div class="panel-body text-center">
                                    <ul class="font-md font-bold">
                                        <button class="btn-link" type="submit" ><li class="hvr-blue">Export Docx</li></button>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- /.container-fluid-->
@endsection

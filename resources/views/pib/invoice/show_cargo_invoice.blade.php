@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <style>
        <!--
        /* Font Definitions */
        @font-face
        {font-family:"MS Mincho";
            panose-1:2 2 6 9 4 2 5 8 3 4;}
        @font-face
        {font-family:"Cambria Math";
            panose-1:2 4 5 3 5 4 6 3 2 4;}
        @font-face
        {font-family:"Palatino Linotype";
            panose-1:2 4 5 2 5 5 5 3 3 4;}
        @font-face
        {font-family:Cambria;
            panose-1:2 4 5 3 5 4 6 3 2 4;}
        @font-face
        {font-family:"Helvetica Neue";}
        @font-face
        {font-family:"Helvetica Neue Light";}
        @font-face
        {font-family:"\@MS Mincho";
            panose-1:2 2 6 9 4 2 5 8 3 4;}
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {margin:0in;
            margin-bottom:.0001pt;
            font-size:12.0pt;
            font-family:"Cambria",serif;}
        p.MsoFooter, li.MsoFooter, div.MsoFooter
        {mso-style-link:"Footer Char";
            margin:0in;
            margin-bottom:.0001pt;
            font-size:12.0pt;
            font-family:"Cambria",serif;}
        span.FooterChar
        {mso-style-name:"Footer Char";
            mso-style-link:Footer;}
        .MsoChpDefault
        {font-size:12.0pt;
            font-family:"Cambria",serif;}
        /* Page Definitions */
        @page WordSection1
        {size:595.0pt 842.0pt;
            margin:56.7pt 56.7pt 42.55pt 56.7pt;}
        div.WordSection1
        {page:WordSection1;}
        -->
    </style>
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class=WordSection1>

                            <p class=MsoNormal><img width=196 height=45
                                                    src="{{ asset('assets/img/image001.jpg') }}" align=right hspace=12 alt=logo1></p>

                            <p class=MsoNormal><b><span style='font-size:18.0pt;font-family:"Helvetica Neue";
color:#404040'>Invoice </span></b></p>

                            <p class=MsoNormal><span style='font-size:11.0pt;font-family:"Helvetica Neue";
color:#404040'>&nbsp;</span></p>

                            <p class=MsoNormal><span style='font-size:11.0pt;font-family:"Helvetica Neue";
color:#404040'>&nbsp;</span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>Invoice No: {{ $data->invoice_no }}</span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>Dated Issued: {{ date('d M Y',strtotime($data->created_at)) }}</span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>&nbsp;</span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>{{ explode("-",$data->Quotes->assured)[1] }}</span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>Address :</span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>c/o: </span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>Attention: Ms. </span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>&nbsp;</span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>Assured                 {{ explode("-",$data->Quotes->assured)[1] }} </span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>Insurer                                   {{ $data->security  }}  </span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>Class                                       Marine Cargo  </span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>Period of Insurance           {{ $data->Quotes->period }}                    
</span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>Days                                       -                                                             
</span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>Amount  Due :                    <b>IDR {{ number_format($data->Quotes->ammount_total,2) }}</b></span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>&nbsp;</span></p>

                            <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 align=left
                                   width=659 style='width:494.15pt;border-collapse:collapse;border:none;
 margin-left:6.75pt;margin-right:6.75pt'>
                                <tr style='height:13.5pt'>
                                    <td width=153 valign=top style='width:114.9pt;border:solid windowtext 1.0pt;
  background:#BFBFBF;padding:0in 5.4pt 0in 5.4pt;height:13.5pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>Instalment
  Number</span></b></p>
                                    </td>
                                    <td width=348 valign=top style='width:260.8pt;border:solid windowtext 1.0pt;
  border-left:none;background:#BFBFBF;padding:0in 5.4pt 0in 5.4pt;height:13.5pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>Due
  Date </span></b></p>
                                    </td>
                                    <td width=158 valign=top style='width:118.45pt;border:solid windowtext 1.0pt;
  border-left:none;background:#BFBFBF;padding:0in 5.4pt 0in 5.4pt;height:13.5pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>Premium</span></b></p>
                                    </td>
                                </tr>
                                @foreach($premium_warranty as $pr)
                                <tr>
                                    <td width=153 valign=top style='width:114.9pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>&nbsp;</span></p>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>1</span></p>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>&nbsp;</span></p>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>&nbsp;</span></p>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>&nbsp;</span></p>
                                    </td>
                                    <td width=348 valign=top style='width:260.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>&nbsp;</span></p>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>{{ $pr->due_date }}</span></p>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>&nbsp;</span></p>
                                    </td>
                                    <td width=158 valign=top style='width:118.45pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>&nbsp;</span></p>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>IDR {{ number_format($data->Quotes->ammount_total,2) }}</span></p>
                                    </td>
                                </tr>
                                    @endforeach
                            </table>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>                                </span><span style='font-size:
9.0pt;font-family:"Helvetica Neue"'>                                                                                </span></p>

                            <p class=MsoNormal><span style='font-size:9.0pt;font-family:"Helvetica Neue"'>E.&amp;.O.E  </span></p>

                            <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>&nbsp;</span></b></p>

                            <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>Settlement please be made to the following bank account:</span></b></p>

                            <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>PT. Bank DBS Indonesia                                  Account
Name: PT. Pacific Indonesia Berjaya</span></b></p>

                            <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>Xxxxxxx                                                                   
Account No: xxxxx (IDR) </span></b></p>

                            <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>Xxxxxxx                                                                   
Swift Code: DBSBIDJA</span></b><span style='font-size:10.0pt'>                                                                                                                                          </span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue Light";
color:#343434'>The above premium is the amount due, any applicable taxes and
remittance charges are to be added to the amount and paid separately. It is a
condition that the premium shall be paid to insurers on or before the due date/s.
A breach of premium warranty entitles the insurer to reduce or avoid his
liability under the Contract of Insurance in respect of a claim regardless of
whether the warranty is material to the loss.</span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue Light";
color:#343434'>We shall not be liable and be hold harmless for any resulting
financial losses or damages sustained by you due to your failure to pay us the
amount payable by the indicated due date/s and for any delay payment to the
insurer as a result of your failure to provide us with the payment advice
and/or payment details.</span><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>               </span></p>

                            <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>&nbsp;</span></p>

                            <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=656
                                   style='width:491.75pt;margin-left:-1.8pt;border-collapse:collapse;border:none'>
                                <tr style='height:28.05pt'>
                                    <td width=95 valign=top style='width:70.9pt;border:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.05pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>Cover</span></b></p>
                                    </td>
                                    <td width=82 valign=top style='width:61.85pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0in 5.4pt 0in 5.4pt;height:28.05pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>Policy Reference</span></b></p>
                                    </td>
                                    <td width=120 valign=top style='width:90.25pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0in 5.4pt 0in 5.4pt;height:28.05pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>Description </span></b></p>
                                    </td>
                                    <td width=147 valign=top style='width:110.15pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0in 5.4pt 0in 5.4pt;height:28.05pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>Sum Insured </span></b></p>
                                    </td>
                                    <td width=59 valign=top style='width:44.4pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0in 5.4pt 0in 5.4pt;height:28.05pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>Rate</span></b></p>
                                    </td>
                                    <td width=152 valign=top style='width:114.2pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0in 5.4pt 0in 5.4pt;height:28.05pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>Premium</span></b></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width=95 valign=top style='width:70.9pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>@foreach($vessels as $v) {{ $v->vessel_name }}  @endforeach</span></p>
                                    </td>
                                    <td width=82 valign=top style='width:61.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>####</span></p>
                                    </td>
                                    <td width=120 valign=top style='width:90.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>{{ $data->Quotes->voyage }} ETD {{ $data->Quotes->period }}</span></p>
                                    </td>
                                    <td width=147 valign=top style='width:110.15pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>IDR {{ number_format($data->Quotes->sum_insured,2) }}</span></p>
                                    </td>
                                    <td width=59 valign=top style='width:44.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>{{ $data->Quotes->rate }}%</span></p>
                                    </td>
                                    <td width=152 valign=top style='width:114.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>IDR {{ number_format($data->Quotes->ammount_total,2) }}</span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width=95 valign=top style='width:70.9pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=82 valign=top style='width:61.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=120 valign=top style='width:90.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=147 valign=top style='width:110.15pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=59 valign=top style='width:44.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=152 valign=top style='width:114.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width=95 valign=top style='width:70.9pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=82 valign=top style='width:61.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=120 valign=top style='width:90.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=147 valign=top style='width:110.15pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=59 valign=top style='width:44.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=152 valign=top style='width:114.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width=95 valign=top style='width:70.9pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=82 valign=top style='width:61.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=120 valign=top style='width:90.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=147 valign=top style='width:110.15pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=59 valign=top style='width:44.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                    <td width=152 valign=top style='width:114.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
                                        <p class=MsoNormal><span style='font-size:10.0pt;font-family:"Helvetica Neue";
  color:#404040'>&nbsp;</span></p>
                                    </td>
                                </tr>
                            </table>

                            <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 align=left
                                   width=659 style='width:494.15pt;border-collapse:collapse;border:none;
 margin-left:6.75pt;margin-right:6.75pt'>
                                <tr style='height:13.35pt'>
                                    <td width=536 valign=top style='width:402.0pt;border:solid windowtext 1.0pt;
  background:#C6D9F1;padding:0in 5.4pt 0in 5.4pt;height:13.35pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>                                                                                                                      
  Deductions</span></b></p>
                                    </td>
                                    <td width=123 valign=top style='width:92.15pt;border:solid windowtext 1.0pt;
  border-left:none;background:#C6D9F1;padding:0in 5.4pt 0in 5.4pt;height:13.35pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>&nbsp;</span></b></p>
                                    </td>
                                </tr>
                                <tr style='height:13.35pt'>
                                    <td width=536 valign=top style='width:402.0pt;border:solid windowtext 1.0pt;
  border-top:none;background:#C6D9F1;padding:0in 5.4pt 0in 5.4pt;height:13.35pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>                                                                                                                              
  Others</span></b></p>
                                    </td>
                                    <td width=123 valign=top style='width:92.15pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#C6D9F1;padding:0in 5.4pt 0in 5.4pt;height:13.35pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>&nbsp;</span></b></p>
                                    </td>
                                </tr>
                                <tr style='height:13.35pt'>
                                    <td width=536 valign=top style='width:402.0pt;border:solid windowtext 1.0pt;
  border-top:none;background:#C6D9F1;padding:0in 5.4pt 0in 5.4pt;height:13.35pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>                                                                                             
  Policy Cost  &amp; Stamp Duty</span></b></p>
                                    </td>
                                    <td width=123 valign=top style='width:92.15pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#C6D9F1;padding:0in 5.4pt 0in 5.4pt;height:13.35pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>IDR    
  {{ number_format($data->Quotes->policy_cost_stamp_duty,2) }}</span></b></p>
                                    </td>
                                </tr>
                                <tr style='height:12.0pt'>
                                    <td width=536 valign=top style='width:402.0pt;border:solid windowtext 1.0pt;
  border-top:none;background:#C6D9F1;padding:0in 5.4pt 0in 5.4pt;height:12.0pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>                                                                                                                          
  Total Due</span></b></p>
                                    </td>
                                    <td width=123 valign=top style='width:92.15pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#C6D9F1;padding:0in 5.4pt 0in 5.4pt;height:12.0pt'>
                                        <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue"'>IDR
   {{ number_format($data->Quotes->ammount_total,2) }}</span></b></p>
                                    </td>
                                </tr>
                            </table>

                            <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:"Helvetica Neue";
color:#404040'>&nbsp;</span></b></p>

                            <p class=MsoNormal><span style='font-size:9.0pt;font-family:"Helvetica Neue"'>                                                                                </span></p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
@endsection
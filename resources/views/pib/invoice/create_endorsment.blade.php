@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="page-title">Create endorsement</h4>
                    <ul class="list-group">
                        <li class="list-group-item list-group-item-primary"><span class="fa fa-info-circle"></span> <a href="">General Endorsement</a> </li>
                        <li class="list-group-item list-group-item-secondary"><span class="fa fa-ship"></span> <a href="{{ url('vessel_endorsement_create',$id) }}">Vessel endorsement</a> </li>
                    </ul>

            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <!-- /.container-fluid-->
@endsection

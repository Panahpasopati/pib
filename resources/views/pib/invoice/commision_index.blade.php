@extends('layouts.report')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <!-- BORDERED TABLE -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Incomes List</h3>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Quotation Ref.</th>
                                        <th>Assured(s)</th>
                                        <th>Premium Ammount(s)</th>
                                        <th>Assurance Company</th>
                                        <th>Premium Ammount</th>
                                        <th>Commision</th>
                                        <th>Margin Income</th>
                                        <th>Commision Income</th>
                                        <th>Total Income</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $i => $d)
                                        <tr>
                                            <td>{{ $i + 1}}</td>
                                            <td>{{ $d->our_ref }}</td>
                                            <td>{{ $d->assured }}</td>
                                            <td>${{ number_format($d->ammount_total,2) }}</td>
                                            <td>{{ $d->assurance_company_name }}</td>
                                            <td>${{ number_format($d->premium_ammount,2) }}</td>
                                            <td>{{ $d->commision }} %</td>
                                            <td>${{ number_format($d->margin_income,2) }}</td>
                                            <td>${{ number_format($d->commision_income,2) }}</td>
                                            <td>${{ number_format(($d->margin_income + $d->commision_income),2 ) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $data->render() }}
                            </div>
                        </div>
                        <!-- END BORDERED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <script>
        function generateInvoice(id){
            $.post('http://localhost/generate_quotes/invoicing.php',{id:id},function(data){
                alert('Generated invoice with status : success');
                //location.reload();
            })
        }
    </script>
@endsection
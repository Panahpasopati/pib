@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                    <div class="row">
                    <div class="col-md-12">
                        <!-- BORDERED TABLE -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Invoices List</h3>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Type</th>
                                        <th>Insurer</th>
                                        <th>Ref.</th>
                                        <th>Period</th>
                                        <th>Installment</th>
                                        <th>Date Issued</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $i => $d)
                                        <tr>
                                            <td>{{ $i + 1}}</td>
                                            <td>{{ @$d->Quotes->QuoteType->name }}</td>
                                            <td style="font-size: small">@foreach($d->Quotes->Insurers as $insurer) &bullet; {{ $insurer->Insurer->insured }} <br>@endforeach</td>
                                            <td>{{ $d->invoice_no }}</td>
                                            <td><small class="text-justify">{{ date('d-m-Y',strtotime($d->period_from)) }}<br> - <br>{{ date('d-m-Y',strtotime($d->period_to)) }}</small></td>
                                            <td>{{ $d->installment_count }}</td>
                                            <td><small>{{ @date('d-m-Y',strtotime($d->date_issued)) }}</small></td>
                                            <td>
                                                <div class="btn-group-vertical">
                                                    <div class="dropdown">
                                                        <button class="btn btn-warning btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-download"></span> <span class="fa fa-caret-down"></span> </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a  {{ !$d->invoice_file ? 'disabled' : '' }}  href="{{ env('DOCUMENT_GENERATED_URL') . 'invoices/'. @$d->invoice_file }}" title="Download Invoice" class="btn btn-link">Invoice </a></li>
                                                            <li><a {{ (!@$d->invoice_detail_file) ? 'disabled' : '' }} href="{{ env('DOCUMENT_GENERATED_URL') . 'invoices/'. @$d->Invoice->invoice_detail_file }}" title="Download Invoice Detail" class="btn btn-link">Invoice Detail</a></li>
                                                            <li><a {{ !$d->commission_note ? 'disabled' : '' }} href="{{ env('DOCUMENT_GENERATED_URL') . 'invoices/'. @$d->commission_note }}" title="Download Commission Notice" class="btn btn-link">Commission Note</a></li>
                                                        </ul>
                                                    </div>
                                                    <a href="{{ route('invoice.show',$d->id) }}" title="View Invoice" class="btn btn-sm btn-success"><span class="fa fa-eye-slash"></span> </a>
                                                    <a {{ date('Y-m-d') < date('Y-m-d',strtotime($d->period_to)) ? '' : 'disabled' }} href="{{ url('inv',['notes','create_credit_note',$d->id]) }}" title="Credit Note" class="btn btn-sm btn-primary"><span class="fa fa-refresh"></span> </a>
                                                    <!--
                                                    <a href="{{ url('vessel_endorsement_create',$d->id) }}" title="Create endorsement" class="btn btn-sm btn-danger"><span class="fa fa-plus"></span> </a>
                                              !-->
                                                    <a href="{{ route('invoice.edit',$d->id) }}" title="Edit Invoice" class="btn btn-sm btn-success"><span class="fa fa-pencil"></span> </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $data->render() }}
                            </div>
                        </div>
                        <!-- END BORDERED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <script>
        function generateInvoice(id,iden){
            var req;
            if(iden === 2){
                req = 'invoicing_cargo.php';
            }else{
                req = 'invoicing.php';
            }
            $.post('{{ env('DOCUMENT_GENERATOR_URL') }}'+req,{id:id},function(data){
                alert('Generated invoice with status : success');
                //location.reload();
            })
        }
    </script>
@endsection
@extends('layouts.pib')
@section('content')
    <style>
        .panel-tabs {
            display: none;
        }
        .panel-tabs {
            animation: fadeEffect 1s; /* Fading effect takes 1 second */
        }
        #filter .panel-body{
            padding: 5px;
        }
        #filter ul{
            margin-top: 5px;
            padding: 0px;
        }
        #filter ul li{
            display: inline;
            padding: 5px;
            margin: 5px 10px;
            border-bottom: 2px solid transparent;
            list-style:	none;
            transition-duration: 0.5s;
            -webkit-transition-duration: 0.5s;
            -moz-transition-duration: 0.5s;
            -o-transition-duration: 0.5s;
        }
        #filter ul li:hover, #filter ul li.active{
            border-color: inherit;
        }
        a.tab-link.active {
            /* border-color: inherit; */
            color: #2196f3;
            border-bottom: 6px solid;
            border-radius: 10px 1px;
        }
        .save-temporary{
            position: fixed;
            background: #333;
            width: 10%;
            padding: 10px;
            top: 81px;
            border-radius: 5px;
            z-index: 999;
            right: 0;
            opacity: 0.1;
        }
        .save-temporary:hover{
            opacity: 0.9;
        }
        .note-btn-group.btn-group.note-para .btn-sm{
            padding: 5px 18px;
        }
    </style>

    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if($error = Session::get('error'))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{$error}}
                    </div>
                    @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="page-title">Direct Invoice</h4>
                <form method="post" class="form-horizontal" action="{{ url('special',['direct-invoice','store']) }}" id="form-create-quote">
                    {{ csrf_field() }}
                    <div>
                        <style>
                            .col-md-6 label.control-label.col-md-4 {
                                padding-left: 0px !important;
                            }
                            input.form-control.our-ref {
                                font-weight: 700;
                            }
                        </style>
                        <div class="row" id="row-1">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="hidden" name="our_ref" class="form-control our-ref" value="QTE/####/###/######" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Type Quotation {{ old('quote_ty') }}</label>
                                        <div class="col-md-6">
                                            <select name="quote_type" class="form-control type_quote" required="">
                                                <option value="" disabled selected>--Choose an Option--</option>
                                                @foreach($type as $t)
                                                    @if($t->id_type != 8)
                                                        <option value="{{ $t->id_type }}">{{ $t->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <input type="hidden" name="quotation_status" class="form-control" value="New">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <hr><br>
                                    <div class="form-group">
                                        <div class="col-md-12 three" style="padding-left: 0px !important;">
                                            <label class="control-label col-md-1" style="padding-left: 0px !important;">Assured</label>
                                            <div class="col-md-4">
                                                <select class="form-control assured" id="ins_direct_invoice" multiple="multiple" name="insurer[]" required="">
                                                </select>
                                                <a class="btn-link" href="{{ route('insurer.create') }}" target="_blank"><i class="fa fa-plus"></i> Add Company</a>
                                            </div>
                                            <div class="col-md-1">
                                                <a onclick="refreshInsurer()"><span title="Refresh insurer company" class="fa fa-refresh"></span> </a>
                                            </div>
                                            <label class="control-label col-md-1" style="padding-left: 0px !important;">Mortgage</label>
                                            <div class="col-md-2">
                                                <select class="form-control banks" name="mortgage_bank">
                                                    @foreach($banks as $b)
                                                        <option value="{{ $b->id }}">{{ $b->bank_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Security</label>
                                                <div class="col-md-6">
                                                    <select name="security" required class="form-control coa">
                                                        @foreach($assured as $ass)
                                                            <option value="{{ $ass->id }}">{{ $ass->assured_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Currency</label>
                                                <div class="col-md-6">
                                                    <select name="currency" required class="form-control">
                                                        <option value="USD" selected>USD</option>
                                                        <option value="IDR">IDR</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Intermediary</th>
                                            <th>Brokerage</th>
                                            <th>VAT</th>
                                            <th>WHT</th>
                                            <th>Policy Cost</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <input type="text" name="intermediary_name" class="form-control comm">
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input required type="text" name="brokerage" class="form-control comm">
                                                    <span class="input-group-addon">%</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input required type="text" name="vat" class="form-control comm">
                                                    <span class="input-group-addon">%</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input required type="text" name="wht" class="form-control comm">
                                                    <span class="input-group-addon">%</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input type="text" name="policy_cost" class="form-control comm">
                                                    <span class="input-group-addon">%</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5"><p class="text text-info result-comm"></p> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Installment</label>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="installment_count" min="1" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr><br>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="panel panel-default" id="filter">
                                    <div class="panel-body text-center">
                                        <ul class="font-md font-bold">
                                            <button class="btn-link" type="submit" ><li class="hvr-blue">Next</li></button>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <!-- /.container-fluid-->
    <div id="modal-company" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add More Company</h4>
                </div>
                <div class="modal-body">
                    <form id="form-add-company">
                        {!! csrf_field() !!}
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Insured</label>
                                    <input type="text" name="insured" class="form-control" value="{{ old('insured') }}" id="insured" required="">
                                    <div class="result_"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Address</label>
                                    <textarea name="address" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Phone</label>
                                    <input type="text" name="phone" class="form-control" value="{{ old('phone') }}" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="text" name="email" class="form-control" value="{{ old('email') }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Channel</label>
                                    <select name="channel" required class="form-control channel">
                                        <option value="" disabled selected>--choose an option--</option>
                                        <option value="Intermediary">Intermediary</option>
                                        <option value="Agent">Agent</option>
                                        <option value="Direct">Direct</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Broker/Agent</label>
                                    <select name="channel_id" class="form-control channel_id" disabled>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">PIC</label>
                                    <input type="text" name="pic" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"></label>
                                    <button class="btn btn-primary" type="button" id="submit-new-company">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

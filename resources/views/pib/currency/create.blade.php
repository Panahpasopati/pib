@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h4 class="page-title">Currency Create</h4>
            <form method="post" action="{{ route('currency.store') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Currency</label>
                                @php
                                    $array = ['USD', 'AUD', 'GBP', 'HKD', 'IDR', 'JPY', 'SIN']
                                @endphp
                                <select name="currency" required class="form-control">
                                    <option value="" selected disabled>--Choose an Option</option>
                                    @foreach($array as $arr)
                                        <option value="{{ $arr }}">{{ $arr }}</option>
                                        @endforeach
                                </select>
                            </div>
                            <table class="table table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Rate</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($lists as $d => $dt)
                                    <tr>
                                        <td>{{ $dt->format("d-M-Y") }}</td>
                                        <td><input type="hidden" name="date[{{ $d }}]" value="{{ $dt->format("Y-m-d") }}"> <input type="text" name="rate[{{ $d }}]" class="form-control" required></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-md">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- /.container-fluid-->



@endsection

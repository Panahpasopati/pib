@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h4 class="page-title">Currency Edit</h4>
            <form method="post" action="{{ route('currency.update',$data->id) }}">
                <input type="hidden" name="_method" value="PATCH">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Tanggal</label>
                                <input type="text" name="date_" class="form-control" value="{{ $data->date_ }}" readonly >
                            </div>
                            <div class="form-group">
                                <label class="control-label">Currency</label>
                                @php
                                $array = ['USD', 'AUD', 'GBP', 'HKD', 'IDR', 'JPY', 'SIN']
                                @endphp
                                <select name="currency" required class="form-control">
                                    <option value="" selected disabled>--Choose an Option</option>
                                    @foreach($array as $arr)
                                        <option value="{{ $arr }}" {{ $arr == $data->currency ? 'selected' : '' }}>{{ $arr }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Rate</label>
                                <input type="text" name="rate" class="form-control" value="{{ $data->rate }}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-md">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- /.container-fluid-->



@endsection

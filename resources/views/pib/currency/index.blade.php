@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <h3 class="page-title"><a type="button" href="{{ route('currency.create') }}" class="btn btn-default"><i class="fa fa-plus-square"></i> New Currency </a></h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- BORDERED TABLE -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Currency DataSet</h3>
                        </div>
                        <div class="panel-body" style="height: 400px; overflow-z: scroll;">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tanggal</th>
                                    <th>Currency</th>
                                    <th>Rate</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $i => $d)
                                    <tr>
                                        <td>{{ $i + 1}}</td>
                                        <td>{{ date('d-M-Y',strtotime($d->date_)) }}</td>
                                        <td>{{ $d->currency." ".number_format($d->rate,2) }}</td>
                                        <td><a href="{{ route('currency.edit',$d->id) }}" title="Edit?" class="btn btn-default"><span class="fa fa-pencil"></span> </a> </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- END BORDERED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
</div>
@endsection
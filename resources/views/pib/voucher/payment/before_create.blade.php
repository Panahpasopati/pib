@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h4 class="page-title">Select Premium Receipt Voucher</h4>
                <form method="post" id="form-create-" accept-charset="utf-8" action="{{ url('voucher','create_premium_payment') }}">
                    {{ csrf_field() }}
                <div>
                    <div class="col-md-12">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="control-label col-md-2">Assured</label>
                                <div class="col-md-6">
                                    <select id="security" name="security" required class="form-control coa" >
                                        @foreach($assured as $ass)
                                            <option value="{{ $ass->id }}">{{ $ass->assured_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Periode</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">From</div>
                                        <input type="date" name="period_from" id="period_from" class="from-control" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">To</div>
                                        <input type="date" name="period_to" id="period_to" class="from-control" required>
                                    </div>
                                </div>
                                <br>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Receipt Voucher</label>
                                <div class="col-md-8">
                                    <a role="button" onclick="getReceiptVoucher()"><span title="Get receipt voucher" class="fa fa-refresh"></span> </a>
                                </div>
                                <br>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                        <table class="table table-bordered data-table" style="display: none;">
                                            <thead>
                                            <tr>
                                                <th class="th-sorting sorting-item"><input type="checkbox" class="checkbox" id="select-all"></th>
                                                <th>Voucher No.</th>
                                                <th>Vessels</th>
                                                <th>Invoice No.</th>
                                                <th>Installment</th>
                                                <th>Amount</th>
                                                <th>Received Date</th>
                                            </tr>
                                            </thead>
                                            <tbody id="_table_list">
                                            </tbody>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr><br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4" style="margin-left: 10px;margin-top: 15px;">
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- /.container-fluid-->
<script>
    function getReceiptVoucher(){
        var from = $("#period_from").val(),
                to = $("#period_to").val(),
                security = $("#security").val();
        if(from == "" || to == "" || security == ""){
            alert('Periksa kembali kolom kolom masukkan!');
            return false;
        }
        $.post(BASE+'/voucher/payment/get_receipt_voucher',{security:security,pfrom:from,pto:to},function(data){
            if(data.success){

                $(".table").css('display','block');
                $.each(data.data,function(i,value){
                    var html = '<tr><td class="th-sorting sorting-item"><input type="checkbox" class="checkbox checkbox-select" name="premium_receipt_id['+i+']" value="'+value.id_+'"></td>';
                    html += '<td>'+value.voucher_no+'</td><td>'+value.quote.vessels+'</td><td>'+value.invoice.invoice_no+'</td><td>'+value.installment.installment_no+' /'+value.invoice.installment_count+'</td>';
                    html += '<td>'+value.amount+'</td><td>'+value.received_date+'</td></tr>';

                    $("#_table_list").append(html);
                })
            }
        })
    }
</script>
@endsection

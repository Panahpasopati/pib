@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <h3 class="page-title">
                <a type="button" href="{{ url('voucher',['payment','before-create']) }}" class="btn btn-default"><i class="fa fa-plus-square"></i> Create</a>
            </h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- BORDERED TABLE -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Payment Vouchers List</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Insurer</th>
                                    <th>Voucher No.</th>
                                    <th>Amount</th>
                                    <th>Payment Date</th>
                                    <th>Created At</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $i => $d)
                                    <tr>
                                        <td>{{ $i + 1}}</td>
                                        <td>{{ $d->Security->assured_name }}</td>
                                        <td>{{ $d->voucher_no }}</td>
                                        <td>{{ number_format($d->amount) }}</td>
                                        <td>{{ date('d-m-Y',strtotime($d->received_date)) }}</td>
                                        <td>{{ date('d M Y H:i',strtotime($d->created_at)) }} WIB</td>
                                        <td><a href="{{ route('premium_payment_voucher.show',$d->id) }}"><i class="fa fa-eye-slash"></i> </a> </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $data->render() }}
                        </div>
                    </div>
                    <!-- END BORDERED TABLE -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h4 class="page-title">Premium Payment Voucher</h4>
            <form method="post" class="form-horizontal" action="{{ route('premium_payment_voucher.store') }}">
                {{ csrf_field() }}
                <div>
                    <div class="col-md-12">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label class="control-label col-md-4">Pay to/Insurer</label>
                                <div class="col-md-6">
                                    <input type="text" readonly class="form-control" value="{{ $assured->assured_name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">From/Insured</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control"  readonly value="as attached">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Debit Note No.</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" readonly value="as attached">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Amount</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="amount" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">In Words</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="amount_in_words" rows="4" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <table class="table table-bordered" width="100%">
                                <tbody>
                                <tr>
                                    <td colspan="4"><strong>Premium Payment Voucher</strong></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Voucher No.</td>
                                    <td colspan="2"><input type="text" class="form-control" name="voucher_no" value="{{ $newRef }}" required> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Premium Rec. No.</td>
                                    <td colspan="2"><input type="text" class="form-control" name="premium_rec_no" required> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Payment Date</td>
                                    <td colspan="2"><input type="text" class="form-control datepicker" name="payment_date" value="" required> </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td width="25%"><input type="text" class="form-control" disabled value="Transfer"> </td>
                                    <td>&nbsp; </td>
                                    <td><input type="text" class="form-control" name="bank" required value=""> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Currency</td>
                                    <td colspan="2"><input type="text" class="form-control" name="currency" required> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Inception Date</td>
                                    <td colspan="2"><input type="text" class="form-control" name="inception_date" required value="as attached"> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Ship Name</td>
                                    <td colspan="2"><input type="text" name="ship_name" class="form-control" readonly value="as attached"> </td>
                                </tr>
                                <tr>
                                    <td>Installment #</td>
                                    <td><input type="text" readonly class="form-control" value="-"> </td>
                                    <td>/ </td>
                                    <td><input type="text" class="form-control" value="-"> </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-4">Gross Premium</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="gross_premium" required value="{{ number_format($gross) }}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Disc. From Insurer</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="disc_from_insurer" value="0">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Brokerage</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" value="{{ number_format($brokerage) }}">
                                <input type="hidden" class="form-control" name="brokerage" value="{{ $brokerage }}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">VAT</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="vat" value="0">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"> WHT</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="wht" value="0">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"> Others</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="others" value="0">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">  Bank Charges</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="bank_charges" value="0">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">  Net to Insurer</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="net_to_insurer" required>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <textarea rows="4" class="form-control" name="accounting_entries" placeholder="Accounting Entries"></textarea>
                        </div>
                        <div class="col-md-4">
                            <table class="table" width="100%">
                                <tbody>
                                <tr>
                                    <td>Prepared by:</td>
                                    <td>Approved by:</td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="prepared_by" class="form-control coa" required>
                                            @foreach($users as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <select name="approved_by" class="form-control coa" required>
                                            @foreach($users as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><div class="input-group"><span class="input-group-addon">Date:</span>  <input type="date" name="prepared_by_date" readonly value="{{ date('d M Y') }}" class="form-control"> </div></td>
                                    <td><div class="input-group"><span class="input-group-addon">Date:</span>  <input type="date" name="approve_date" class="form-control"> </div></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12 alert alert-success">Receipt Voucher Detail <span type="button" data-toggle="collapse" data-target="#row1" class="btn-link">Show</span> </div>
                    <div class="collapse" id="row1">
                        <div id="row1">
                            <div class="col-md-12">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Invoice No.</th>
                                        <th>Voucher No.</th>
                                        <th>Installment</th>
                                        <th>Amount</th>
                                        <th>Received Date</th>
                                        <th>Created At</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $i => $d)
                                        <input type="hidden" name="receipt_id[{{ $i }}]" value="{{ $d->id }}">
                                            <tr>
                                                <td>{{ $i + 1}}</td>
                                                <td><a target="_blank" href="{{ route('invoice.show',$d->Invoice->id) }}">{{ $d->Invoice->invoice_no }}</a></td>
                                                <td>{{ $d->voucher_no }}</td>
                                                <th>{{ $d->Installment->installment_no }} /{{ $d->Invoice->installment_count }}</th>
                                                <td>{{ number_format($d->amount) }}</td>
                                                <td>{{ date('d-m-Y',strtotime($d->received_date)) }}</td>
                                                <td>{{ date('d M Y H:i',strtotime($d->created_at)) }} WIB</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <hr><br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4" style="margin-left: 10px;margin-top: 15px;">
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- /.container-fluid-->

@endsection

@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h4 class="page-title">Premium Payment Voucher</h4>
            <form>
                <div>
                    <div class="col-md-12">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label class="control-label col-md-4">Pay to/Insurer</label>
                                <div class="col-md-6">
                                    <input type="text" readonly class="form-control" value="{{ $data->Security->assured_name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">From/Insured</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control"  readonly value="as attached">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Debit Note No.</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" readonly value="as attached">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Amount</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="amount" value="{{ number_format($data->amount) }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">In Words</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="amount_in_words" rows="4" required>{{ $data->amount_in_words }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <table class="table table-bordered" width="100%">
                                <tbody>
                                <tr>
                                    <td colspan="4"><strong>Premium Payment Voucher</strong></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Voucher No.</td>
                                    <td colspan="2"><input type="text" class="form-control" name="voucher_no" required value="{{ $data->voucher_no }}"> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Premium Rec. No.</td>
                                    <td colspan="2"><input type="text" class="form-control" name="premium_rec_no" value="as attached" required> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Payment Date</td>
                                    <td colspan="2"><input type="text" class="form-control datepicker" name="payment_date" value="{{ $data->payment_date }}" required> </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td width="25%"><input type="text" class="form-control" disabled value="Transfer"> </td>
                                    <td>&nbsp; </td>
                                    <td><input type="text" class="form-control" name="bank" required value="{{ $data->bank }}"> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Currency</td>
                                    <td colspan="2"><input type="text" class="form-control" name="currency" value="{{ $data->currency }}" required> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Inception Date</td>
                                    <td colspan="2"><input type="text" class="form-control" name="inception_date" required value="as attached"> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Ship Name</td>
                                    <td colspan="2"><input type="text" name="ship_name" class="form-control" readonly value="as attached"> </td>
                                </tr>
                                <tr>
                                    <td>Installment #</td>
                                    <td><input type="text" readonly class="form-control" value="-"> </td>
                                    <td>/ </td>
                                    <td><input type="text" class="form-control" value="-"> </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-4">Gross Premium</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="gross_premium" required value="{{ number_format($data->gross_premium) }}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Disc. From Insurer</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="disc_from_insurer" value="{{ $data->disc_from_insurer }}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Brokerage</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" value="{{ number_format($data->brokerage) }}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">VAT</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="vat" value="{{ $data->vat }}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"> WHT</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="wht" value="{{ $data->wht }}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"> Others</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="others" value="{{ $data->others }}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">  Bank Charges</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="bank_charges" value="{{ $data->bank_charges }}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">  Net to Insurer</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="net_to_insurer" value="{{ number_format($data->amount) }}" required>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <textarea rows="4" class="form-control" name="accounting_entries" placeholder="Accounting Entries"></textarea>
                        </div>
                        <div class="col-md-4">
                            <table class="table" width="100%">
                                <tbody>
                                <tr>
                                    <td>Prepared by:</td>
                                    <td>Approved by:</td>
                                </tr>
                                <tr>
                                    <td>
                                        <input readonly class="form-control" value="{{ $data->PreparedBy->name }}">
                                    </td>
                                    <td>
                                        <input readonly class="form-control" value="{{ $data->ApprovedBy->name }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td><div class="input-group"><span class="input-group-addon">Date:</span>  <input type="date" name="prepared_by_date" readonly value="{{ date('d M Y') }}" class="form-control"> </div></td>
                                    <td><div class="input-group"><span class="input-group-addon">Date:</span>  <input type="date" name="approve_date" class="form-control"> </div></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12 alert alert-success">Receipt Voucher Detail <span type="button" data-toggle="collapse" data-target="#row1" class="btn-link">Show</span> </div>
                    <div class="collapse" id="row1">
                        <div id="row1">
                            <div class="col-md-12">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Invoice No.</th>
                                        <th>Voucher No.</th>
                                        <th>Installment</th>
                                        <th>Amount</th>
                                        <th>Received Date</th>
                                        <th>Created At</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data->PaymentVoucherDetail as $i => $d)
                                        <tr>
                                            <td>{{ $i + 1}}</td>
                                            <td><a target="_blank" href="{{ route('invoice.show',$d->ReceiptVoucher->Invoice->id) }}">{{ $d->ReceiptVoucher->Invoice->invoice_no }}</a></td>
                                            <td>{{ $d->ReceiptVoucher->voucher_no }}</td>
                                            <th>{{ $d->ReceiptVoucher->Installment->installment_no }} /{{ $d->ReceiptVoucher->Invoice->installment_count }}</th>
                                            <td>{{ number_format($d->ReceiptVoucher->amount) }}</td>
                                            <td>{{ date('d-m-Y',strtotime($d->ReceiptVoucher->received_date)) }}</td>
                                            <td>{{ date('d M Y H:i',strtotime($d->ReceiptVoucher->created_at)) }} WIB</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <hr><br>
                    <hr>
                </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- /.container-fluid-->

@endsection

@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form method="post" class="form-horizontal" action="">
                        {{ csrf_field() }}
                        <div>
                            <div class="col-md-12">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <img src="{{ asset('assets/img/pib.png') }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2"><small>Task</small></label>
                                        <div class="col-md-6">
                                            <select name="task" class="form-control task">
                                                <option value="" selected disabled>--choose--</option>
                                                <option value="premium_receipt">Premium Receipt</option>
                                                <option value="premium_payment">Premium Payment</option>
                                            </select>
                                        </div>
                                        <label class="control-label col-md-2">ACC : </label>
                                        <div class="col-md-2">
                                            <select name="task" class="form-control coa">
                                                @foreach($coa as $c)
                                                    <option value="{{ $c->code }}">{{ $c->code }}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2"><small>Bank</small></label>
                                        <div class="col-md-6">
                                            <select name="bank" class="form-control bank">
                                            </select>
                                        </div>
                                        <label class="control-label col-md-2">CASH.ACC</label>
                                        <div class="col-md-2">
                                            <select name="task" class="form-control coa">
                                                @foreach($coa as $c)
                                                    <option value="{{ $c->code }}">{{ $c->code }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4"><small>Pay to/Insurer</small></label>
                                        <div class="col-md-6">
                                            <select name="pay_to_insurer" id="pay_to" class="form-control coa" required>
                                                <option value="" selected disabled>select an option</option>
                                                @foreach($assured as $assu)
                                                    <option value="{{ $assu->id }}">{{ $assu->assured_name }}</option>
                                                    @endforeach
                                            </select>
                                            <small id="loading" class="hidden">Loading...</small>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4"><small>Invoice No</small></label>
                                        <div class="col-md-6">
                                            <select name="invoice_no" id="invoice_no" class="form-control coa" disabled required>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4"><small>From/Insured</small></label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-form" name="from_insured" id="from_insured" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Debit Note No.</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-form" name="debit_note_no">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Amount</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-form" name="ammount">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">In Words</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control input-form" name="in_words" rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <table class="table table-bordered" width="100%">
                                        <tbody>
                                        <tr>
                                            <td colspan="4">Premium Payment Voucher</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Voucher No.</td>
                                            <td colspan="2"><input type="text" class="form-control input-form" name="voucher_no"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Premium Rec. No.</td>
                                            <td colspan="2"><input type="text" class="form-control input-form" name="premium_rec_no"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Payment Date</td>
                                            <td colspan="2"><input type="text" class="form-control input-form" name="payment_date" value="{{ date('Y-m-d') }}"> </td>
                                        </tr>
                                        <tr>
                                            <td>Paid by</td>
                                            <td width="25%"><input type="text" class="form-control input-form" name="paid_by"> </td>
                                            <td>from </td>
                                            <td><input type="text" class="form-control input-form" name="paid_from"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Currency</td>
                                            <td colspan="2"><input type="radio" name="currency" class="radio-inline" value="usd"> USD <input type="radio" name="currency" value="idr" class="radio-inline"> IDR </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Inception Date</td>
                                            <td colspan="2"><input type="date" class="form-control input-form" name="inception_date"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Ship Name</td>
                                            <td colspan="2"><input type="text" class="form-control input-form" name="ship_name"> </td>
                                        </tr>
                                        <tr>
                                            <td>Installment #</td>
                                            <td><input type="text" class="form-control input-form" name="installment"> </td>
                                            <td>/ </td>
                                            <td><input type="text" class="form-control input-form" name="installment_count" id="installment_count"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Gross Premium</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control input-form" name="gross_premium" id="gross">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-form">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Disc From Insurer</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control input-form" name="disc_from_insurer">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-form">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Brokerage</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control input-form" name="brokerage">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-form">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">VAT</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control input-form" name="vat">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-form">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4"> WHT</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control input-form" name="wht">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-form">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4"> Others</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control input-form" name="others">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-form">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">  Bank Charges</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control input-form" name="bank_charges">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-form">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">  Net to Insurer</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control input-form" name="net_to_insurer">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-form">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <textarea rows="4" class="form-control input-form" name="accounting_entries" placeholder="Accounting Entries"></textarea>
                                </div>
                                <div class="col-md-4">
                                    <table class="table" width="100%">
                                        <tbody>
                                        <tr>
                                            <td>Prepared by:</td>
                                            <td>Approved by:</td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" name="prepared_by" class="form-control input-form"> </td>
                                            <td><input type="text" name="approved_by" class="form-control input-form"> </td>
                                        </tr>
                                        <tr>
                                            <td><div class="input-group"><span class="input-group-addon">Date:</span>  <input type="date" name="prepared_by_date" class="form-control input-form"> </div></td>
                                            <td><div class="input-group"><span class="input-group-addon">Date:</span>  <input type="date" name="approved_by_date" class="form-control input-form"> </div></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr><br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4" style="margin-left: 10px;margin-top: 15px;">
                                        <div class="form-group">
                                            <button class="btn btn-primary" type="submit">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <!-- /.container-fluid-->

<script>
    var receipt  = [
        ['premium_receipt_mandiri','Premium Receipt Mandiri IDR'],
        ['premium_receipt_dbs','Premium Receipt DBS USD']
    ];

    var payment = [
        ['premium_payment_mandiri','Premium Payment Mandiri IDR'],
        ['premium_payment_dbs','Premium Payment DBS USD']
    ];
    $(document).ready(function(){
        task();
        $("#pay_to").change(function(){
            $("#loading").removeClass('hidden');
            $("#invoice_no").html("");
            $.post('{{ url('acc/get_invoice_no') }}',{security:$("#pay_to").val()},function(data){
                $.each(data,function(i,val){
                    $("#invoice_no").append('<option value='+val.id+'>'+val.invoice_no+'</option>');
                });
                $("#invoice_no").prepend('<option selected disabled>select an option</option>');
                $("#invoice_no").removeAttr('disabled');
                $("#loading").addClass('hidden');
                $(".input-form").val("")
            })
        })

        $("#invoice_no").change(getDataFromInvoiceNo);

    });

    function task(){
        $(".task").change(function(){
            var value = $(".task").val();
            switch(value){
                case 'premium_receipt':
                    $(".bank").html("");
                    for(var i = 0; i<receipt.length; i++){
                        $(".bank").append('<option value='+receipt[i][0]+'>'+receipt[i][1]+'</option>');
                    }
                    break;
                case 'premium_payment':
                    $(".bank").html("");
                    for(var i = 0; i<receipt.length; i++){
                        $(".bank").append('<option value='+payment[i][0]+'>'+payment[i][1]+'</option>');
                    }
                    break;
            }
        })
    }

    function getDataFromInvoiceNo(){
        var input = $("#invoice_no").val();
        $.post('{{ url('acc/request_data_from_invoice_no') }}',{inv:input},function(data){
            $("#from_insured").val(data.from);
            $("#gross").val(data.gross);
            $("#installment_count").val(data.installment);
        })
    }
</script>
@endsection

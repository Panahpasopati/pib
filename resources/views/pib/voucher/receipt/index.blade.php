@extends('layouts.pib')
@section('content')
        <style>.sorting-item{display: none;}</style>
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
                <h3 class="page-title">
                    <a type="button" href="{{ route('premium_receipt_voucher.create') }}" class="btn btn-default"><i class="fa fa-plus-square"></i> Create</a>
                </h3>
                <div class="row">
                    <div class="col-md-2 col-md-offset-10">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#modal-sortir">Filter</button>
                        <a href="{{ route('premium_receipt_voucher.index') }}" class="btn btn-sm">Clear</a>
                    </div>
                <div class="col-md-12">
                    <!-- BORDERED TABLE -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Receipt Vouchers List</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th class="th-sorting sorting-item"><input type="checkbox" class="checkbox" id="select-all"></th>
                                    <th>No.</th>
                                    <th>Voucher No.</th>
                                    <th>Vessels</th>
                                    <th>Invoice No.</th>
                                    <th>Installment</th>
                                    <th>Amount</th>
                                    <th>Received Date</th>
                                    <th>Insured</th>
                                    <th>Created At</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <form method="post" id="form-create-" accept-charset="utf-8" action="{{ url('voucher','create_premium_payment') }}">
                                    {{ csrf_field() }}
                                    @if(isset($_GET['assured']))
                                        <input type="hidden" name="security" value="{{ $_GET['assured'] }}">
                                    @endif
                                @foreach($data as $i => $d)
                                    <tr>
                                        <td class="th-sorting sorting-item"><input type="checkbox" class="checkbox checkbox-select" name="premium_receipt_id[{{ $i }}]" value="{{ $d->id }}"></td>
                                        <td>{{ $i + 1}}</td>
                                        <td>{{ $d->voucher_no }}</td>
                                        <td>{{  formatVesselsName($d->Quotes->Vessels) }}</td>
                                        <td><a href="{{ route('invoice.show',$d->Invoice->id) }}">{{ $d->Invoice->invoice_no }}</a></td>
                                        <th>{{ $d->Installment->installment_no }} /{{ $d->Invoice->installment_count }}</th>
                                        <td>{{ number_format($d->amount) }}</td>
                                        <td>{{ date('d-m-Y',strtotime($d->received_date)) }}</td>
                                        <td>@foreach($d->Quotes->insurers as $insurer) &bullet; {{ $insurer->Insurer->insured }} <br>@endforeach</td>
                                        <td>{{ date('d M Y H:i',strtotime($d->created_at)) }} WIB</td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-link " title="Lihat receipt voucher" href="{{ route('premium_receipt_voucher.show',$d->id) }}"><i class="fa fa-eye-slash"></i> </a>
                                                <a onclick="generateFile({{ $d->id }})" class="btn btn-link" title="Generate File"><span class="fa fa-arrow-right"></span></a>
                                                <a class="btn btn-link" {{ ($d->file) ? 'href='.env('DOCUMENT_GENERATED_URL') .'vouchers/'. $d->file.'' : 'disabled' }} title="Download File"><span class="fa fa-download"></span></a>
                                            @if($d->PaymentVoucher)
                                                    <a class="btn btn-link" title="Lihat payment voucher" href="{{ route('premium_payment_voucher.show',$d->PaymentVoucher->payment_voucher_id) }}">PV</a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </form>
                                </tbody>
                            </table>
                            @if($paginate)
                                {{ $data->render() }}
                                @endif
                        </div>
                    </div>
                    <!-- END BORDERED TABLE -->
                </div>
            </div>
        </div>
    </div>
    <div id="modal-sortir" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Filter Data</h4>
                </div>
                <div class="modal-body">
                    <form method="get" action="{{ route('premium_receipt_voucher.index') }}">
                        <div class="form-group">
                            <label class="control-label">Assured</label>
                            <select name="assured" class="form-control" required>
                                <option disabled selected>--Choose an Option--</option>
                                @foreach($assured as $a)
                                    <option @if(isset($_GET['assured'])) @if($_GET['assured'] == $a->id) selected @endif @endif value="{{ $a->id }}">{{ $a->assured_name }}</option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Rows</label>
                            <select name="rows" class="form-control" required>
                                <option disabled selected>--Choose an Option--</option>
                                <option @if(isset($_GET['rows'])) @if($_GET['rows'] == 10) selected @endif @endif  value="10">10</option>
                                <option @if(isset($_GET['rows'])) @if($_GET['rows'] == 20) selected @endif @endif  value="20">20</option>
                                <option @if(isset($_GET['rows'])) @if($_GET['rows'] == 30) selected @endif @endif  value="30">30</option>
                                <option @if(isset($_GET['rows'])) @if($_GET['rows'] == 40) selected @endif @endif  value="40">40</option>
                                <option @if(isset($_GET['rows'])) @if($_GET['rows'] == 50) selected @endif @endif  value="50">50</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label"></label>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- END MAIN CONTENT -->
    <script>
        function generateFile(id){
            $.ajax({
                url:  '{{ env('DOCUMENT_GENERATOR_URL') }}voucher/receipt.php',
                type: 'POST',
                async: false,
                data: {id:id},
                cache: false,
                success: function(data){
                    alert('Generated document with status : success');
                    location.reload();
                },
                error: function(){}
            });
        }
    </script>
</div>
<!-- END MAIN -->
@endsection
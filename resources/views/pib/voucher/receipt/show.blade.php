@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h4 class="page-title">Premium Receipt Voucher</h4>
            <form>
                <div>
                    <div class="col-md-12">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label class="control-label col-md-4">Debit Note No.</label>
                                <div class="col-md-6">
                                    <input type="text" name="" class="form-control" readonly value="{{ $data->Invoice->invoice_no }}">
                                </div>
                            </div>
                            <input type="hidden" name="quote_id" value="">
                            <input type="hidden" name="invoice_id" value="">
                            <div class="form-group">
                                <label class="control-label col-md-4">From/Insured</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="insured" readonly value="{{ $data->Quotes->Security->assured_name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">To/Insurer</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" rows="3" readonly name="insurer">@foreach($data->Quotes->Insurers as $insurer) -{{ $insurer->Insurer->insured }} @endforeach</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Amount</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" readonly name="amount" required value="{{ number_format($data->amount) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">In Words</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" readonly name="amount_in_words" rows="4" required>{{ $data->amount_in_words }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <table class="table table-bordered" width="100%">
                                <tbody>
                                <tr>
                                    <td colspan="4"><strong>Premium Receipt Voucher</strong></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Voucher No.</td>
                                    <td colspan="2"><input type="text" class="form-control" name="voucher_no" required readonly value="{{ $data->voucher_no }}"> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Received Date</td>
                                    <td colspan="2"><input type="text" class="form-control" readonly name="received_date" required value="{{ $data->received_date }}"> </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td width="25%"><input type="text" class="form-control" disabled value="Transfer"> </td>
                                    <td&nbsp; </td>
                                    <td><input type="text" class="form-control" name="bank" required readonly value=""> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Currency</td>
                                    <td colspan="2"><input type="text" class="form-control" name="currency" required readonly value="{{ $data->Quotes->currency }}"> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Policy No.</td>
                                    <td colspan="2"><input type="text" class="form-control" name="policy_no" readonly value="{{ $data->policy_no }}"> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Ship Name</td>
                                    <td colspan="2"><input type="text" class="form-control" name="ship_name" required value="{{ formatVesselsName($data->Quotes->Vessels) }}"> </td>
                                </tr>
                                <tr>
                                    <td>Installment #</td>
                                    <td><input class="form-control" name="installment_id" required readonly value="{{ $data->Installment->installment_no }}"> </td>
                                    <td>/ </td>
                                    <td><input type="text" class="form-control" name="installment_count" required readonly value="{{ $data->Invoice->installment_count }}"> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Due Date</td>
                                    <td colspan="2"><input type="text" class="form-control" name="due_date" required value="{{ $data->Installment->installment_no }}" readonly > </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-4">Gross Premium</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="gross_premium" required readonly value="{{ number_format($data->gross_premium) }}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Client Discount</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="client_discount" value="0">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Third Party Commission</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="third_party_commision" value="0">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">VAT</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="vat" value="0">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"> WHT</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="wht" value="0">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"> Others</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="others" value="0">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">  Bank Charges</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="bank_charges" value="0">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">  Net Received</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="net_received" required readonly value="{{ number_format($data->net_received) }}">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <textarea rows="4" class="form-control" name="accounting_entries" placeholder="Accounting Entries">{{ $data->accounting_entries }}</textarea>
                        </div>
                        <div class="col-md-4">
                            <table class="table" width="100%">
                                <tbody>
                                <tr>
                                    <td>Prepared by:</td>
                                    <td>Approved by:</td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" readonly class="form-control" value="{{ $data->PreparedBy->name }}">
                                    </td>
                                    <td>
                                        <input type="text" readonly class="form-control" value="{{ $data->ApprovedBy->name }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td><div class="input-group"><span class="input-group-addon">Date:</span>  <input type="date" name="prepared_by_date" readonly value="{{ date('d M Y',strtotime($data->created_at)) }}" class="form-control"> </div></td>
                                    <td><div class="input-group"><span class="input-group-addon">Date:</span>  <input type="date" name="approve_date" class="form-control"> </div></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <hr><br>
                    <hr>
                </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- /.container-fluid-->

@endsection

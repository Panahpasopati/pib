@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <h4 class="page-title">Premium Receipt Voucher</h4>
                    <form method="post" class="form-horizontal" action="{{ route('premium_receipt_voucher.store') }}">
                        {{ csrf_field() }}
                        <div>
                            <div class="col-md-12">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Debit Note No.</label>
                                        <div class="col-md-6">
                                            <select name="invoice_no" id="receipt_invoices_list" class="form-control coa" required>
                                                <option value="" disabled selected>--Choose an Option--</option>
                                                @foreach($invoices as $inv)
                                                    <option value="{{ $inv->id }}">{{ $inv->invoice_no }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" name="quote_id" value="">
                                    <input type="hidden" name="invoice_id" value="">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">From/Insured</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="insured">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">To/Insurer</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="insurer">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Amount</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="amount" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">In Words</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" name="amount_in_words" rows="4" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <table class="table table-bordered" width="100%">
                                        <tbody>
                                        <tr>
                                            <td colspan="4"><strong>Premium Receipt Voucher</strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Voucher No.</td>
                                            <td colspan="2"><input type="text" class="form-control" name="voucher_no" value="{{ $newRef }}" required> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Received Date</td>
                                            <td colspan="2"><input type="text" class="form-control datepicker" name="received_date" required> </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td width="25%"><input type="text" class="form-control" disabled value="Transfer"> </td>
                                            <td&nbsp; </td>
                                            <td><input type="text" class="form-control" name="bank" required> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Currency</td>
                                            <td colspan="2"><input type="text" class="form-control" name="currency" required> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Policy No.</td>
                                            <td colspan="2"><input type="text" class="form-control" name="policy_no"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Ship Name</td>
                                            <td colspan="2"><input type="text" class="form-control" name="ship_name" required> </td>
                                        </tr>
                                        <tr>
                                            <td>Installment #</td>
                                            <td><select class="form-control" name="installment_id" required></select> </td>
                                            <td>/ </td>
                                            <td><input type="text" class="form-control" name="installment_count" required> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Due Date</td>
                                            <td colspan="2"><input type="text" class="form-control" name="due_date" required> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Gross Premium</label>
                                    <div class="col-md-2">
                                        <input type="hidden" class="form-control" name="gross_premium" required>
                                        <input type="text" class="form-control" id="gross_premium" required>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Client Discount</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="client_discount" value="0">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Third Party Commission</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="third_party_commision" value="0">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">VAT</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="vat" value="0">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4"> WHT</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="wht" value="0">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4"> Others</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="others" value="0">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">  Bank Charges</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="bank_charges" value="0">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">  Net Received</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="net_received" required>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <textarea rows="4" class="form-control" name="accounting_entries" placeholder="Accounting Entries"></textarea>
                                </div>
                                <div class="col-md-4">
                                    <table class="table" width="100%">
                                        <tbody>
                                        <tr>
                                            <td>Prepared by:</td>
                                            <td>Approved by:</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select name="prepared_by" class="form-control coa" required>
                                                    @foreach($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                        @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <select name="approved_by" class="form-control coa" required>
                                                    @foreach($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><div class="input-group"><span class="input-group-addon">Date:</span>  <input type="date" name="prepared_by_date" readonly value="{{ date('d M Y') }}" class="form-control"> </div></td>
                                            <td><div class="input-group"><span class="input-group-addon">Date:</span>  <input type="date" name="approve_date" class="form-control"> </div></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr><br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4" style="margin-left: 10px;margin-top: 15px;">
                                        <div class="form-group">
                                            <button class="btn btn-primary" type="submit">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <!-- /.container-fluid-->

@endsection

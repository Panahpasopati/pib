@extends('layouts.pib')
@section('content')
    <style>
        .panel-tabs {
            display: none;
        }
        .panel-tabs {
            animation: fadeEffect 1s; /* Fading effect takes 1 second */
        }
        #filter .panel-body{
            padding: 5px;
        }
        #filter ul{
            margin-top: 5px;
            padding: 0px;
        }
        #filter ul li{
            display: inline;
            padding: 5px;
            margin: 5px 10px;
            border-bottom: 2px solid transparent;
            list-style:	none;
            transition-duration: 0.5s;
            -webkit-transition-duration: 0.5s;
            -moz-transition-duration: 0.5s;
            -o-transition-duration: 0.5s;
        }
        #filter ul li:hover, #filter ul li.active{
            border-color: inherit;
        }
        a.tab-link.active {
            /* border-color: inherit; */
            color: #2196f3;
            border-bottom: 6px solid;
            border-radius: 10px 1px;
        }
    </style>
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                            <!--
                !-->
                    <h4 class="page-title">PR</h4>
                    <form method="post" class="form-horizontal" action="">
                        {{ csrf_field() }}
                        <div>
                            <div class="col-md-12">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <img src="{{ asset('assets/img/pib.png') }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">From/Insured</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="from_insurer">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">To/Insurer</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="to_insurer">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Debit Note No.</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="debit_note_no">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Amount</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="ammount">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">In Words</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" name="in_words" rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <table class="table table-bordered" width="100%">
                                        <tbody>
                                        <tr>
                                            <td colspan="4">Premium Receipt Voucher</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Voucher No.</td>
                                            <td colspan="2"><input type="text" class="form-control" name="voucher_no"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Received Date</td>
                                            <td colspan="2"><input type="text" class="form-control" name="received_date"> </td>
                                        </tr>
                                        <tr>
                                            <td>Paid by</td>
                                            <td width="25%"><input type="text" class="form-control" name="paid_by"> </td>
                                            <td>from </td>
                                            <td><input type="text" class="form-control" name="paid_from"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Currency</td>
                                            <td colspan="2"><input type="text" class="form-control" name="currency"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Policy No.</td>
                                            <td colspan="2"><input type="date" class="form-control" name="policy_no"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Ship Name</td>
                                            <td colspan="2"><input type="text" class="form-control" name="ship_name"> </td>
                                        </tr>
                                        <tr>
                                            <td>Installment #</td>
                                            <td><input type="text" class="form-control" name="installment"> </td>
                                            <td>/ </td>
                                            <td><input type="text" class="form-control" name="installment_count"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Due Date</td>
                                            <td colspan="2"><input type="date" class="form-control" name="due_date"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Gross Premium</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="gross_premium">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Client Discount</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="client_discount">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Third Party Commission</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="third_party_commision">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">VAT</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="vat">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4"> WHT</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="wht">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4"> Others</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="others">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">  Bank Charges</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="bank_charges">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">  Net Received</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="net_received">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <textarea rows="4" class="form-control" name="accounting_entries" placeholder="Accounting Entries"></textarea>
                                </div>
                                <div class="col-md-4">
                                    <table class="table" width="100%">
                                        <tbody>
                                        <tr>
                                            <td>Prepared by:</td>
                                            <td>Approved by:</td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" name="prepared_by" class="form-control"> </td>
                                            <td><input type="text" name="approved_by" class="form-control"> </td>
                                        </tr>
                                        <tr>
                                            <td><div class="input-group"><span class="input-group-addon">Date:</span>  <input type="date" name="prepared_by_date" class="form-control"> </div></td>
                                            <td><div class="input-group"><span class="input-group-addon">Date:</span>  <input type="date" name="approved_by_date" class="form-control"> </div></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr><br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4" style="margin-left: 10px;margin-top: 15px;">
                                        <div class="form-group">
                                            <button class="btn btn-primary" type="submit">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <!-- /.container-fluid-->

@endsection

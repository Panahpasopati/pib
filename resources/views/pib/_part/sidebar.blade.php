<nav>
    <ul class="nav">
        <li><a href="{{ url('/')  }}" class="active"><i class="lnr lnr-home"></i> <span>Home</span></a></li>
        <li>
            <a href="#sm" data-toggle="collapse" class="{{ Request::segment(1) == 'sys' ? 'active' : 'collapsed' }}"><i class="lnr lnr-file-empty"></i> Sys Management <i class="icon-submenu lnr lnr-chevron-down"></i></a>
            <div id="sm" class="collapse {{ Request::segment(1) == 'sys' ? 'in' : '' }}">
                <ul class="nav">
                    <li><a href="{{ route('channel.index') }}" class="">Sales & Marketing</a></li>
                    <!-- <li><a href="{{ url('clients_list') }}" class="">Customer</a></li> !-->
                    <li><a href="{{ route('assured.index') }}" class="">Assured</a></li>
                    <li><a href="{{ route('manage_user.index') }}" class="">Users</a></li>
                    <li><a href="{{ route('coa.index') }}" class="">COA</a></li>
                    <li><a href="{{ route('insurer.index') }}" class="">Insurer</a></li>
                    <li><a href="{{ route('manage_type.index') }}" class="">Manage Type</a></li>
                    <li><a href="{{ route('currency.index') }}" class="">Currency</a></li>
                    <li><a href="{{ route('banks.index') }}" class="">Bank</a></li>
                </ul>
            </div>
        </li>
        <li>
            <a href="{{ route('quote.index') }}"><i class="lnr lnr-file-empty"></i> Quotes </a>
        </li>
        <li>
            <a data-toggle="collapse" class="{{ Request::segment(1) == 'inv' ? 'active' : 'collapsed' }}"  href="#inv"><i class="lnr lnr-file-empty"></i> Invoicing <i class="icon-submenu lnr lnr-chevron-down"></i></a>
            <div id="inv" class="collapse {{ Request::segment(1) == 'inv' ? 'in' : '' }}">
                <ul class="nav">
                    <li><a href="{{ url('special',['direct-invoice','create']) }}">Direct Invoice</a> </li>
                    <li><a href="{{ route('invoice.index') }}">Invoices List</a> </li>
                    <li><a href="{{ url('endorsements_list') }}">Endorsements List</a> </li>
                </ul>
            </div>
        </li>
        <li>
            <a data-toggle="collapse" class="{{ Request::segment(1) == 'voucher' ? 'active' : 'collapsed' }}"  href="#acc"><i class="lnr lnr-file-empty"></i> Vouchers <i class="icon-submenu lnr lnr-chevron-down"></i></a>
            <div id="acc" class="collapse {{ Request::segment(1) == 'voucher' ? 'in' : '' }}">
                <ul class="nav">
                    <li><a href="{{ route('premium_receipt_voucher.index') }}">Premium Receipt</a> </li>
                    <li><a href="{{ route('premium_payment_voucher.index') }}">Premium Payment</a> </li>
                </ul>
            </div>
        </li>
        <li>
            <a href=""><i class="lnr lnr-file-empty"></i>Claims</a>
        </li>
        <li>
            <a href="{{ url('reports') }}"><i class="lnr lnr-file-empty"></i> Reports</a>
        </li>
    </ul>
</nav>
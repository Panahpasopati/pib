@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="page-title">Edit Assured</h4>
                    {!! Form::model($data, ['method' => 'PATCH','route' => ['assured.update', $data->id], 'enctype'=>'multipart/form-data']) !!}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Assured Name</label>
                            <input type="text" class="form-control" name="assured_name" value="{{ $data->assured_name }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Address</label>
                            <textarea class="form-control" name="address" rows="3">{{ $data->address }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Description</label>
                            <textarea class="form-control" name="description" rows="3">{{ $data->description }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">NPWP</label>
                            <input type="text" class="form-control" name="npwp" value="{{ $data->npwp }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Phone</label>
                            <input type="text" class="form-control" name="text" value="{{ $data->phone }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Fax</label>
                            <input type="text" class="form-control" name="fax" value="{{ $data->fax }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control" name="email" value="{{ $data->email }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Website</label>
                            <input type="text" class="form-control" name="website" value="{{ $data->website }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Brokerage</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="brokerage" required="" value="{{ $data->brokerage }}">
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->

@endsection
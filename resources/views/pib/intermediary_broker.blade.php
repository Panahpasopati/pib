@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                    <h3 class="page-title"><a type="button" href="{{ url('intermediary_broker_create') }}" class="btn btn-default"><i class="fa fa-plus-square"></i> New Broker </a></h3>
                    <div class="row">
                    <div class="col-md-12">
                        <!-- BORDERED TABLE -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Intermediary Brokers List</h3>
                                <div class="pull-right">
                                    <form method="get" class="form-inline" action="{{ url('intermediary_list/search_rows') }}">
                                        <input type="text" class="form-control" name="query" placeholder="search....">
                                        <input type="hidden" class="form-control" name="identity" value="intermediary_broker">
                                        <button type="submit" class="btn btn-default"><span class="fa fa-search"></span> </button>
                                    </form>
                                    <br>
                                </div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Broker</th>
                                        <th>Brokerage</th>
                                        <th>Add Comm</th>
                                        <th>Invoicing</th>
                                        <th>NPWP</th>
                                        <th>Alamat</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $i => $d)
                                        <tr>
                                            <td><small>{{ $i + 1}}</small></td>
                                            <td><small>{{ $d->broker }}</small></td>
                                            <td><small>{{ $d->brokerage }}</small></td>
                                            <td><small>{{ $d->add_comm }}</small></td>
                                            <td><small>{{ $d->invoicing }}</small></td>
                                            <td><small>{{ $d->npwp }}</small></td>
                                            <td><small>{{ $d->alamat }}</small></td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-primary" href="{{ url('intermediary_broker_edit',$d->id) }}"><span class="fa fa-edit"></span></a>
                                                    <a class="btn btn-danger" href="{{ url('intermediary_broker_delete',$d->id) }}"><span class="fa fa-trash"></span></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $data->render() }}
                            </div>
                        </div>
                        <!-- END BORDERED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

    <!-- END MAIN -->
@endsection
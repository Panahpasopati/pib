@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h4 class="page-title">New Item Quote</h4>
            <form method="post" action="{{ route('type_quo.update',$data->id) }}">
                <input type="hidden" name="_method" value="PATCH">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Type Quote</label>
                                <select name="id_type" class="form-control" required>
                                   <option value="{{ $data->id_type }}" selected>{{ $data->Type->name }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="height: 300px;overflow-x: scroll;margin-bottom: 20px;" >
                        <table class="table table-striped table-responsive table-bordered" style="margin-left: 15px;">
                            <thead>
                            <tr>
                                <th>Item</th>
                                <th><input type="checkbox" id="select-all" onchange="select_all()" style="margin-left: 40%;height: 14px;width: 14px;" class="form-control"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $i => $item)
                                <tr>
                                    <td>
                                        {{ $item->name }}
                                    </td>
                                    <td align="center"><input type="checkbox" {{ in_array($item->id,$form_items) ? 'checked' : '' }} name="chosen_item[{{ $i }}]" value="{{ $item->id }}" style="height: 14px;width: 14px;" class="checkbox form-control"> </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <button type="submit" style="margin-left: 20px;" class="btn btn-primary btn-md">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- /.container-fluid-->
<script>
    function select_all(){
        if($("#select-all").is(":checked")){
            $(".checkbox").attr('checked', true);
        }else{
            $(".checkbox").removeAttr('checked');
        }
        return false;
    }
</script>


@endsection

@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="page-title">Add New User</h4>
                {!! Form::model($data, ['method' => 'PATCH','route' => ['manage_user.update', $data->id], 'enctype'=>'multipart/form-data']) !!}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Name</label>
                            <input type="text" class="form-control" name="name" value="{{ $data->name }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control" name="email" value="{{ $data->email }}" readonly>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Username</label>
                            <input type="text" class="form-control" name="username" value="{{ $data->username }}" readonly>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Departement</label>
                            <input type="text" class="form-control" name="departement" value="{{ $data->departement }}">
                        </div>
                        <select name="hak_akses" class="form-control" required>
                            <option value="" disabled selected>--Choose--</option>
                            <option {{ $data->hak_akses == 'admin' ? 'selected' : ''  }}value="admin">Admin</option>
                            <option {{ $data->hak_akses == 'user1' ? 'selected' : ''  }}value="user1">User1</option>
                            <option {{ $data->hak_akses == 'user2' ? 'selected' : '' }}value="user2">User2</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->

@endsection
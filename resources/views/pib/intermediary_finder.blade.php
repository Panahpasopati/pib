@extends('layouts.pib')
@section('content')
    <style>
        table.ndj{
            margin: -8px;
            width: 103% !important;
        }

        table.ndj td{
            padding:8px;
        }
    </style>
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                    <h3 class="page-title"><a type="button" href="{{ url('intermediary_finder_create') }}" class="btn btn-default"><i class="fa fa-plus-square"></i> New Intermediary Finder </a></h3>
                    <div class="row">
                    <div class="col-md-12">
                        <!-- BORDERED TABLE -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Assured Agent</h3>
                                <div class="pull-right">
                                    <form method="get" class="form-inline" action="{{ url('intermediary_list/search_rows') }}">
                                        <input type="text" class="form-control" name="query" @if(isset($query)) value="{{ $query }}" @endif placeholder="search....">
                                        <input type="hidden" class="form-control" name="identity" value="intermediary_finder">
                                        <button type="submit" class="btn btn-default"><span class="fa fa-search"></span> </button>
                                    </form>
                                    <br>
                                </div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Finder</th>
                                        <th>Assured</th>
                                        <th>Comm</th>
                                        <th>Remark</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $i => $d)
                                        <tr>
                                            <td>{{ $i + 1}}</td>
                                            <td>{{ $d->finder }}</td>
                                            <? $comm = explode(",",$d->comm); $remark = explode(",",$d->remark); ?>
                                            <td>
                                                <table class="ndj">
                                                    @foreach(explode(",",$d->assured) as $x => $item)
                                                        <tr>
                                                            <td>{{ $item}}</td>
                                                        </tr>
                                                        @endforeach
                                                </table>
                                            </td>
                                            <td>
                                                <table class="ndj">
                                                    @foreach($comm as $item)
                                                        <tr>
                                                            <td>{{ ($item) ? $item : 'N/A'}}</td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                            <td>
                                                <table class="ndj">
                                                    @foreach($remark as $item)
                                                        <tr>
                                                            <td>{{ ($item) ? $item : 'N/A' }}</td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-primary" href="{{ url('intermediary_finder_edit',$d->fid) }}"><span class="fa fa-edit"></span></a>
                                                    <a class="btn btn-danger" href="{{ url('intermediary_finder_delete',$d->fid) }}"><span class="fa fa-trash"></span></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $data->render() }}
                            </div>
                        </div>
                        <!-- END BORDERED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

    <!-- END MAIN -->
@endsection
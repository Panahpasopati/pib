@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                    <h3 class="page-title"><a type="button" href="{{ url('create_new_client') }}" class="btn btn-default"><i class="fa fa-plus-square"></i> New Client </a></h3>
                    <div class="row">
                    <div class="col-md-12">
                        <!-- BORDERED TABLE -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Clients List</h3>
                                <div class="pull-right">
                                    <form method="get" class="form-inline" action="{{ url('clients_list/search_rows') }}">
                                        <input type="text" class="form-control" name="query" @if(isset($query)) value="{{ $query }}" @endif placeholder="search....">
                                        <button type="submit" class="btn btn-default"><span class="fa fa-search"></span> </button>
                                    </form>
                                    <br>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Client Name</th>
                                        <th>Alamat</th>
                                        <th>Telpon</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $i => $d)
                                        <tr>
                                            <td>{{ $i + 1}}</td>
                                            <td>{{ $d->client_name }}</td>
                                            <td>{{ $d->alamat }}</td>
                                            <td>{{ $d->telpon }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-default" onclick="viewDetail({{ $d->id }})"><span class="fa fa-eye-slash"></span> View Detail</a>
                                                    <a class="btn btn-primary" href="{{ url('client_edit',$d->id) }}"><span class="fa fa-edit"></span></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $data->render() }}
                            </div>
                        </div>
                        <!-- END BORDERED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Client Insureds</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Insured Name</th>
                                <th>Insured Address</th>
                                <th>Telp</th>
                                <th>PIC</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody class="insured_table_content">

                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function viewDetail(id){
            $("#myModal").modal('show');
            $('.modal-footer').html("");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: 'request_client_insureds',
                data: {id: id},
                success: function (data) {
                    $('.insured_table_content').html("");
                    $.each(data,function(i,val){
                        var html = '<tr><td>'+parseInt(i + 1)+'</td><td>'+val.insured_name+'</td><td>'+val.insured_address+'</td><td>'+val.telp+'</td><td>'+val.pic+'</td><td>'+val.email+'</td></tr>';
                        $('.insured_table_content').append(html);
                    });
                    $('.modal-footer').append('<a class="btn btn-primary" href="client_edit/'+id+'"><span class="fa fa-edit"></span></a> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>')
                }
            })
        }
    </script>
    <!-- END MAIN -->
@endsection
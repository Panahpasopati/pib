@extends('layouts.pib')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <h4 class="page-title">Edit Client</h4>
            <form method="post" action="{{ url('update_client',$data->id) }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Client Name</label>
                                <input type="text" name="client_name" class="form-control" value="{{ $data->client_name }}" required="">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Client Address</label>
                                <textarea name="alamat" class="form-control" required="" rows="6">{{ $data->alamat }}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Client Phone</label>
                                <input type="tel" name="telpon" class="form-control" value="{{ $data->tel }}" required="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <table style="margin-left: 20px;" class="table table-condensed">
                            <thead>
                            <tr>
                                <th width="40%">Insured Name</th>
                                <th width="30%">Insured Address</th>
                                <th width="10%">Phone</th>
                                <th width="10%">PIC</th>
                                <th width="10%">Email</th>
                                <th></th>
                            </tr>
                            </thead>
                            <!--elemet sebagai target append-->
                            <tbody id="itemlist">
                            @foreach($client_insurances as $i => $val)
                            <tr id="row-{{ $i }}">
                                <td>
                                    <input name="insured_name[{{ $i }}]" class="form-control" required="" value="{{ $val->insured_name }}" type="text">
                                </td>
                                <td>
                                    <textarea name="insured_address[{{ $i }}]" class="form-control" rows="1" >{{ $val->insured_address }}</textarea>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="phone[{{ $i }}]" value="{{ $val->phone }}">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="pic[{{ $i }}]" value="{{ $val->pic }}">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="email[{{ $i }}]" value="{{ $val->email }}">
                                </td>
                                <td><button type="button" class="btn btn-small btn-default" onclick="deleteRow({{ $i }})"><i class="fa fa-trash"></i></button></td>
                            </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="4"></td>
                                <td>
                                    <button class="btn btn-small btn-default" type="button" onclick="additem(); return false"><i class="fa fa-plus"></i></button>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-md">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- /.container-fluid-->
<script>
    function deleteRow(id){
        $("#row-"+id).remove();
    }
    var i = {{ count($client_insurances) }};
    function additem() {
        var itemlist = document.getElementById('itemlist');

        var row = document.createElement('tr');
        var b = document.createElement('td');
        var c = document.createElement('td');
        var d = document.createElement('td');
        var e = document.createElement('td');
        var f = document.createElement('td');
        var aksi = document.createElement('td');
        itemlist.appendChild(row);
        row.appendChild(b);
        row.appendChild(c);
        row.appendChild(d);
        row.appendChild(e);
        row.appendChild(f);
        row.appendChild(aksi);
        var insured_name = document.createElement('input');
        insured_name.setAttribute('name', 'insured_name[' + i + ']');
        insured_name.setAttribute('class', 'form-control');
//
        var insured_address = document.createElement('input');
        insured_address.setAttribute('name', 'insured_address[' + i + ']');
        insured_address.setAttribute('class', 'form-control');
//
        var insured_telp = document.createElement('input');
        insured_telp.setAttribute('name', 'telp[' + i + ']');
        insured_telp.setAttribute('class', 'form-control');
//
        var insured_pic = document.createElement('input');
        insured_pic.setAttribute('name', 'pic[' + i + ']');
        insured_pic.setAttribute('class', 'form-control');
//
        var insured_email = document.createElement('input');
        insured_email.setAttribute('name', 'email[' + i + ']');
        insured_email.setAttribute('class', 'form-control');
//
//
        var hapus = document.createElement('span');
        b.appendChild(insured_name);
        c.appendChild(insured_address);
        d.appendChild(insured_telp);
        e.appendChild(insured_pic);
        f.appendChild(insured_email);
        aksi.appendChild(hapus);
        hapus.innerHTML = '<button type="button" class="btn btn-small btn-default"><i class="fa fa-trash"></i></button>';
//                membuat aksi delete element
        hapus.onclick = function () {
            row.parentNode.removeChild(row);
        };


        i++;
    }
</script>



@endsection

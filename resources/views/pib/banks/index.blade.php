@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <h3 class="page-title"><a type="button" href="{{ route('banks.create') }}" class="btn btn-default"><i class="fa fa-plus-square"></i> New Data </a></h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- BORDERED TABLE -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Banks DataSet</h3>
                        </div>

                        <div class="panel-body">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Bank</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $i => $d)
                                    <tr>
                                        <td>{{ $i + 1}}</td>
                                        <td>{{ $d->bank_name }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-primary" href="{{ route('banks.edit',$d->id) }}"><span class="fa fa-edit"></span> Edit</a>
                                                <form method="post" action="{{ route('banks.destroy',$d->id)  }}" style="display:inline">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    {{ csrf_field() }}
                                                    <button type="submit" onclick="return confirm('are you sure?');" class="btn btn-danger"><span class="fa fa-trash-o"></span> Delete</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $data->render() }}
                        </div>
                        <!-- END BORDERED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
</div>
@endsection
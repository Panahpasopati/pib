@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="page-title">Add New Broker</h4>
                <form method="post" action="{{ url('intermediary_broker_update') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Broker</label>
                                <input type="text" class="form-control" name="broker" value="{{ $data->broker }}">
                                <input type="text" class="form-control" name="id" value="{{ $data->id }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Brokerage</label>
                                <input type="text" class="form-control" name="brokerage" value="{{ $data->brokerage }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Add Comm</label>
                                <input type="text" class="form-control" name="add_comm" value="{{ $data->add_comm }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">NPWP</label>
                                <input type="text" class="form-control" name="npwp" value="{{ $data->npwp }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Alamat</label>
                                <input type="text" class="form-control" name="alamat" value="{{ $data->alamat }}">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->

@endsection
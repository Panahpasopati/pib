@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                    <h3 class="page-title"><a type="button" href="{{ url('new_endorsement') }}" class="btn btn-default"><i class="fa fa-plus-square"></i> New Endorsement </a></h3>
                    <div class="row">
                    <div class="col-md-12">
                        <!-- BORDERED TABLE -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Endorsements List</h3>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Assured</th>
                                        <th>Quote Ref.</th>
                                        <th>Invoice No.</th>
                                        <th>Premi Change</th>
                                        <th>Document</th>
                                        <th>Date Created</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $i => $d)
                                        <tr>
                                            <td>{{ $i + 1}}</td>
                                            <td>{{ explode("-",$d->Quote->assured)[1] }}</td>
                                            <td>{{ $d->Quote->our_ref }}</td>
                                            <td>{{ $d->Invoice->invoice_no }}</td>
                                            <td>{{ ($d->is_premi_change) ? 'Yes' : 'No' }}</td>
                                            <td><a href="{{ url('storage/app/public/endorsement/'.$d->doc_endorsement) }}">Link</a></td>
                                            <td>{{ date('d M Y',strtotime($d->created_at)) }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    @if($d->is_premi_change)
                                                        <a href="{{ env('DOCUMENT_GENERATED_URL') . @$d->Invoice->invoice_file }}" title="Download Additional Invoice" class="btn btn-warning"><span class="fa fa-download"></span> </a>
                                                        <a onclick="generateAdditionalInvoice({{ $d->id }})" title="Generate Additional Invoice" class="btn btn-default"><span class="fa fa-book"></span> </a>
                                                        <a href="{{ $d->quote_type == 2 ? url('show_cargo_invoice',$d->id) : url('show_invoice',$d->id) }}" title="View Additional Invoice" class="btn btn-success"><span class="fa fa-eye-slash"></span> </a>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $data->render() }}
                            </div>
                        </div>
                        <!-- END BORDERED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <script>
        function generateAdditionalInvoice(id){

        }
    </script>
@endsection
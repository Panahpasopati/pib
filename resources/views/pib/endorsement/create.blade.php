@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="page-title">New Endorsement</h4>
                <form class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-4">Invoice Number : </label>
                                <div class="col-md-6">
                                    <select class="form-control select2">
                                        <option value="" selected disabled>Select an option</option>
                                        @foreach($data as $i)
                                            <option value="{{ $i->Quotes->id }}">{{ $i->invoice_no }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row output">

                    </div>
                </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <!-- /.container-fluid-->
    <script>
        $(document).ready(function(){
            $(".select2").select2({
                placeholder : 'select an option'
            })

            $(".select2").change(function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: 'request_data_invoice',
                    data: {id: $(".select2").val()},
                    success: function (data) {
                        $(".output").html("");
                        $(".output").html(data)
                    }
                })
            })
        })
    </script>


@endsection

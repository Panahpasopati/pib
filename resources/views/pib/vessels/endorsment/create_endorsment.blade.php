@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="page-title">Create Vessels Endorsement</h4>
                <table class="table table-responsive table-striped">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Vessel Name</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($vessels as $i => $item)
                        <tr>
                            <td>{{ $i + 1 }}</td>
                            <td>{{ $item->vessel_name }}</td>
                            <td><a class="btn btn-default" href="{{ url('vessel_endorsement_create_detail',$item->v_id) }}?quote={{ $data->quotes_id }}">Create</a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $vessels->render() }}
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <!-- /.container-fluid-->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Create endorsement</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Is Include Premium</label>
                        <input type="hidden" class="form-control" id="vessel_id" value="">
                        <input type="radio" class="radio-inline" name="include_premi" id="include_premi" value="True"> Yes
                        <input type="radio" class="radio-inline" name="include_premi" id="include_premi" value="No"> No
                    </div>
                    <div class="form-group">
                        <label class="control-label">endorsement</label>
                        <textarea class="form-control" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-default" onclick="saveendorsement()">Save</button>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function openForm(id){
            $("#myModal").modal('show');
            $("#vessel_id").val(id);
        }
    </script>



@endsection

@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="page-title">Endorsement</h4>
                <form method="post" action="{{ url('store_vessel_endorsement') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Assured</label>
                                    <div class="input-group">
                                        <input type="text" name="insured" class="form-control assured" value="{{ explode("-",$data->Quotes->assured)[1] }}" required="">
                                        <span type="button" onclick="generateEndorsement('assured')" class="input-group-addon"><span class="fa fa-check"></span> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Condition</label>
                                    <div class="input-group">
                                        <textarea class="form-control condition_" rows="5">{{ $data->Quotes->condition_ }}</textarea>
                                        <span type="button" onclick="generateEndorsement('condition_')" class="input-group-addon"><span class="fa fa-check"></span> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Vessel Name</label>
                                    <div class="input-group">
                                        <input type="text" name="insured" class="form-control vessel_name" value="{{ $detail->vessel_name }}" required="">
                                        <span type="button" onclick="generateEndorsement('vessel_name')" class="input-group-addon"><span class="fa fa-check"></span> </span>
                                    </div>
                                    <input type="hidden" name="id" class="form-control id" value="{{ $detail->id }}" required="">
                                    <input type="hidden" name="id_invoice" class="form-control" value="{{ $data->id }}" required="">
                                    <input type="hidden" name="id_quote" class="form-control" value="{{ $data->quotes_id }}" required="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Vessel Type</label>
                                    <div class="input-group">
                                        <input type="text" name="vessel_type" class="form-control vessel_type" value="{{ $detail->vessel_type }}">
                                        <span type="button" onclick="generateEndorsement('vessel_type')" class="input-group-addon"><span class="fa fa-check"></span> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Built</label>
                                    <div class="input-group">
                                        <input type="text" name="built" class="form-control built" value="{{ $detail->built }}">
                                        <span type="button" onclick="generateEndorsement('built')" class="input-group-addon"><span class="fa fa-check"></span> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">GT</label>
                                    <div class="input-group">
                                        <input type="text" name="gt" class="form-control gt" value="{{ $detail->gt }}">
                                        <span type="button" onclick="generateEndorsement('gt')" class="input-group-addon"><span class="fa fa-check"></span> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Flag</label>
                                    <div class="input-group">
                                        <input type="text" name="flag" class="form-control flag" value="{{ $detail->flag }}">
                                        <span type="button" onclick="generateEndorsement('flag')" class="input-group-addon"><span class="fa fa-check"></span> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Crew</label>
                                    <div class="input-group">
                                        <input type="text" name="crew" class="form-control crew" value="{{ $detail->gt }}">
                                        <span type="button" onclick="generateEndorsement('crew')" class="input-group-addon"><span class="fa fa-check"></span> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Port Of Registry</label>
                                    <div class="input-group">
                                        <input type="text" name="port_of_registry" class="form-control port_of_registry" value="{{ $detail->port_of_registry }}">
                                        <span type="button" onclick="generateEndorsement('port_of_registry')" class="input-group-addon"><span class="fa fa-check"></span> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Term</label>
                                    <div class="input-group">
                                        <input type="text" name="term" class="form-control term" value="{{ $detail->term }}">
                                        <span type="button" onclick="generateEndorsement('term')" class="input-group-addon"><span class="fa fa-check"></span> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Trading</label>
                                    <div class="input-group">
                                        <input type="text" name="trading" class="form-control trading" value="{{ $detail->trading }}">
                                        <span type="button" onclick="generateEndorsement('trading')" class="input-group-addon"><span class="fa fa-check"></span> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Is Premium Change?</label>
                                    <div class="well well-sm ">
                                        <div class="dlk-radio btn-group">
                                            <label class="btn btn-danger">
                                                <input name="premium_change" id="premium_change" class="form-control" type="radio" value="No" checked>
                                                <i class="fa fa-times glyphicon glyphicon-remove"></i> No
                                            </label>
                                            <label class="btn btn-primary">
                                                <input name="premium_change" id="premium_change" class="form-control" type="radio" value="Yes" >
                                                <i class="fa fa-check glyphicon glyphicon-ok"></i> Yes
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Document Endorsement</label>
                                    <input type="file" class="form-control" name="document_endorsement" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">History</label>
                                    <input type="hidden" name="selected_value" id="selected_value">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-md">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <script>
        function generateEndorsement(str){
            var ii = $("#selected_value").val();
            if(ii == ''){
                $("#selected_value").val(str)
            }else{
                $("#selected_value").val(ii+','+str)
            }
            alert('success:continue')
        }
    </script>
    <!-- END MAIN -->
    <!-- /.container-fluid-->
@endsection

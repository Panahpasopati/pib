@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <h4 class="page-title">Add Premium</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <? $additional = 1 ?>
                            <? $total = 0 ?>
                            @foreach($vessels as $i => $v)
                                <form id="form-{{ $v->premi_id }}" method="post">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Vessel Name</th>
                                            <th>Type</th>
                                            <th>Premium Type</th>
                                            <th>Annual Premium</th>
                                        </tr>
                                        </thead>
                                        <tbody id="{{ $v->premi_id }}">
                                        <tr>
                                            <td>{{ $v->vessel_name }}</td>
                                            <td>
                                                <input type="text" name="type_[0]" value="{{ $v->type_ }}" class="form-control">
                                                <input type="hidden" name="id_premi" value="{{ $v->premi_id }}">
                                            </td>
                                            <td><input type="text" name="premium_type[0]" value="{{ $v->premium_type }}" class="form-control"></td>
                                            <td><input type="text" name="annual_premium[0]" value="{{ $v->annual_premium }}" placeholder="" class="form-control"></td>
                                        </tr>
                                        @if(\App\Http\Controllers\EndorsementController::getAdditionalPremiumEndorsement($v->premi_id))
                                            @foreach(\App\Http\Controllers\EndorsementController::getAdditionalPremiumEndorsement($v->premi_id) as $x => $add)
                                                <tr>
                                                    <td></td>
                                                    <td><input type="text" name="type_[{{ $x + 1}}]" value="{{ $add->type_ }}" class="form-control"></td>
                                                    <td><input type="text" name="premium_type[{{ $x + 1 }}]" value="{{ $add->premium_type }}" class="form-control"></td>
                                                    <td><input type="text" name="annual_premium[{{ $x + 1 }}]" value="{{ $add->annual_premium }}" placeholder="" class="form-control"></td>
                                                    <input type="hidden" name="add_id[{{ $x + 1 }}]" value="{{ $add->id }}" placeholder="" class="form-control">
                                                    <td><button type="button" class="btn btn-small btn-default" onclick="deleteAdditional({{ $add->id }})"><i class="fa fa-trash"></i></button></td>
                                                </tr>
                                                <? $additional += 1 ?>
                                            @endforeach
                                        @endif
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td><button type="button" onclick="addRow({{ $v->premi_id }},{{ count(\App\Http\Controllers\EndorsementController::getAdditionalPremiumEndorsement($v->premi_id)) + 1 }})" class="btn btn-default">Add Row</button> </td>
                                            <td><button type="button" onclick="saveForm({{ $v->premi_id }})" class="btn btn-primary">Save</button> </td>
                                            <td>Sub-Total</td>
                                            <td><input type="text" class="form-control" id="subtotal" value="{{ $v->annual_premium + \App\Http\Controllers\EndorsementController::sumAnnualPremium($v->premi_id) }}"> </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </form>
                                <? $total += ($v->annual_premium + \App\Http\Controllers\EndorsementController::sumAnnualPremium($v->premi_id)) ?>
                            @endforeach
                        </div>
                        <div class="col-md-4">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <td>Total</td>
                                    <td><input type="text" style="font-weight: 700; color: red;" class="form-control" id="total" name="total_premium" value="{{ @$total }}"> </td>
                                </tr>
                                <? @\App\Http\Controllers\EndorsementController::updateTotalDue($v->quotes_id,$total) ?>
                                <tr>
                                    <td><a href="{{ url('endorsements_list') }}" class="btn btn-default">Finish</a> </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <script>
        function saveForm(id){
            var data = $("#form-"+id).serialize();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: '../update_premium_endorsement',
                data: {data: data,add_num:{{ $additional }}},
                success: function (data) {
                    alert(data)
                    location.reload();
                }
            })
        }
        function deleteAdditional(id)
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: '../delete_additional_premium',
                data: {data: id},
                success: function (data) {
                    location.reload();
                }
            })
        }
        var i = {!! $additional !!};
        function addRow(id,num) {
            var itemlist = document.getElementById(id);

            var row = document.createElement('tr');
            var b = document.createElement('td');
            var c = document.createElement('td');
            var d = document.createElement('td');
            var e = document.createElement('td');
            var aksi = document.createElement('td');
            itemlist.appendChild(row);
            row.appendChild(b);
            row.appendChild(c);
            row.appendChild(d);
            row.appendChild(e);
            row.appendChild(aksi);

            var add_id = document.createElement('input');
            add_id.setAttribute('name', 'add_id');
            add_id.setAttribute('type', 'hidden');
            add_id.setAttribute('value', {{ $additional }});

            var insured_name = document.createElement('input');
            insured_name.setAttribute('name', 'type_[' + i + ']');
            insured_name.setAttribute('class', 'form-control');
//
            var insured_address = document.createElement('input');
            insured_address.setAttribute('name', 'premium_type[' + i + ']');
            insured_address.setAttribute('class', 'form-control');
//
            var insured_telp = document.createElement('input');
            insured_telp.setAttribute('name', 'annual_premium[' + i + ']');
            insured_telp.setAttribute('class', 'form-control');
//
//
//
            var hapus = document.createElement('span');
            b.appendChild(add_id);
            c.appendChild(insured_name);
            d.appendChild(insured_address);
            e.appendChild(insured_telp);
            aksi.appendChild(hapus);
            hapus.innerHTML = '<button type="button" class="btn btn-small btn-default"><i class="fa fa-trash"></i></button>';
//                membuat aksi delete element
            hapus.onclick = function () {
                row.parentNode.removeChild(row);
            };

            i++;
        }
    </script>
@endsection
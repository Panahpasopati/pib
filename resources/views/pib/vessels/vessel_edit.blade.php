@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
<style>
    table>tbody#itemlist>tr>td>.form-control {
        padding: 3px 2px;
        font-size: small;
        font-weight: 500;
    }
    .table>tbody>tr>td{
        padding: 3px;
    }
</style>
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="page-title">Edit Vessel Company</h4>
                <form method="post" action="{{ url('update_vessel') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Insured</label>
                                    <input type="text" name="insured" class="form-control" value="{{ $data->insured }}" required="">
                                    <input type="hidden" name="vessel_id" class="form-control" value="{{ $data->id }}" required="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Address</label>
                                        <textarea name="address" class="form-control" rows="5">{{ $data->address }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Phone</label>
                                        <input type="text" name="phone" class="form-control" value="{{ $data->phone }}" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="text" name="email" class="form-control" value="{{ $data->email }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Channel</label>
                                        <select name="channel" required class="form-control channel" onchange="changeChannel()">
                                            <option value="" disabled selected>--choose an option--</option>
                                            <option {{ $data->channel == 'Intermediary' ? 'selected' : '' }} value="Intermediary">Intermediary</option>
                                            <option {{ $data->channel == 'Agent' ? 'selected' : '' }} value="Agent">Agent</option>
                                            <option {{ $data->channel == 'Direct' ? 'selected' : '' }} value="Direct">Direct</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Broker/Agent</label>
                                        <select name="channel_id" required class="form-control channel_id" disabled>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">PIC</label>
                                        <input type="text" name="pic" class="form-control" value="{{ $data->pic }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="overflow-x:auto;">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th width="20%">Vessel Name</th>
                                    <th width="10%">Vessel Type</th>
                                    <th width="10%">Built</th>
                                    <th width="10%">GT</th>
                                    <th width="10%">Flag</th>
                                    <th width="10%">Class</th>
                                    <th width="10%">Crew</th>
                                    <th width="10%">Port Of Registry</th>
                                    <th width="10%">Term</th>
                                    <th width="10%">Trading</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <!--elemet sebagai target append-->
                                <tbody id="itemlist">
                                @foreach($detail as $i => $val)
                                    <tr id="tr-{{ $val->id }}">
                                        <td>
                                            <input name="vessel_name[{{ $i }}]" class="form-control" value="{{ $val->vessel_name }}" type="text" required>
                                            <input name="id_v[{{ $i }}]" class="form-control" value="{{ $val->id }}" type="hidden" required>
                                        </td>
                                        <td>
                                            <input name="vessel_type[{{ $i }}]" class="form-control" value="{{ $val->vessel_type }}" type="text" required>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="built[{{ $i }}]" value="{{ $val->built }}">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="gt[{{ $i }}]" value="{{ $val->gt }}">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="flag[{{ $i }}]"  value="{{ $val->flag }}">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="class[{{ $i }}]"  value="{{ $val->class }}">
                                        </td>
                                        <td>
                                            <input name="crew[{{ $i }}]" class="form-control"  type="text"  value="{{ $val->crew }}">
                                        </td>
                                        <td>
                                            <input name="port_of_registry[{{ $i }}]" class="form-control"  value="{{ $val->port_of_registry }}" type="text">
                                        </td>
                                        <td>
                                            <input name="term[{{ $i }}]" class="form-control" value="{{ $val->term }}" type="text">
                                        </td>
                                        <td>
                                            <input name="trading[{{ $i }}]" class="form-control" value="{{ $val->trading }}" type="text">
                                        </td>
                                        <td>
                                            <button type="button" onclick="deleteRow({{ $val->id }})" class="btn btn-xs btn-default"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="10"></td>
                                    <td>
                                        <button class="btn btn-xs btn-default" type="button" onclick="additem(); return false"><i class="fa fa-plus"></i></button>

                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-md">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <!-- /.container-fluid-->
    <script>
        function deleteRow(id){
            $("#tr-"+id).remove();
        }

        var x = {!! count($detail) !!};
        function additem() {
            var itemlist = document.getElementById('itemlist');

            var row = document.createElement('tr');
            var b = document.createElement('td');
            var c = document.createElement('td');
            var d = document.createElement('td');
            var e = document.createElement('td');
            var f = document.createElement('td');
            var g = document.createElement('td');
            var h = document.createElement('td');
            var i = document.createElement('td');
            var j = document.createElement('td');
            var k = document.createElement('td');
            var aksi = document.createElement('td');
            itemlist.appendChild(row);
            row.appendChild(b);
            row.appendChild(c);
            row.appendChild(d);
            row.appendChild(e);
            row.appendChild(f);
            row.appendChild(g);
            row.appendChild(h);
            row.appendChild(i);
            row.appendChild(j);
            row.appendChild(k);
            row.appendChild(aksi);
            var insured_name = document.createElement('input');
            insured_name.setAttribute('name', 'vessel_name[' + x + ']');
            insured_name.setAttribute('class', 'form-control');
            insured_name.setAttribute('required', '');
//
            var insured_address = document.createElement('input');
            insured_address.setAttribute('name', 'vessel_type[' + x + ']');
            insured_address.setAttribute('class', 'form-control');
            insured_address.setAttribute('required', '');
//'vessel_name','vessel_type', 'built', 'gt', 'flag', 'class', 'crew', 'port_of_registry',
            var insured_telp = document.createElement('input');
            insured_telp.setAttribute('name', 'built[' + x + ']');
            insured_telp.setAttribute('class', 'form-control');
//
            var insured_pic = document.createElement('input');
            insured_pic.setAttribute('name', 'gt[' + x + ']');
            insured_pic.setAttribute('class', 'form-control');
//
            var insured_email = document.createElement('input');
            insured_email.setAttribute('name', 'flag[' + x + ']');
            insured_email.setAttribute('class', 'form-control');

            var insured_class = document.createElement('input');
            insured_class.setAttribute('name', 'class[' + x + ']');
            insured_class.setAttribute('class', 'form-control');

            var insured_crew = document.createElement('input');
            insured_crew.setAttribute('name', 'crew[' + x + ']');
            insured_crew.setAttribute('class', 'form-control');

            var insured_por = document.createElement('input');
            insured_por.setAttribute('name', 'port_of_registry[' + x + ']');
            insured_por.setAttribute('class', 'form-control');

            var insured_term = document.createElement('input');
            insured_term.setAttribute('name', 'term[' + x + ']');
            insured_term.setAttribute('class', 'form-control');

            var insured_endor = document.createElement('input');
            insured_endor.setAttribute('name', 'trading[' + x + ']');
            insured_endor.setAttribute('class', 'form-control');
//
//
            var hapus = document.createElement('span');
            b.appendChild(insured_name);
            c.appendChild(insured_address);
            d.appendChild(insured_telp);
            e.appendChild(insured_pic);
            f.appendChild(insured_email);
            g.appendChild(insured_class);
            h.appendChild(insured_crew);
            i.appendChild(insured_por);
            j.appendChild(insured_term);
            k.appendChild(insured_endor);

            aksi.appendChild(hapus);
            hapus.innerHTML = '<button type="button" class="btn btn-xs btn-default"><i class="fa fa-trash"></i></button>';
//                membuat aksi delete element
            hapus.onclick = function () {
                row.parentNode.removeChild(row);
            };

            console.log(x);
            x++;
        }
        $(document).ready(function(){
           changeChannel();
        });

        function changeChannel(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: '{!! url('get_sales_or_marketing') !!}',
                data: {channel: $(".channel").val()},
                success: function (data) {
                    $('.channel_id').empty();
                    $('.channel_id').append('<option selected disabled>--choose an option--</option>')
                    for (var i = 0; i < data.length; i++) {
                        var selected = '';
                        if(data[i].id == {!! ($data->channel_id) ? $data->channel_id : 0 !!}){
                            selected = 'selected';
                        }


                        $('.channel_id').append('<option '+selected+' value=' + data[i].id + '>' + data[i].name + '</option')
                    }
                    $(".channel_id").removeAttr('disabled')
                }
            })
        }
    </script>



@endsection

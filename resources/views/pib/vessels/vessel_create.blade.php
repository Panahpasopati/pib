@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<style>
    table>tbody#itemlist>tr>td>.form-control {
        padding: 3px 2px;
        font-size: small;
        font-weight: 500;
    }
    .table>tbody>tr>td{
        padding: 3px;
    }
</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h4 class="page-title">New Vessel Company</h4>
            <form method="post" action="{{ url('store_vessel') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Insured</label>
                                <input type="text" name="insured" class="form-control" value="{{ old('insured') }}" id="insured" required="">
                                <div class="result_"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Address</label>
                                <textarea name="address" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Phone</label>
                                <input type="text" name="phone" class="form-control" value="{{ old('phone') }}" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Email</label>
                                <input type="text" name="email" class="form-control" value="{{ old('email') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Channel</label>
                                <select name="channel" required class="form-control channel">
                                    <option value="" disabled selected>--choose an option--</option>
                                    <option value="Intermediary">Intermediary</option>
                                    <option value="Agent">Agent</option>
                                    <option value="Direct">Direct</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Broker/Agent</label>
                                <select name="channel_id" class="form-control channel_id" disabled>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">PIC</label>
                                <input type="text" name="pic" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <table style="margin-left: 20px;" class="table table-condensed">
                            <thead>
                            <tr>
                                <th width="20%">Vessel Name</th>
                                <th width="10%">Vessel Type</th>
                                <th width="10%">Built</th>
                                <th width="10%">GT</th>
                                <th width="10%">Flag</th>
                                <th width="10%">Class</th>
                                <th width="10%">Crew</th>
                                <th width="10%">Port Of Registry</th>
                                <th width="10%">Term</th>
                                <th width="10%">Trading</th>
                                <th></th>
                            </tr>
                            </thead>
                            <!--elemet sebagai target append-->
                            <tbody id="itemlist">
                            <tr>
                                <td>
                                    <input name="vessel_name[0]" class="form-control" required="" type="text">
                                </td>
                                <td>
                                    <input name="vessel_type[0]" class="form-control" required="" type="text">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="built[0]">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="gt[0]">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="flag[0]">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="class[0]">
                                </td>
                                <td>
                                    <input name="crew[0]" class="form-control" type="text">
                                </td>
                                <td>
                                    <input name="port_of_registry[0]" class="form-control" type="text">
                                </td>
                                <td>
                                    <input name="term[0]" class="form-control"  type="text">
                                </td>
                                <td>
                                    <input name="trading[0]" class="form-control"  type="text">
                                </td>
                                <td></td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="10"></td>
                                <td>
                                    <button class="btn btn-xs btn-default" type="button" onclick="additem(); return false"><i class="fa fa-plus"></i></button>

                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-md">Save</button>
                        <button type="button" class="btn btn-success btn-md" onclick="uploadWindow()">Upload File</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- /.container-fluid-->
<script>
    function uploadWindow(){
        var strWindowFeatures = "location=yes,height=570,width=520,scrollbars=yes,status=yes";
        var URL = "{{ env('EXPORT_IMPORT_URL') }}import.php";
        var win = window.open(URL, "_blank", strWindowFeatures);
    }
    var x = 1;
    function additem() {
        var itemlist = document.getElementById('itemlist');

        var row = document.createElement('tr');
        var b = document.createElement('td');
        var c = document.createElement('td');
        var d = document.createElement('td');
        var e = document.createElement('td');
        var f = document.createElement('td');
        var g = document.createElement('td');
        var h = document.createElement('td');
        var i = document.createElement('td');
        var j = document.createElement('td');
        var k = document.createElement('td');
        var aksi = document.createElement('td');
        itemlist.appendChild(row);
        row.appendChild(b);
        row.appendChild(c);
        row.appendChild(d);
        row.appendChild(e);
        row.appendChild(f);
        row.appendChild(g);
        row.appendChild(h);
        row.appendChild(i);
        row.appendChild(j);
        row.appendChild(k);
        row.appendChild(aksi);
        var insured_name = document.createElement('input');
        insured_name.setAttribute('name', 'vessel_name[' + x + ']');
        insured_name.setAttribute('class', 'form-control');
        insured_name.setAttribute('required', '');
//
        var insured_address = document.createElement('input');
        insured_address.setAttribute('name', 'vessel_type[' + x + ']');
        insured_address.setAttribute('class', 'form-control');
        insured_address.setAttribute('required', '');
//'vessel_name','vessel_type', 'built', 'gt', 'flag', 'class', 'crew', 'port_of_registry',
        var insured_telp = document.createElement('input');
        insured_telp.setAttribute('name', 'built[' + x + ']');
        insured_telp.setAttribute('class', 'form-control');
//
        var insured_pic = document.createElement('input');
        insured_pic.setAttribute('name', 'gt[' + x + ']');
        insured_pic.setAttribute('class', 'form-control');
//
        var insured_email = document.createElement('input');
        insured_email.setAttribute('name', 'flag[' + x + ']');
        insured_email.setAttribute('class', 'form-control');

        var insured_class = document.createElement('input');
        insured_class.setAttribute('name', 'class[' + x + ']');
        insured_class.setAttribute('class', 'form-control');

        var insured_crew = document.createElement('input');
        insured_crew.setAttribute('name', 'crew[' + x + ']');
        insured_crew.setAttribute('class', 'form-control');

        var insured_por = document.createElement('input');
        insured_por.setAttribute('name', 'port_of_registry[' + x + ']');
        insured_por.setAttribute('class', 'form-control');

        var insured_term = document.createElement('input');
        insured_term.setAttribute('name', 'term[' + x + ']');
        insured_term.setAttribute('class', 'form-control');

        var insured_endor = document.createElement('input');
        insured_endor.setAttribute('name', 'trading[' + x + ']');
        insured_endor.setAttribute('class', 'form-control');
//
//
        var hapus = document.createElement('span');
        b.appendChild(insured_name);
        c.appendChild(insured_address);
        d.appendChild(insured_telp);
        e.appendChild(insured_pic);
        f.appendChild(insured_email);
        g.appendChild(insured_class);
        h.appendChild(insured_crew);
        i.appendChild(insured_por);
        j.appendChild(insured_term);
        k.appendChild(insured_endor);

        aksi.appendChild(hapus);
        hapus.innerHTML = '<button type="button" class="btn btn-xs btn-default"><i class="fa fa-trash"></i></button>';
//                membuat aksi delete element
        hapus.onclick = function () {
            row.parentNode.removeChild(row);
        };


        x++;
    }

    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".channel").change(function(){

            $.ajax({
                type: 'post',
                url: 'get_sales_or_marketing',
                data: {channel: $(".channel").val()},
                success: function (data) {
                    $('.channel_id').empty();
                    $('.channel_id').append('<option selected disabled>--choose an option--</option>')
                    for (var i = 0; i < data.length; i++) {
                        $('.channel_id').append('<option value=' + data[i].id + '>' + data[i].name + '</option')
                    }
                    $(".channel_id").removeAttr('disabled')
                }
            })

        });
        $("#insured").blur(function(){
            var input = $("#insured").val();
            $.post('check_insured_name',{input:input},function(data){
                if(data.response){
                    $(".result_").html('<p class="text-danger">'+data.data+'</p>');
                    $.each(data.rows,function(i,val){
                        $(".result_").append('<p><a href="'+val.link+'" title="edit">'+val.name+'</a>');
                })
        }else{
            $(".result_").html("");
        }
    });
    })



    })
</script>



@endsection

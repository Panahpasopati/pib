@extends('layouts.pib')
@section('content')
    <style>
        table.ndj{
            margin: -8px;
            width: 103% !important;
        }

        table.ndj td{
            padding:8px;
        }
    </style>
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                    <h3 class="page-title"><a type="button" href="{{ url('create_new_vessel') }}" class="btn btn-default"><i class="fa fa-plus-square"></i> New Insured Company </a></h3>
                    <div class="row">
                    <div class="col-md-12">
                        <!-- BORDERED TABLE -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Insured Company</h3>
                                <div class="pull-right">
                                    <form method="get" class="form-inline" action="{{ url('vessels/search_rows') }}">
                                        <input type="text" class="form-control" name="query" @if(isset($query)) value="{{ $query }}" @endif placeholder="search....">
                                        <button type="submit" class="btn btn-default"><span class="fa fa-search"></span> </button>
                                    </form>
                                    <br>
                                </div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered data-table table-striped">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Company</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Channel</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $i => $d)
                                        <tr>
                                            <td>{{ $i + 1}}</td>
                                            <td>{{ $d->insured }}  @if(isset($_GET['query'])) {!! (\App\Http\Controllers\VesselController::is_in_vessel($d->id,strip_tags($_GET['query']))) ? ' <i class="fa fa-ship" title="Ditemukan di daftar kapal."></i>' : '' !!} @endif</td>
                                            <td><small>{{ $d->address }}</small></td>
                                            <td>{{ $d->phone }}</td>
                                            <td>{{ $d->email }}</td>
                                            <td>{{ $d->channel }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-default btn-sm" onclick="viewDetail({{ $d->id }})"><span class="fa fa-eye-slash"></span></a> <a class="btn btn-primary btn-sm" href="{{ url('edit_vessel_company',$d->id) }}"><span class="fa fa-pencil"></span></a> <form method="post" id="company-{{ $d->id }}" style="display:inline;" action="{{ url('delete_company_vessel',$d->id) }}">{{ csrf_field() }} <a class="btn btn-danger btn-sm" onclick="event.preventDefault(); document.getElementById('company-{{ $d->id }}').submit();"><span class="fa fa-trash"></span></a></form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $data->appends($_GET)->links() }}
                            </div>
                        </div>
                        <!-- END BORDERED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Vessel Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="detail_broker"></div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Vessel Name</th>
                            <th>Vessel Type</th>
                            <th>Built</th>
                            <th>GT</th>
                            <th>flag</th>
                            <th>Class</th>
                            <th>Crew</th>
                            <th>Port of Registry</th>
                            <th>Cargo</th>
                            <th>Trading</th>
                            <th>Term</th>
                            <!--
                            <th>endorsement</th>
                            !-->
                        </tr>
                        </thead>
                        <tbody class="vessels_table_content">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
    <script>
        function viewDetail(id){
            $("#myModal").modal('show');
            $('.modal-footer').html("");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var endorsement_url = '{{ ('vessel_endorsement_view/') }}';
            $.ajax({
                type: 'post',
                url: 'vessel_detail',
                data: {id: id},
                success: function (data) {
                    $(".detail_broker").html(
                      '<table class="table" style="border: none;font-weight:600;"> ' +
                      '<tr><td>Channel</td><td>:</td><td>'+data.channel.channel+'</td></tr> ' +
                      '<tr><td>Marketing</td><td>:</td><td>'+data.channel.name+'</td></tr> ' +
                      '<tr><td>PIC</td><td>:</td><td>'+data.channel.pic+'</td></tr>' +
                      '</table><hr>'
                    );
                    $('.vessels_table_content').html("");
                    $.each(data.data,function(i,val){
                        var html = '<tr><td>'+parseInt(i + 1)+'</td><td>'+val.vessel_name+'</td><td>'+val.vessel_type+'</td><td>'+val.built+'</td><td>'+val.gt+'</td><td>'+val.flag+'</td><td>'+val.class+'</td><td>'+val.crew+'</td><td>'+val.port_of_registry+'</td><td>'+val.cargo+'</td><td>'+val.trading+'</td><td>'+val.term+'</td><!--<td><a href="'+endorsement_url+val.id+'" class="btn btn-success btn-sm">View</a></td>!--></tr>';
                        $('.vessels_table_content').append(html);
                    });
                    $('.modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>')
                }
            })
        }

        $(".btn-danger").click(function(){
            if(!confirm('are you sure?')){
                return false;
            }
        })
    </script>
    <!-- END MAIN -->
@endsection
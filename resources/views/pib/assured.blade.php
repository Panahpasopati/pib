@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <h3 class="page-title"><a type="button" href="{{ route('assured.create') }}" class="btn btn-default"><i class="fa fa-plus-square"></i> New Assured </a></h3>
                <div class="row">
                    <div class="col-md-10">
                        <!-- BORDERED TABLE -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Assured</h3>
                                <div class="pull-right">
                                    <form method="get" class="form-inline" action="{{ url('assured_search') }}">
                                        <input type="text" class="form-control" name="query" @if(isset($_GET['query'])) value="{{ $_GET['query'] }}" @endif placeholder="search....">
                                        <button type="submit" class="btn btn-default"><span class="fa fa-search"></span> </button>
                                    </form>
                                    <br>
                                </div>
                            </div>
                            <div class="panel-body" >
                                <table class="table table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Assured Name</th>
                                        <th>Address</th>
                                        <th>Description</th>
                                        <th>NPWP</th>
                                        <th>Phone</th>
                                        <th>Fax</th>
                                        <th>Email</th>
                                        <th>Website</th>
                                        <th>Brokerage</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $i => $d)
                                        <tr>
                                            <td>{{ $i + 1}}</td>
                                            <td>{{ $d->assured_name }}</td>
                                            <td>{{ $d->address }}</td>
                                            <td><small>{{ $d->description }}</small></td>
                                            <td>{{ $d->npwp }}</td>
                                            <td>{{ $d->phone }}</td>
                                            <td>{{ $d->fax }}</td>
                                            <td>{{ $d->email }}</td>
                                            <td>{{ $d->website }}</td>
                                            <td>{{ $d->brokerage }}%</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-primary" href="{{ route('assured.edit',$d->id) }}"><span class="fa fa-edit"></span></a>
                                                    <form method="POST" action="{{ route('assured.destroy',$d->id)  }}'" style="display:inline">
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <input name="_token" type="hidden" value=" {{ csrf_token() }}">
                                                        <button type="submit" class="btn btn-danger"><span class="fa fa-trash"></span></button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $data->render() }}
                            </div>
                        </div>
                        <!-- END BORDERED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

    <!-- END MAIN -->
@endsection
@extends('layouts.pib')
@section('content')
    <style>
        table.ndj{
            margin: -8px;
            width: 103% !important;
        }

        table.ndj td{
            padding:8px;
        }
    </style>
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <h3 class="page-title"><a type="button" href="{{ route('manage_type.create') }}" class="btn btn-default"><i class="fa fa-plus-square"></i> Create New </a></h3>
                <div class="row">
                    <div class="col-md-12">
                        <!-- BORDERED TABLE -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Type</h3>
                                <div class="pull-right">
                                    <br>
                                </div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Type</th>
                                        <th>Acc. Akun</th>
                                        <th>Gl. Akun</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $i => $d)
                                        <tr>
                                            <td>{{ $i + 1}}</td>
                                            <td>{{ $d->name }}</td>
                                            <td>{{ $d->acc_akun }}</td>
                                            <td>{{ $d->gl_akun }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-primary" href="{{ route('manage_type.edit',$d->id_type) }}"><span class="fa fa-eye-edit"></span> Edit Type</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $data->render() }}
                            </div>
                        </div>
                        <!-- END BORDERED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
@endsection
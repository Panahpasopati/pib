@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="page-title">New Type</h4>
                    {!! Form::model($data, ['method' => 'PATCH','route' => ['manage_type.update', $data->id_type], 'enctype'=>'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Group</label>
                                    <select name="group_name" class="form-control group_name" required>
                                        <option value="" selected disabled>--Choose--</option>
                                        <option {{ $data->group_name == 'QUO' ? 'selected' : '' }} value="QUO">QUO</option>
                                        <option {{ $data->group_name == 'INV' ? 'selected' : '' }} value="INV">INV</option>
                                        <option {{ $data->group_name == 'PR' ? 'selected' : '' }} value="PR">PR</option>
                                        <option {{ $data->group_name == 'PP' ? 'selected' : '' }} value="PP">PP</option>
                                        <option {{ $data->group_name == 'GP' ? 'selected' : '' }} value="GP">GP</option>
                                        <option {{ $data->group_name == 'GR' ? 'selected' : '' }} value="GR">GR</option>
                                        <option {{ $data->group_name == 'PC' ? 'selected' : '' }} value="PC">PC</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" name="name" class="form-control" value="{{ $data->name  }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 special {{ $data->group_name == 'QUO' || $data->group_name == 'INV' ? 'hidden' : '' }}">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Acc. Akun</label>
                                    <select name="acc_akun" required class="form-control coa">
                                        @foreach($coa as $c)
                                            <option {{ $c->code == $data->acc_akun ? 'selected' : '' }} value="{{ $c->code }}">{{ $c->code }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Gl. Akun</label>
                                    <select name="gl_akun" required class="form-control coa">
                                        @foreach($coa as $c)
                                            <option {{ $c->code == $data->gl_akun ? 'selected' : '' }} value="{{ $c->code }}">{{ $c->code }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-md">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->

<script>
    $(document).ready(function(){
        $(".group_name").change(function(){
            var value =  $(".group_name").val();
            if(value === "QUO"){
                $(".special").addClass('hidden');
            }else if(value === "INV"){
                $(".special").addClass('hidden');
            }else{
                $(".special").removeClass('hidden');
            }

        })
    })
</script>


@endsection

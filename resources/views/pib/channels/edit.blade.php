@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h4 class="page-title">Add New Sales & Marketing</h4>
            <form method="post" action="{{ route('channel.update',$data->id) }}">
                <input type="hidden" name="_method" value="PATCH">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Name</label>
                            <input type="text" class="form-control" name="name" value="{{ $data->name }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Address</label>
                            <textarea class="form-control" name="address" rows="3">{{ $data->address }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Phone</label>
                            <input type="tel" class="form-control" name="phone" value="{{ $data->phone }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control" name="email"  value="{{ $data->email }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">NPWP</label>
                            <input type="text" class="form-control" name="npwp"  value="{{ $data->npwp }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Type</label>
                            <select name="type_" class="form-control" required>
                                <option value="" disabled selected>--choose an option--</option>
                                <option {{ $data->type_ == 'Intermediary' ? 'selected' : ''  }} value="Intermediary">Intermediary</option>
                                <option {{ $data->type_ == 'Agent' ? 'selected' : ''  }} value="Agent">Agent</option>
                            </select>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@endsection
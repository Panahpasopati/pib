@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <h3 class="page-title"><a type="button" href="{{ route('channel.create') }}" class="btn btn-default"><i class="fa fa-plus-square"></i> New Data </a></h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- BORDERED TABLE -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sales & Marketing List</h3>
                            <div class="pull-right">
                                <form method="get" class="form-inline" action="{{ url('channel-search') }}">
                                    <input type="text" class="form-control" name="query" placeholder="search....">
                                    <button type="submit" class="btn btn-default"><span class="fa fa-search"></span> </button>
                                </form>
                                <br>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Channel</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>NPWP</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $i => $d)
                                    <tr>
                                        <td><small>{{ $i + 1}}</small></td>
                                        <td><small>{{ $d->type_ }}</small></td>
                                        <td><small>{{ $d->name }}</small></td>
                                        <td><small>{{ $d->address }}</small></td>
                                        <td><small>{{ $d->phone }}</small></td>
                                        <td><small>{{ $d->email }}</small></td>
                                        <td><small>{{ $d->npwp }}</small></td>
                                        <td>
                                            <div class="btn-group">
                                                <!--
                                                <a class="btn btn-primary" href="{{ route('channel.show',$d->id) }}"><span class="fa fa-eye"></span></a>
                                                !-->
                                                <a class="btn btn-primary" href="{{ route('channel.edit',$d->id) }}"><span class="fa fa-edit"></span></a>
                                                <form method="post" action="{{ route('channel.destroy',$d->id)  }}" style="display:inline">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    {{ csrf_field() }}
                                                    <button type="submit" onclick="return confirm('are you sure?');" class="btn btn-danger"><span class="fa fa-trash-o"></span> Delete</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $data->render() }}
                        </div>
                    </div>
                    <!-- END BORDERED TABLE -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>

<!-- END MAIN -->
@endsection
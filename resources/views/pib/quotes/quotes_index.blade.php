@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <h3 class="page-title">
                    <a type="button" href="{{ route('quote.create') }}" class="btn btn-default"><i class="fa fa-plus-square"></i> Create Quote </a>
                    <div class="pull-right">
                        <a type="button" href="{{ url('quotation','quotes_draft') }}" class="btn btn-default"><i class="fa fa-list"></i> Draft Quote <span class="badge badge-primary">{{ $draft }}</span> </a>
                    </div>
                </h3>
                <div class="row">
                    <div class="col-md-12">
                        <!-- BORDERED TABLE -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Quotes List</h3>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Quotation Status</th>
                                        <th>Type</th>
                                        <th>Ref.</th>
                                        <th>Insurer</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th>Created By</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $i => $d)
                                        <tr>
                                            <td>{{ $i + 1}}</td>
                                            <td>{{ $d->quotation_status }}</td>
                                            <td>{{ @$d->QuoteType->name }}</td>
                                            <td>{{ $d->our_ref }}</td>
                                            <td>@foreach($d->Insurers as $insurer) &bull; {{ $insurer->Insurer->insured }}<br>@endforeach</td>
                                            <td><strong>{{ strtoupper($d->status) }}</strong></td>
                                            <td><i>{{ @$d->CreatedBy->name }}</i></td>
                                            <td>{{ date('d M Y H:i',strtotime($d->created_at)) }} WIB</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-primary" href="{{ route('quote.show',$d->id) }}" title="Preview"><span class="fa fa-eye-slash"></span></a>
                                                    <div class="dropdown">
                                                        <button class="btn btn-warning btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-download"></span> <span class="fa fa-caret-down"></span> </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a class="btn btn-default" {{ ($d->Document) ? 'href='.env('DOCUMENT_GENERATED_URL') . 'quotes/'. $d->Document->file.'' : 'disabled' }} title="Download Docx File"><span class="fa fa-download"></span></a></li>
                                                            <li><a class="btn btn-default" {{ ($d->Document) ? 'href='.env('DOCUMENT_GENERATED_URL') . 'quotes/'. $d->Document->vessels_file.'' : 'disabled' }} title="Download Docx File"><span class="fa fa-download"></span></a></li>
                                                        </ul>
                                                    </div>
                                                    <a class="btn btn-destroy" href="{{ url('quotation',['send_invoice',$d->id]) }}" title="Send Invoice"><span class="fa fa-send"></span></a>
                                                    <a class="btn btn-warning" href="{{ route('quote.edit',$d->id) }}" title="Revision"><span class="fa fa-edit"></span></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $data->render() }}
                            </div>
                        </div>
                        <!-- END BORDERED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

    <script>
        function GenerateDocxFile(id,t){
            var req;
            if(t === 2){
                req = 'push_cargo.php';
            }else{
                req = 'pushs.php';
            }
            $.post('{{ env('DOCUMENT_GENERATOR_URL') }}'+req,{id:id},function(data){
                alert('Generated document with status : success');
                location.reload();
            })
        }
    </script>
    <!-- END MAIN -->
@endsection
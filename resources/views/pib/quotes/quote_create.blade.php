@extends('layouts.pib')
@section('content')
    <style>
        .panel-tabs {
            display: none;
        }
        .panel-tabs {
            animation: fadeEffect 1s; /* Fading effect takes 1 second */
        }
        #filter .panel-body{
            padding: 5px;
        }
        #filter ul{
            margin-top: 5px;
            padding: 0px;
        }
        #filter ul li{
            display: inline;
            padding: 5px;
            margin: 5px 10px;
            border-bottom: 2px solid transparent;
            list-style:	none;
            transition-duration: 0.5s;
            -webkit-transition-duration: 0.5s;
            -moz-transition-duration: 0.5s;
            -o-transition-duration: 0.5s;
        }
        #filter ul li:hover, #filter ul li.active{
            border-color: inherit;
        }
        a.tab-link.active {
            /* border-color: inherit; */
            color: #2196f3;
            border-bottom: 6px solid;
            border-radius: 10px 1px;
        }
        .save-temporary{
            position: fixed;
            background: #333;
            width: 10%;
            padding: 10px;
            top: 81px;
            border-radius: 5px;
            z-index: 999;
            right: 0;
            opacity: 0.1;
        }
        .save-temporary:hover{
            opacity: 0.9;
        }
        .note-btn-group.btn-group.note-para .btn-sm{
            padding: 5px 18px;
        }
    </style>

    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="save-temporary">
                <button class="btn btn-default btn_save_to_draft" type="button" style="width: 100%;">Save to Draft</button>
            </div>
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <h4 class="page-title">Create Quote</h4>
                    <form method="post" class="form-horizontal" action="{{ route('quote.store') }}" id="form-create-quote">
                        {{ csrf_field() }}
                        <div>
                            <div class="col-md-12 alert alert-success">Insured Detail <span type="button" data-toggle="collapse" data-target="#row1" class="btn-link">Show</span> </div>
                            <div class="collapse" id="row1">
                                @include('pib.quotes.rows.row1')
                            </div>
                            <div class="col-md-12 alert alert-success">Term &amp; Condition <span type="button" data-toggle="collapse" data-target="#row2" class="btn-link">Show</span> </div>
                            <div class="collapse" id="row2">
                                @include('pib.quotes.rows.row2')
                            </div>
                            <div class="col-md-12 alert alert-success">General Term <span type="button" data-toggle="collapse" data-target="#row3" class="btn-link">Show</span> </div>
                            <div class="collapse" id="row3">
                                @include('pib.quotes.rows.row3')
                            </div>
                            @include('pib.quotes.rows.row4')
                            <hr><br>
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="panel panel-default" id="filter">
                                        <div class="panel-body text-center">
                                            <ul class="font-md font-bold">
                                                <button class="btn-link" type="submit" ><li class="hvr-blue">Save</li></button>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <!-- /.container-fluid-->

    <div id="modal-company" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add More Company</h4>
                </div>
                <div class="modal-body">
                    <form id="form-add-company">
                        {!! csrf_field() !!}
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Insured</label>
                                    <input type="text" name="insured" class="form-control" value="{{ old('insured') }}" id="insured" required="">
                                    <div class="result_"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Address</label>
                                    <textarea name="address" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Phone</label>
                                    <input type="text" name="phone" class="form-control" value="{{ old('phone') }}" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="text" name="email" class="form-control" value="{{ old('email') }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Channel</label>
                                    <select name="channel" required class="form-control channel">
                                        <option value="" disabled selected>--choose an option--</option>
                                        <option value="Intermediary">Intermediary</option>
                                        <option value="Agent">Agent</option>
                                        <option value="Direct">Direct</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Broker/Agent</label>
                                    <select name="channel_id" class="form-control channel_id" disabled>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">PIC</label>
                                    <input type="text" name="pic" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"></label>
                                    <button class="btn btn-primary" type="button" id="submit-new-company">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add More Vessels</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Vessel Name</label>
                            <input type="text" class="form-control" id="vessel_name">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Vessel Type</label>
                            <input type="text" class="form-control" id="vessel_type">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Built</label>
                            <input type="text" class="form-control" id="built">
                        </div>
                        <div class="form-group">
                            <label class="control-label">GT</label>
                            <input type="text" class="form-control" id="gt">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Flag</label>
                            <input type="text" class="form-control" id="flag">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Class</label>
                            <input type="text" class="form-control" id="class">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Crew</label>
                            <input type="text" class="form-control" id="crew">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Port of Registry</label>
                            <input type="text" class="form-control" id="port_of_registry">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group add-vessel-button">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

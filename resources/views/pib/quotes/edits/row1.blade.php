<style>
    .col-md-6 label.control-label.col-md-4 {
        padding-left: 0px !important;
    }
    input.form-control.our-ref {
        font-weight: 700;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-4">Our Ref : </label>
                <div class="col-md-6">
                    <input type="text" name="our_ref" readonly class="form-control our-ref" value="{{ $data->our_ref }} ">
                    <input type="hidden" name="temp_id" id="temp_id" readonly class="form-control" value="{{ $data->id }} ">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Type Quotation</label>
                <div class="col-md-6">
                    <select name="quote_type" class="form-control type_quote" required="">
                        <option value="" disabled selected>--Choose an Option--</option>
                        @foreach($type as $t)
                            @if($t->id_type != 8)
                                <option {{ $t->id_type == $data->quote_type ? 'selected' : '' }} value="{{ $t->id_type }}">{{ $t->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Quotation Status</label>
                <div class="col-md-6">
                    <select name="quotation_status" required="" class="form-control quotation-status">
                        <option value="" disabled>--Choose an Option--</option>
                        <option {{ $data->quotation_status == 'New' ? 'selected' : '' }} value="New" >New</option>
                        <option {{ $data->quotation_status == 'Renewal' ? 'selected' : ''  }} value="Renewal">Renewal</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Date Issued</label>
                <div class="col-md-6">
                    <input type="text" name="date_issued" class="form-control date_issued datepicker" value="{{ $data->date_issued }}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Term Date</label>
                <div class="col-md-6">
                    <select name="term_date" class="form-control valid_until col-md-4" required="">
                        <option {{ $data->term_date  == 7 ? 'selected' : '' }} value="7">7 Days</option>
                        <option {{ $data->term_date  == 14 ? 'selected' : '' }} value="14">14 Days</option>
                        <option {{ $data->term_date  == 30 ? 'selected' : ''  }} value="30">30 Days</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Due Date</label>
                <div class="col-md-6">
                    <input type="text" class="form-control until  col-md-4 {{ $data->quotation_status == 'Renewal' ? 'datepicker' : '' }}" name="valid_until" value="<? echo $data->valid_until ?>" {{ $data->quotation_status == 'Renewal' ? '' : 'readonly' }}>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-4">Periode</label>
                <div class="col-md-6">
                    <select name="period" class="form-control" id="periode-new">
                        <option @if($data->period == "12 month from date issued") selected @endif value="12 month from date issued" selected>12 month from date issued</option>
                        <option @if($data->period == "To be advised") selected @endif value="To be advised">TBA</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12" style="padding-left: 0px !important;">
                    <label class="col-md-2 control-label periode" style="padding-left: 0px !important;">From</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control periode-renew datepicker" id="" name="period_from" value="{{ $data->period_from }}">
                    </div>
                    <label class="col-md-2 control-label periode">To</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control periode-renew datepicker" name="period_to" id="" value="{{ $data->period_to }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <hr><br>
            <div class="col-md-12" style="padding-left: 0px !important;">
                <div class="col-md-8">
                    <label class="control-label col-md-2" style="padding-left: 0px !important;">Assured</label>
                    <div class="col-md-10">
                        <select class="form-control assured" name="insurer[]" multiple="multiple" required="">
                            @foreach($company as $c)
                                <option @if(in_array($c->id,$insurers)) selected @endif value="{{ $c->id }}">{{ $c->insured }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="control-label col-md-4" style="padding-left: 0px !important;">Mortgage</label>
                    <div class="col-md-6">
                        <select class="form-control banks" name="mortgage_bank">
                            <option value="" selected disabled>--Choose--</option>
                            @foreach($banks as $b)
                                <option value="{{ $b->id }}">{{ $b->bank_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <hr><br>
            <div class="col-md-12" style="padding-left: 0px !important;">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Security</label>
                        <div class="col-md-6">
                            <select name="security" required class="form-control coa">
                                @foreach($assured as $ass)
                                    <option value="{{ $ass->id }}" {{ $ass->id == $data->security ? 'selected' : '' }}>{{ $ass->assured_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Currency</label>
                        <div class="col-md-6">
                            <select name="currency" required class="form-control">
                                <option value="USD" {{ $data->currency == 'USD' ? 'selected' : '' }}>USD</option>
                                <option value="IDR" {{ $data->currency == 'IDR' ? 'selected' : '' }}>IDR</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    input.form-control.our-ref {
        font-weight: 700;
    }
</style>
<div class="row" id="row-2">
    <div class="col-md-12">
        <div class="form-group">
            <label class="col-md-4 control-label">Important Information</label>
            <div class="col-md-8">
                <textarea class="form-control" name="important_information" rows="3">{!! $data->important_information !!}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">Trading Limits</label>
            <div class="col-md-8">
                <textarea class="form-control" name="trading_limit" rows="3">{!! $data->trading_limit !!}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">Condition</label>
            <div class="col-md-8">
                <table class="table">
                    <tbody id="itemCondition">
                    <tr>
                        <td width="100%">
                            <textarea class="form-control summernote-text" name="condition_temp" rows="2">{!! $data->condition_ !!}</textarea>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">Limit of Cover</label>
            <div class="col-md-8">
                <textarea class="form-control" name="limit_of_cover" rows="3">{!! $data->limit_of_cover !!}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">Deductibles</label>
            <div class="col-md-8">
                <table class="table">
                    <tbody id="deductible-tbl">
                    <tr>
                        <td><textarea rows="2" name="deductibles_temp" class="summernote-text">{!! $data->deductibles !!}</textarea> </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">General Conditions</label>
            <div class="col-md-6">
                <textarea class="summernote-text" name="general_conditions">{!! $data->general_conditions !!}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">Information Given</label>
            <div class="col-md-8">
                <table class="table">
                    <tbody id="inform-given">
                    <tr>
                        <td><textarea name="information_given_temp" class="summernote-text">{!! $data->information_given !!}</textarea> </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">Comment</label>
            <div class="col-md-8">
                <table class="table">
                    <tbody id="comment-tr">
                    <tr>
                        <td><textarea class="summernote-text" name="comment_temp">{!! $data->comment !!}</textarea> </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">Warranty</label>
            <div class="col-md-8">
                <table class="table">
                    <tbody id="swar-tr">
                    <tr>
                        <td><textarea  class="summernote-text" name="warranty_temp">{!! $data->warranty !!}</textarea> </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">Survey Warranty</label>
            <div class="col-md-8">
                <table class="table">
                    <tbody id="swar-tr">
                    <tr>
                        <td><textarea  class="summernote-text" name="survey_warranty_temp">{!! $data->survey_warranty !!}</textarea> </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">War Risk (P&I) Endorsement A</label>
            <div class="col-md-8">
                <table class="table">
                    <tbody id="swar-tr">
                    <tr>
                        <td><textarea  class="summernote-text" name="war_risk_etc_temp">{!! $data->war_risk_etc !!}</textarea> </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="form-group"><label class="col-md-4 control-label">Information Required Prior To Cover</label>
            <div class="col-md-8">
                <table class="table">
                    <tbody id="information-required-tr">
                    @if($data->information_required)
                        @foreach(explode("|||",$data->information_required) as $x => $ir)
                            <tr id="ir-{{ $x }}">
                                <td><textarea  rows="2" class="form-control" name="information_required_temp[{{ $x }}]">{{ $ir }}</textarea> </td>
                                <td><button type="button" class="btn btn-small" onclick="deleteInformationRequired({{ $ir }})"><i class="fa fa-trash"></i> </button></td>
                            </tr>
                            @endforeach
                        @else
                        <tr>
                            <td><textarea  rows="2" class="form-control" name="information_required_temp[0]"></textarea> </td>
                            <td></td>
                        </tr>
                        @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2" align="right"><button type="button" class="btn btn-small" onclick="addInformationRequired()"><i class="fa fa-plus"></i></button> </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">Subjectivities</label>
            <div class="col-md-8">
                <table class="table">
                    <tbody id="swar-tr">
                    <tr>
                        <td><textarea  class="summernote-text" name="subjectivities_temp">{!! $data->subjectivities !!}</textarea> </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var y = {!! ($data->information_required) ? count(explode("|||",$data->information_required)) : 1 !!};
    function addInformationRequired(){
        $("#information-required-tr").append('<tr id="ir-'+y+'"> ' +
                '<td><textarea class="form-control" name="information_required_temp['+y+']" rows="2"></textarea> </td> ' +
                '<td><button type="button" class="btn btn-small btn-default" onclick="deleteInformationRequired('+y+');"><i class="fa fa-trash"></i></button></td> ' +
                '</tr>');
    }
    function deleteInformationRequired(id){
        $("#ir-"+id).remove();
    }
    //var sw = 4 ;
    var sw = 1 ;
    function addSW(){
        $("#swar-tr").append('<tr id="sw-'+sw+'"> ' +
                '<td><textarea class="form-control" name="survey_warranty_temp['+com+']" rows="2" "></textarea> </td> ' +
                '<td><button type="button" class="btn btn-small btn-default" onclick="deleteSW('+sw+');"><i class="fa fa-trash"></i></button></td> ' +
                '</tr>');
        com++;
    }
    function deleteSW(id){
        $("#sw-"+id).remove();
    }
    var c = 1;
    function addCondition(){
        $("#itemCondition").append('<tr id="con-'+c+'"><td>' +
                '<textarea class="form-control condition_value" style="margin: 0px; width: 497px; height: 52px;" name="condition_temp['+c+']" rows="2"></textarea>'+
                '</td><td><button type="button" class="btn btn-small btn-default" onclick="deleteCondition('+c+');"><i class="fa fa-trash"></i></button></td></tr>')
        c++;
    }

    function deleteCondition(x){
        $("#con-"+x).remove();
    }

    //var ig = 4;
    var ig = 1;
    function addIG(){
        $("#inform-given").append('<tr id="ig-'+ig+'"> ' +
                '<td><textarea name="information_given_temp['+ig+']" class="form-control" rows="2"></textarea> </td> ' +
                '<td><button type="button" class="btn btn-small btn-default" onclick="deleteIG('+ig+');"><i class="fa fa-trash"></i></button></td> ' +
                '</tr>');
        ig++;
    }

    function deleteIG(i){
        $("#ig-"+i).remove();
    }

    //var com = 2;
    var com = 1;
    function addComment(){
        $("#comment-tr").append('<tr id="com-'+com+'"> ' +
                '<td><textarea class="form-control" value="" name="comment_temp['+com+']" rows="2"></textarea> </td> ' +
                '<td><button type="button" class="btn btn-small btn-default" onclick="deleteComment('+com+');"><i class="fa fa-trash"></i></button></td> ' +
                '</tr>');
        com++;
    }

    function deleteComment(id){
        $("#com-"+id).remove();
    }

    //var ded = 5;
    var ded = 1;
    function addDeductible(){
        $("#deductible-tbl").append('<tr id="ded-'+ded+'"> ' +
                '<td><textarea rows="2" class="form-control"  name="deductibles_temp['+ded+']"></textarea></td> ' +
                '<td><button type="button" class="btn btn-small btn-default" onclick="deleteDeductible('+ded+');"><i class="fa fa-trash"></i></button></td> ' +
                '</tr>');
        ded++;
    }

    function deleteDeductible(f){
        $("#ded-"+f).remove();
    }

</script>
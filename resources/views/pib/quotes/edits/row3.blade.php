<div class="row" id="row-3">
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-4 control-label">Excluded Risks</label>
                <div class="col-md-6">
                    <input type="text" name="excluded_risks" class="form-control" value="{{ $data->excluded_risks }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Crew Contract Rest.</label>
                <div class="col-md-6">
                    <input type="text" name="crew_contract_restricted" class="form-control" value="{{ $data->crew_contract_restricted }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Class Warranty</label>
                <div class="col-md-6">
                    <input type="text" name="class_warranty" class="form-control" value="{{ $data->class_warranty }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Applicable Clauses</label>
                <div class="col-md-6">
                    <input type="text" name="applicable_clauses" class="form-control" value="{{ $data->applicable_clauses }}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Est. Time Departure</label>
                <div class="col-md-6">
                    <input type="date" class="form-control col-md-4" name="estimate_time_departure" value="{{ $data->estimate_time_departure }}">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-4">Interest Insured</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="interest_insured" value="{{ $data->interest_insured }}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Vessel/Conveyance</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="vessel_conveyance" value="{{ $data->vessel_conveyance }}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Sum Insured</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="sum_insured" value="{{ $data->sum_insured }}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Rate</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="rate_temp" value="{{ $data->rate }}">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var war = 1;
    function addWarranty(){
        $("#warranty").append('<tr id="_warranty-'+war+'"> ' +
                '<td><textarea class="form-control"  name="warranty_temp['+war+']"></textarea> </td> ' +
                '<td><button type="button" class="btn btn-small btn-default" onclick="deleteWarranty('+war+');"><i class="fa fa-trash"></i></button></td> ' +
                '</tr>');
        war++;
    }

    function deleteWarranty(id){
        $("#_warranty-"+id).remove();
    }
</script>
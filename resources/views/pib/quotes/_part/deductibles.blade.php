<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Deductibles
:</span></b></p>

@if($list = __add_mark_html_tag($data->deductibles))
    @foreach($list as $deductible)
        <p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.25in;text-autospace:none'><span style='font-size:9.0pt;
font-family:Consolas;color:#262626'>{{ $deductible }} </span></p>
    @endforeach
@endif
@if($list = $data->war_risk_etc)
    @foreach(__add_mark_html_tag($list) as $a)
        <p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.25in;text-autospace:none'><span style='font-size:9.0pt;
font-family:Consolas;color:#262626'>{{ $a }} </span></p>
    @endforeach
@endif


<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>When one
accident gives rise to claims of a different nature, the aggregate of all
claims shall be                     subject to the highest deductible
applicable to any one such claim.</span></p>

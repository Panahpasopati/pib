<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Period
:             </span></b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>{{ $data->quotation_status == 'New' ? $data->period : date('d M Y',strtotime($data->period_from)).' - '.date('d M Y',strtotime($data->period_to)) }}  </span></p>
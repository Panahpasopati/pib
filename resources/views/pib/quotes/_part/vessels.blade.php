<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
       style='border-collapse:collapse;border:none'>
    <tr>
        <td width=38 valign=top style='width:28.3pt;border:solid white 1.0pt;
  border-bottom:solid white 3.0pt;background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>No</span></b></p>
        </td>
        @php $multi_insurer = false; @endphp
        @if(count($data->Insurers) > 1)
            <td width=100 valign=top style='width:75.15pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Insurer</span></b></p>
            </td>
            @php $multi_insurer = true @endphp
        @endif
        <td width=100 valign=top style='width:75.15pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Vessel
  Name </span></b></p>
        </td>
        <td width=69 valign=top style='width:51.6pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Type</span></b></p>
        </td>
        <td width=69 valign=top style='width:51.6pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Built
  </span></b></p>
        </td>
        <td width=68 valign=top style='width:51.35pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>GT</span></b></p>
        </td>
        <td width=77 valign=top style='width:57.85pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Flag </span></b></p>
        </td>
        <td width=69 valign=top style='width:51.6pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Class</span></b></p>
        </td>
        <td width=56 valign=top style='width:41.7pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Crew</span></b></p>
        </td>
        <td width=68 valign=top style='width:51.1pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>IMO
  NO</span></b></p>
        </td>
        <td width=84 valign=top style='width:63.2pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Port
  Of Registry</span></b></p>
        </td>
    </tr>
    @php $total_vessels = count($mortgage) @endphp
    @if($total_vessels > 3)
        <tr>
            <td width=38 valign=top style='width:28.3pt;border-top:none;border-left:solid white 1.0pt;
  border-bottom:solid white 1.0pt;border-right:solid white 3.0pt;background:
  #5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>1</span></p>
            </td>
            @if($multi_insurer)
                <td width=100 valign=top style='width:75.15pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#ADCCEA;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ str_limit($mortgage[0]->insured,30) }}
  </span></p>
                </td>
            @endif
            <td width=100 colspan="{{ ($multi_insurer) ? 9 :  8 }}" valign=top style='width:75.15pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#ADCCEA;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $mortgage[0]->vessel_name }} &amp; {{ ($total_vessels - 1) }} others
  </span></p>
            </td>
        </tr>
        @else
        @foreach($mortgage as $no => $mort)
            <tr>
                <td width=38 valign=top style='width:28.3pt;border-top:none;border-left:solid white 1.0pt;
  border-bottom:solid white 1.0pt;border-right:solid white 3.0pt;background:
  #5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $no + 1 }}</span></p>
                </td>
                @if($multi_insurer)
                    <td width=100 valign=top style='width:75.15pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#ADCCEA;padding:0in 5.4pt 0in 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ str_limit($mort->insured,30) }}
  </span></p>
                    </td>
                @endif
                <td width=100 valign=top style='width:75.15pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#ADCCEA;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $mort->vessel_name }}
  </span></p>
                </td>
                <td width=69 valign=top style='width:51.6pt;border-top:none;border-left:none;
  border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;background:
  #ADCCEA;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'> {{ $mort->vessel_type }}</span></p>
                </td>
                <td width=69 valign=top style='width:51.6pt;border-top:none;border-left:none;
  border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;background:
  #ADCCEA;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $mort->built }}</span></p>
                </td>
                <td width=68 valign=top style='width:51.35pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#ADCCEA;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $mort->gt }}</span></p>
                </td>
                <td width=77 valign=top style='width:57.85pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#ADCCEA;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $mort->flag }}</span></p>
                </td>
                <td width=69 valign=top style='width:51.6pt;border-top:none;border-left:none;
  border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;background:
  #ADCCEA;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $mort->class }}</span></p>
                </td>
                <td width=56 valign=top style='width:41.7pt;border-top:none;border-left:none;
  border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;background:
  #ADCCEA;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $mort->crew }}</span></p>
                </td>
                <td width=68 valign=top style='width:51.1pt;border-top:none;border-left:none;
  border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;background:
  #ADCCEA;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>#####</span></p>
                </td>
                <td width=84 valign=top style='width:63.2pt;border-top:none;border-left:none;
  border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;background:
  #ADCCEA;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $mort->port_of_registry }}</span></p>
                </td>
            </tr>
        @endforeach
    @endif
</table>
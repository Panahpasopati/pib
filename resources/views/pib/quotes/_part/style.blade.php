<style>
    <!--
    /* Font Definitions */
    @font-face {
        font-family: Helvetica;
        panose-1: 2 11 6 4 2 2 2 2 2 4;
    }

    @font-face {
        font-family: Courier;
        panose-1: 2 7 4 9 2 2 5 2 4 4;
    }

    @font-face {
        font-family: Wingdings;
        panose-1: 5 0 0 0 0 0 0 0 0 0;
    }

    @font-face {
        font-family: "MS Mincho";
        panose-1: 2 2 6 9 4 2 5 8 3 4;
    }

    @font-face {
        font-family: "Cambria Math";
        panose-1: 2 4 5 3 5 4 6 3 2 4;
    }

    @font-face {
        font-family: Calibri;
        panose-1: 2 15 5 2 2 2 4 3 2 4;
    }

    @font-face {
        font-family: Consolas;
        panose-1: 2 11 6 9 2 2 4 3 2 4;
    }

    @font-face {
        font-family: Tahoma;
        panose-1: 2 11 6 4 3 5 4 4 2 4;
    }

    @font-face {
        font-family: Cambria;
        panose-1: 2 4 5 3 5 4 6 3 2 4;
    }

    @font-face {
        font-family: "Lucida Grande";
    }

    @font-face {
        font-family: Times-Roman;
    }

    @font-face {
        font-family: CalibriBold;
    }

    @font-face {
        font-family: TimesNewRomanPSMT;
    }

    @font-face {
        font-family: 0 \2202 ^ \02C7 ;
        panose-1: 0 0 0 0 0 0 0 0 0 0;
    }

    @font-face {
        font-family: ArialUnicodeMS;
    }

    @font-face {
        font-family: UniversCondensedBQ-Bold;
        panose-1: 0 0 0 0 0 0 0 0 0 0;
    }

    @font-face {
        font-family: UniversBQ-Bold;
        panose-1: 0 0 0 0 0 0 0 0 0 0;
    }

    @font-face {
        font-family: UniversBQ-Light;
        panose-1: 0 0 0 0 0 0 0 0 0 0;
    }

    @font-face {
        font-family: "\@MS Mincho";
        panose-1: 2 2 6 9 4 2 5 8 3 4;
    }

    /* Style Definitions */
    p.MsoNormal, li.MsoNormal, div.MsoNormal {
        margin-top: 0in;
        margin-right: 0in;
        margin-bottom: 10.0pt;
        margin-left: 0in;
        font-size: 12.0pt;
        font-family: "Cambria", serif;
    }

    h1 {
        mso-style-link: "Heading 1 Char";
        margin-top: 12.0pt;
        margin-right: 0in;
        margin-bottom: 3.0pt;
        margin-left: 0in;
        page-break-after: avoid;
        font-size: 16.0pt;
        font-family: "Cambria", serif;
    }

    p.MsoFootnoteText, li.MsoFootnoteText, div.MsoFootnoteText {
        mso-style-link: "Footnote Text Char";
        margin: 0in;
        margin-bottom: .0001pt;
        font-size: 12.0pt;
        font-family: "Cambria", serif;
    }

    p.MsoHeader, li.MsoHeader, div.MsoHeader {
        mso-style-link: "Header Char";
        margin: 0in;
        margin-bottom: .0001pt;
        font-size: 12.0pt;
        font-family: "Cambria", serif;
    }

    p.MsoFooter, li.MsoFooter, div.MsoFooter {
        mso-style-link: "Footer Char";
        margin: 0in;
        margin-bottom: .0001pt;
        font-size: 12.0pt;
        font-family: "Cambria", serif;
    }

    span.MsoFootnoteReference {
        vertical-align: super;
    }

    span.MsoEndnoteReference {
        vertical-align: super;
    }

    p.MsoEndnoteText, li.MsoEndnoteText, div.MsoEndnoteText {
        mso-style-link: "Endnote Text Char";
        margin: 0in;
        margin-bottom: .0001pt;
        font-size: 12.0pt;
        font-family: "Cambria", serif;
    }

    a:link, span.MsoHyperlink {
        color: blue;
        text-decoration: underline;
    }

    a:visited, span.MsoHyperlinkFollowed {
        color: #954F72;
        text-decoration: underline;
    }

    p.MsoPlainText, li.MsoPlainText, div.MsoPlainText {
        mso-style-link: "Plain Text Char";
        margin: 0in;
        margin-bottom: .0001pt;
        font-size: 11.0pt;
        font-family: "Calibri", sans-serif;
    }

    p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {
        mso-style-link: "Balloon Text Char";
        margin: 0in;
        margin-bottom: .0001pt;
        font-size: 9.0pt;
        font-family: "Lucida Grande";
    }

    span.HeaderChar {
        mso-style-name: "Header Char";
        mso-style-link: Header;
    }

    span.FooterChar {
        mso-style-name: "Footer Char";
        mso-style-link: Footer;
    }

    span.BalloonTextChar {
        mso-style-name: "Balloon Text Char";
        mso-style-link: "Balloon Text";
        font-family: "Lucida Grande";
    }

    p.BasicParagraph, li.BasicParagraph, div.BasicParagraph {
        mso-style-name: "\[Basic Paragraph\]";
        margin: 0in;
        margin-bottom: .0001pt;
        line-height: 120%;
        text-autospace: none;
        font-size: 12.0pt;
        font-family: Times-Roman;
        color: black;
    }

    span.EndnoteTextChar {
        mso-style-name: "Endnote Text Char";
        mso-style-link: "Endnote Text";
    }

    span.FootnoteTextChar {
        mso-style-name: "Footnote Text Char";
        mso-style-link: "Footnote Text";
    }

    p.MediumGrid21, li.MediumGrid21, div.MediumGrid21 {
        mso-style-name: "Medium Grid 21";
        margin: 0in;
        margin-bottom: .0001pt;
        font-size: 12.0pt;
        font-family: "Cambria", serif;
    }

    span.Heading1Char {
        mso-style-name: "Heading 1 Char";
        mso-style-link: "Heading 1";
        font-family: "Times New Roman", serif;
        font-weight: bold;
    }

    span.apple-style-span {
        mso-style-name: apple-style-span;
    }

    p.Default, li.Default, div.Default {
        mso-style-name: Default;
        margin: 0in;
        margin-bottom: .0001pt;
        text-autospace: none;
        font-size: 12.0pt;
        font-family: "Arial", sans-serif;
        color: black;
    }

    p.MediumList2-Accent21, li.MediumList2-Accent21, div.MediumList2-Accent21 {
        mso-style-name: "Medium List 2 - Accent 21";
        margin: 0in;
        margin-bottom: .0001pt;
        font-size: 12.0pt;
        font-family: "Cambria", serif;
    }

    p.ColorfulList-Accent11, li.ColorfulList-Accent11, div.ColorfulList-Accent11 {
        mso-style-name: "Colorful List - Accent 11";
        margin-top: 0in;
        margin-right: 0in;
        margin-bottom: 0in;
        margin-left: .5in;
        margin-bottom: .0001pt;
        font-size: 11.0pt;
        font-family: "Calibri", sans-serif;
    }

    span.PlainTextChar {
        mso-style-name: "Plain Text Char";
        mso-style-link: "Plain Text";
        font-family: "Calibri", sans-serif;
    }

    .MsoChpDefault {
        font-size: 10.0pt;
        font-family: "Cambria", serif;
    }

    /* Page Definitions */
    @page WordSection1 {
        size: 595.0pt 842.0pt;
        margin: 10.6pt 37.0pt 70.9pt 45.35pt;
    }

    div.WordSection1 {
        page: WordSection1;
    }

    /* List Definitions */
    ol {
        margin-bottom: 0in;
    }

    ul {
        margin-bottom: 0in;
    }

    -->
</style>
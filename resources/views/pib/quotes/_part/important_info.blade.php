<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:1.5in;margin-bottom:.0001pt;text-indent:-1.5in;text-autospace:none'><b><span
                style='font-size:9.0pt;font-family:Consolas;color:#262626'>Important Information  :</span></b>
</p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:1.5in;margin-bottom:.0001pt;text-indent:-1.5in;text-autospace:none'><span
            style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $data->important_information }}  </span>
</p>


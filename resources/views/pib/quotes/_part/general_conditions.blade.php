<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>General
Conditions :</span></b></p>


@if($data->general_conditions)
    @foreach(__add_mark_html_tag($data->general_conditions) as $gc)
        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $gc }}</span></p>
    @endforeach
@endif
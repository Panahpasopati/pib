<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Premium
:</span></b></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
       style='border-collapse:collapse;border:none'>
    <tr>
        <td width=158 valign=top style='width:118.55pt;border:solid white 1.0pt;
  border-bottom:solid white 3.0pt;background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Vessel
  Name </span></b></p>
        </td>
        <td width=113 valign=top style='width:85.05pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Type</span></b></p>
        </td>
        <td width=154 valign=top style='width:115.6pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Premium
  Type</span></b></p>
        </td>
        <td width=177 valign=top style='width:132.5pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Annual
  Premium </span></b></p>
        </td>
        @foreach($vessels as $v => $ves)
            @if($ves->discount)
                <td width=177 valign=top style='width:132.5pt;border-top:solid white 1.0pt;
  border-left:none;border-bottom:solid white 3.0pt;border-right:solid white 1.0pt;
  background:#5B9BD5;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Discount
  Premium </span></b></p>
                </td>
                @break
            @endif
        @endforeach
    </tr>
    <? $total = 0 ?>
    @foreach($vessels as $v => $ves)
        @php $subtotal = 0; @endphp
        <tr>
            <td width=158 valign=top style='width:118.55pt;border:solid white 1.0pt;
  border-top:none;background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $ves->vessel_name }}
  </span></p>
            </td>
            <td width=113 valign=top style='width:85.05pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $ves->type_ }}</span></p>
            </td>
            <td width=154 valign=top style='width:115.6pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $ves->premium_type }}</span></p>
            </td>
            <td width=177 valign=top style='width:132.5pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $data->currency }}  {{ number_format($ves->annual_premium) }}</span></p>
            </td>
            @if($ves->discount)
                <td width=177 valign=top style='width:132.5pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $ves->discount }} %</span></p>
                </td>
            @endif
        </tr>
        @if(\App\Http\Controllers\QuotesController::getAdditionalPremium($ves->premi_id))
            @foreach(\App\Http\Controllers\QuotesController::getAdditionalPremium($ves->premi_id) as $x => $add)
                <tr>
                    <td width=158 valign=top style='width:118.55pt;border:solid white 1.0pt;
  border-top:none;background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;
  </span></p>
                    </td>
                    <td width=113 valign=top style='width:85.05pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $add->type_ }}</span></p>
                    </td>
                    <td width=154 valign=top style='width:115.6pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $add->premium_type }}</span></p>
                    </td>
                    <td width=177 valign=top style='width:132.5pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $data->currency }}  {{ number_format($add->annual_premium) }}</span></p>
                    </td>
                    @foreach($vessels as $v => $ves)
                        @if($ves->discount)
                            <td width=158 valign=top style='width:118.55pt;border:solid white 1.0pt;
  border-top:none;background:#DEEAF6;padding:0in 5.4pt 0in 5.4pt'>
                                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;
  </span></p>
                            </td>
                            @break
                        @endif
                    @endforeach
                </tr>
                @php $subtotal = $subtotal + $add->annual_premium; @endphp
            @endforeach
        @endif

        <tr>
            <td width=158 valign=top style='width:118.55pt;border:solid white 1.0pt;
  border-top:none;background:#AEAAAA;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>
            </td>
            <td width=158 valign=top style='width:118.55pt;border:solid white 1.0pt;
  border-top:none;background:#AEAAAA;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>
            </td>

            <td width=113 valign=top style='width:85.05pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#AEAAAA;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>
            </td>
            @foreach($vessels as $v => $vesss)
                @if($vesss->discount)
                    <td width=113 valign=top style='width:85.05pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#AEAAAA;padding:0in 5.4pt 0in 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>
                    </td>
                    @break
                @endif
            @endforeach
            <td width=177 valign=top style='width:132.5pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#AEAAAA;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $data->currency }}
                            @php
                            $min = ($ves->discount/100) * $ves->annual_premium;
                                                        $subtotal = $subtotal + $ves->annual_premium;
                                                    $subtotal = $subtotal - $min;
                            @endphp
                            {{ number_format($subtotal) }}</span></b>
                </p>
            </td>
        </tr>


        <? $total += ($subtotal + \App\Http\Controllers\QuotesController::sumAnnualPremium($ves->premi_id)) ?>
    @endforeach
    <tr>
        <td width=158 valign=top style='width:118.55pt;border:solid white 1.0pt;
  border-top:none;background:#AEAAAA;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>
        </td>
        <td width=113 valign=top style='width:85.05pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#AEAAAA;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>
        </td>
        @foreach($vessels as $v => $ves)
            @if($ves->discount)
                <td width=154 valign=top style='width:115.6pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#AEAAAA;padding:0in 5.4pt 0in 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>
  </span></b></p>
                </td>
                @break
            @endif
        @endforeach
        <td width=154 valign=top style='width:115.6pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#AEAAAA;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Total
  </span></b></p>
        </td>
        <td width=177 valign=top style='width:132.5pt;border-top:none;border-left:
  none;border-bottom:solid white 1.0pt;border-right:solid white 1.0pt;
  background:#AEAAAA;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $data->currency }}
                        {{ number_format($data->PremiumAmount->amount_total,2) }}</span></b></p>
        </td>
    </tr>
</table>
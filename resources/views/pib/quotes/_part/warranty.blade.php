<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>
@if($message = $data->warranty)
    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Warranty
:</span></b></p>
    @foreach(__add_mark_html_tag($message) as $c)
        <p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.25in;text-autospace:none'><span style='font-size:9.0pt;
font-family:Consolas;color:#262626'>{{ $c }} </span></p>
    @endforeach
@endif
<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

@if($message = $data->survey_warranty)
    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Survey Warranty
:</span></b></p>
    @foreach(__add_mark_html_tag($message) as $c)
        <p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.25in;text-autospace:none'><span style='font-size:9.0pt;
font-family:Consolas;color:#262626'>{{ $c }} </span></p>
    @endforeach
@endif
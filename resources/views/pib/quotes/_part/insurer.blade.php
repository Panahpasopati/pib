<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Assured(s)
:          @foreach($data->Insurers as $insurer) <br>{{ $insurer->Insurer->insured }} @endforeach</span></b><span style='font-size:
9.0pt;font-family:Consolas;color:#262626'>  </span></p>
<br>
<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Joint
Assured(s) :    {{ $data->join_assured  }}</span></b></p>
<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Mortgagee
:          </span></b></p>
<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Quotation
</span></b><span style='font-size:9.0pt;font-family:Consolas;color:#262626;
background:yellow'>({{ $data->quotation_status }})</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Type
: </span></b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ htmlspecialchars($data->QuoteType->name) }}</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Our
Ref : </span></b><span style='font-size:9.0pt;font-family:Consolas;color:#262626;
background:yellow'>{{ $data->our_ref }}</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Dated
Issued : </span></b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626;background:yellow'>{{ date('d M Y',strtotime($data->date_issued)) }}</span></p>

<p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>Valid Until : </span></b><span style='font-size:9.0pt;
font-family:Consolas;color:#262626;background:yellow'>{{ date('d M Y',strtotime($data->valid_until)) }}</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>We attached below the quotation received from the Underwriter
for your consideration. </span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>For full details of the coverage and terms, please refer to policy
term, copy attached or website at </span></p>

<p class=MsoNormalCxSpMiddle><span class=MsoHyperlink><span style='font-size:
9.0pt;font-family:Consolas;color:#262626'><a href="{{ @$data->Security->website }}">{{ @$data->Security->website  }}
            </a></span></span><span
            style='font-size:9.0pt;font-family:Consolas;color:#262626'>  </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>The
terms and conditions of the Association/ The policy wording of the
Insurer are incorporated in their entirety into this
contract of insurance.</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626;background:yellow'> contained
herein are acceptable and entirely in accordance with your instructions. Please
advise us immediately of any necessary changes.</span><span style='font-size:
9.0pt;font-family:Consolas;color:#262626'>  </span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>We can only instruct the insurer(s) to effect cover when we have
received a written</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>instruction from the assured by way of signing this quotation by
a duly authorized person and receipt of the required
information and supporting documents.</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>Cover will be in full force once the insurer(s) has received
such subsequent instruction from us.                     We encourage
reasonable time to be provided to prepare our instruction to insurer(s), taking
into                     consideration the daily, weekly, or other occasional
closing time.</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>By signing this quotation you have noted and agreed that :</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:"Arial",sans-serif;
color:#262626'>&nbsp;</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>You officially appoint us to solely and exclusively represent
you to effect this insurance cover                     quoted herein and to
attend all pertaining issues to the insurer(s) on your behalf and that
all                     other similar instruction to other brokers/agents are
deemed to be null and void, and therefore                     superseded by
this appointment.</span></p>

<p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></p>

<p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>Please be sure to read it carefully to ensure that the terms, conditions contained herein are
entirely in accordance with your instructions </span></b><b><span
                style='font-size:9.0pt;font-family:Consolas;color:#262626'>and that the
Security with whom it will be placed, meet your approval and are acceptable to
you</span></b><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>,
and that we have made all reasonable efforts to bring to your attention the
relevant and                     significant issues of the terms and conditions
of this insurance and all regarding inquiries has                     been satisfactorily
responded by us.</span></b></p>

<p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>You have read and agreed to be bound to the terms, conditions
and disclaimers attaching to this quotation. </span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>
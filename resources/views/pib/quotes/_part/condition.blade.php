<p class=Default style='margin-left:1.5in;text-indent:-1.5in'><span
            style='font-family:"Times New Roman",serif'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Condition
:</span></b></p>


@if($message = __add_mark_html_tag($data->condition_))
    @foreach($message as $c)
        <p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.25in;text-autospace:none'><span style='font-size:9.0pt;
font-family:Consolas;color:#262626'>{{ $c }} </span></p>
    @endforeach
@endif
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
<!-- MAIN -->
<br>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="control-label col-md-4">Rate</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="rate" id="rate" value="{{ $quotes->rate }}">
                            <input type="hidden" class="form-control" id="quote_id" value="{{ $quotes->id }}">
                        </div>
                        <br>
                        <br>
                    </div>
                    <? $additional = 1 ?>
                    <? $total = 0 ?>
                    @foreach($vessels as $i => $v)
                        <form id="form-{{ $v->premi_id }}" method="post">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Vessel Name</th>
                                    <th>Sum Insured</th>
                                    <th>Premium</th>
                                </tr>
                                </thead>
                                <tbody id="{{ $v->premi_id }}">
                                <tr>
                                    <td>{{ $v->vessel_name }}</td>
                                    <input type="hidden" name="id_premi" value="{{ $v->premi_id }}">
                                    <td><input type="text" id="sum-{{ $v->premi_id }}" name="sum_insured[0]" onkeypress="Calculate({{ $v->premi_id }})" value="{{ $v->sum_insured }}" class="form-control"></td>
                                    <td><input type="text" id="annual-{{ $v->premi_id }}" name="annual_premium[0]" value="{{ $v->annual_premium }}" class="form-control"></td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td></td>
                                    <td><button type="button" onclick="saveForm({{ $v->premi_id }})" class="btn btn-primary">Save</button> </td>
                                </tr>
                                </tfoot>
                            </table>
                        </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
<script>
    function saveForm(id){
        var data = $("#form-"+id).serialize();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        $.ajax({
            type: 'post',
            url: '../update_hm_premium_quotes',
            data: {data: data,rate:$("#rate").val(),quote_id:$("#quote_id").val()},
            success: function (data) {
                alert(data)
                location.reload();
            }
        })
    }

    function Calculate(id){
        var rate = $("#rate").val();
        var count = (rate/100) * parseFloat($("#sum-"+id).val());
        $("#annual-"+id).val(count);
    }
</script>
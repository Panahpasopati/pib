<!-- MAIN -->
<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
<script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h4 class="page-title">Create General Premium</h4>
            <form method="post" action="{{ url('update_general_premium') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-responsive table-striped">
                            @foreach($data as $y => $v)
                                <tr>
                                    <td>{{ $y + 1 }}</td>
                                    <td>{{ $v->insurer_name }}</td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="form-group">
                            <label class="control-label"></label>
                            <table style="margin-left: 20px;" class="table table-condensed">
                                <thead>
                                <tr>
                                    <th width="5%">No.</th>
                                    <th width="20%">Sum Insured</th>
                                    <th width="10%">Regulation rate</th>
                                    <th width="20%">Premi</th>
                                </tr>
                                </thead>
                                <!--elemet sebagai target append-->
                                <tbody>
                                @foreach($data as $i => $d)
                                    <tr>
                                        <td>
                                            {{ $i + 1 }}
                                        </td>
                                        <td>
                                            <input id="sum_insured-{{ $i }}" type="text" class="form-control" name="sum_insured[{{ $d->id }}]"  placeholder="Sum Insured">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="regulation_rate_name[{{ $d->id}}]"  placeholder="Name">
                                            <input id="rate-{{ $i }}" type="text" onkeypress="premiCount({{ $i }})" class="form-control" name="regulation_rate_value[{{ $d->id }}]"  placeholder="Value (%)">
                                        </td>
                                        <td>
                                            <input id="premi-{{ $i }}" type="text" class="form-control" name="premi[{{ $d->id }}]" placeholder="Premi">
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td><button type="button" onclick="calculate()" class="btn btn-defaul">Calculate</button> </td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td>
                                        <input type="text" id="total_sum_insured" class="form-control" required="">
                                        <input type="hidden" name="total_sum_insured"  class="form-control" required="">
                                    </td>
                                    <td>
                                        <input type="text" id="total_regulation_rate" class="form-control" required="">
                                        <input type="hidden" name="total_regulation_rate"  class="form-control" required="">
                                    </td>
                                    <td>
                                        <input type="text" id="total_premi" class="form-control" required="">
                                        <input type="hidden" name="total_premi" class="form-control" required="">
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script>
    var sum = 0;
    var rate = 0;
    var premi = 0;
    function calculate(){
        for(var i = 0; i<{{ count($data) }}; i++){
            if($("#sum_insured-"+i).val() == ""){
                sum = sum + 0;
            }else{
                sum = sum + parseInt($("#sum_insured-"+i).val());
            }
            if($("#rate-"+i).val() == ""){
                rate = rate + 0;
            }else{
                rate = rate + parseFloat($("#rate-"+i).val());
            }
            if($("#premi-"+i).val() == ""){
                premi = premi + 0;
            }else{
                premi = premi + parseInt($("#premi-"+i).val());
            }
            console.log(parseFloat($("#rate-"+i).val()))
            console.log(parseInt($("#sum_insured-"+i).val()))
        }
        $("#total_sum_insured").val(sum.toPrecision());
        $("input[name='total_sum_insured']").val(sum);
        $("#total_regulation_rate").val(rate);
        $("input[name='total_regulation_rate']").val(rate);
        $("#total_premi").val(premi.toPrecision());
        $("input[name='total_premi']").val(premi);
        sum = 0;
        rate = 0;
        premi = 0;
    }

    function premiCount(i){
        var value;
        value = (parseFloat($("#rate-"+i).val() / 100)) * parseInt($("#sum_insured-"+i).val());
        $("#premi-"+i).val(value.toPrecision());
    }
</script>
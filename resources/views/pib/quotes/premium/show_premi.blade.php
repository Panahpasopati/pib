@extends('layouts.pib')
@section('content')
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-title">Create Premium</h1>
                </div>
                <div class="col-md-12">
                    <form method="post" action="{{ url('quotation',['premium','save']) }}">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{ $data->id }}" id="temp_id" name="temp_id">
                        @if($data->Insurers)
                            @foreach($data->Insurers as $insurer)
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">{{ str_limit($insurer->Insurer->insured,40) }}</label>
                                        <div class="pull-right"><button onclick="openModal({{$insurer->insurer_id}},{{$insurer->quote_id}})" type="button" class="btn btn-link btn-xs"><span class="fa fa-plus-square"></span></button> </div>
                                        <select id="select-vessel-{{$insurer->insurer_id}}" onchange="changeVessels({{$insurer->insurer_id}})" name="mortgage[]" class="form-control mortgage" multiple="multiple" required="">
                                            @if($vessels = $insurer->Insurer->Vessels)
                                                @foreach($vessels as $vessel)
                                                    <option @if(in_array($vessel->id,$quoteVessels)) selected @endif value="{{ $vessel->id }}">{{ $vessel->vessel_name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="table-vessel">

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Premium <span class="btn btn-xs btn-link" role="button" onclick="location.reload()"><i>refresh</i></span></label>
                                <iframe class="premium_iframe" id="bsm_iframe" src="{{ url('quotation',['premium','show_premium_quotes_iframe',$data->id]) }}" height="400" width="100%" style="border: 0px;"></iframe>
                            </div>
                        </div>
                        <hr><br>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="panel panel-default" id="filter">
                                    <div class="panel-body text-center">
                                        <ul class="font-md font-bold">
                                            <button class="btn-link" type="submit" ><li class="hvr-blue">Save</li></button>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add More Vessels</h4>
                </div>
                <div class="">
                    <form id="form-add-new-vessel">
                        <input type="hidden" value="" name="insurer_id">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Vessel Name</label>
                                <input type="text" class="form-control" id="vessel_name" name="vessel_name">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Vessel Type</label>
                                <input type="text" class="form-control" id="vessel_type" name="vessel_type">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Built</label>
                                <input type="text" class="form-control" id="built" name="built">
                            </div>
                            <div class="form-group">
                                <label class="control-label">GT</label>
                                <input type="text" class="form-control" id="gt" name="gt">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Flag</label>
                                <input type="text" class="form-control" id="flag" name="flag">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Class</label>
                                <input type="text" class="form-control" id="class" name="class">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Crew</label>
                                <input type="text" class="form-control" id="crew" name="crew">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Port of Registry</label>
                                <input type="text" class="form-control" id="port_of_registry" name="port_of_registry">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <div class="form-group add-vessel-button">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    @endsection

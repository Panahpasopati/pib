<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
<br>
<style>
    tr {
        font-size: 12px !important;
        padding: 5px !important;
    }

    td {
        padding: 5px !important;
    }

    th {
        padding: 5px !important;
    }

    input.form-control {
        height: 30px;
        padding: 4px 10px;
        font-size: 12px;
    }
</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8">
                    <? $additional = 0 ?>
                    <? $total = 0 ?>
                    @foreach($vessels as $i => $v)
                        <form id="form-{{ $v->premi_id }}" method="post">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Vessel Name</th>
                                    <th>Type</th>
                                    <th>Premium Type</th>
                                    <th>Annual Premium</th>
                                    <th>Discount(%)</th>
                                </tr>
                                </thead>
                                <tbody id="{{ $v->premi_id }}">
                                <tr>
                                    <td>{{ $v->vessel_name }}</td>
                                    <td>
                                        <input type="text" name="type_[0]" value="{{ $v->type_ }}" class="form-control">
                                        <input type="hidden" name="id_premi" value="{{ $v->premi_id }}">
                                    </td>
                                    <td><input type="text" name="premium_type[0]" value="{{ $v->premium_type }}" class="form-control"></td>
                                    <td>
                                        <input type="text" id="annual_premium_{{ $v->id }}" name="annual_premium[0]" onkeypress="savePremi({{ $v->premi_id }})" value="{{ $v->annual_premium }}" placeholder="" class="form-control">
                                        <small class="text-info" style="font-size: 10px;text-align: right;"><i class="fa fa-info-circle"></i> Tekan spasi untuk menyimpan.</small>
                                    </td>
                                    <td><input type="text" id="discount_{{ $v->id }}" name="discount[0]" onkeypress="savePremi({{ $v->premi_id }})" value="{{ $v->discount }}" placeholder="" class="form-control"></td>
                                </tr>
                                @if(\App\Http\Controllers\QuotesController::getAdditionalPremium($v->premi_id))
                                    @foreach(\App\Http\Controllers\QuotesController::getAdditionalPremium($v->premi_id) as $x => $add)
                                        <tr id="tr-add-{{ $add->id }}">
                                            <td></td>
                                            <td><input type="text" name="add_type_[{{ $x }}]" value="{{ $add->type_ }}" class="form-control"></td>
                                            <td><input type="text" name="add_premium_type[{{ $x }}]" value="{{ $add->premium_type }}" class="form-control"></td>
                                            <td>
                                                <input type="text" id="annual_premium_{{ $v->id }}" onkeydown="savePremi({{ $v->premi_id }})" name="add_annual_premium[{{ $x  }}]" value="{{ $add->annual_premium }}" placeholder="" class="form-control">
                                                <small class="text-info" style="font-size: 10px;text-align: right;"><i class="fa fa-info-circle"></i> Tekan spasi untuk menyimpan.</small>
                                            </td>
                                            <input type="hidden" name="add_id[{{ $x }}]" value="{{ $add->id }}" placeholder="" class="form-control">
                                            <td><button type="button" class="btn btn-small btn-default" onclick="deleteAdditional({{ $add->id }},{{ $v->premi_id }})"><i class="fa fa-trash"></i></button></td>
                                        </tr>
                                        <? $additional += 1 ?>
                                    @endforeach
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td><a href="#" onclick="deletePremi({{ $v->premi_id }},{{ $v->id_vessel }})" class="btn btn-danger"><small>Delete</small></a></td>
                                    <td><button type="button" onclick="addRow({{ $v->premi_id }},{{ count(\App\Http\Controllers\QuotesController::getAdditionalPremium($v->premi_id)) + 1 }})" class="btn btn-default">Add Additional</button> </td>
                                    <td>Sub-Total</td>
                                    <?
                                    $min = ($v->discount*doubleval($v->annual_premium)) /100;
                                    $subtotal = $v->annual_premium - $min;
                                    ?>
                                    <td><input type="text" class="form-control" id="subtotal_{{ $v->premi_id }}" value="{{  $subtotal + \App\Http\Controllers\QuotesController::sumAnnualPremium($v->premi_id) }}"> </td>
                                </tr>
                                </tfoot>
                            </table>
                        </form>
                        <? $total += ( $subtotal + \App\Http\Controllers\QuotesController::sumAnnualPremium($v->premi_id)) ?>
                    @endforeach
                </div>
                <div class="col-md-4">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <td>Total</td>
                            <td><input type="text" style="font-weight: 700; color: red;" class="form-control" id="total" name="total_premium" value="{{ @$total }}"> </td>
                        </tr>
                        <? @\App\Http\Controllers\QuotesController::updateTotalDue($v->quotes_id,$total) ?>
                        <tr>
                            <td> </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>

<script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
<script>
    $(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
    })
    function savePremi(id){
        setTimeout(function(){
            var data = $("#form-"+id).serialize();
            $.ajax({
                type: 'post',
                url: '../update_premium_quotes_onchange',
                data: {data: data,add_num:{{ $additional }}},
                success: function (data) {
                    $("#total").val(data.total);
                    $("#subtotal_"+id).val(data.subtotal);
                }
            })
        },1000)
    }
    function deletePremi(id,v)
    {
        $.ajax({
            type: 'post',
            url: '../delete_v_premium',
            data: {data: id,vessel:v},
            success: function (data) {
                location.reload();
            }
        });
        return false;
    }
    function deleteAdditional(id,pid)
    {
        $.ajax({
            type: 'post',
            url: '../delete_additional_premium',
            data: {data: id},
            success: function (data) {
                $("#tr-add-"+id).remove();
                setTimeout(function(){
                    savePremi(pid)
                },500);
            }
        })
    }
    function addRow(id,num) {
        $.ajax({
            type: 'post',
            url: '../create_initial_additional_premium',
            data: {data: id},
            success: function (data) {
                if(data.success){
                    createNewRow(data.pid,data.id)
                }
            }
        });
    }
    var i = {!! $additional !!};
    function createNewRow(pid,id){
        var itemlist = document.getElementById(pid);
        var row = document.createElement('tr');
        var b = document.createElement('td');
        var c = document.createElement('td');
        var d = document.createElement('td');
        var e = document.createElement('td');
        var aksi = document.createElement('td');

        itemlist.appendChild(row);
        row.appendChild(b);
        row.appendChild(c);
        row.appendChild(d);
        row.appendChild(e);
        row.appendChild(aksi);

        row.setAttribute('id','tr-add-'+id);
        var insured_name = document.createElement('input');
        insured_name.setAttribute('name', 'add_type_[' + i + ']');
        insured_name.setAttribute('class', 'form-control');

        var insured_address = document.createElement('input');
        insured_address.setAttribute('name', 'add_premium_type[' + i + ']');
        insured_address.setAttribute('class', 'form-control');

        var insured_telp = document.createElement('input');
        insured_telp.setAttribute('name', 'add_annual_premium[' + i + ']');
        insured_telp.setAttribute('class', 'form-control');
        insured_telp.setAttribute('onkeypress','savePremi('+pid+')');

        var small = document.createElement('small');
        small.setAttribute('style','font-size: 10px;text-align: right;')
        small.setAttribute('class','text-info')

        var hapus = document.createElement('span');
        c.appendChild(insured_name);
        d.appendChild(insured_address);
        e.appendChild(insured_telp);
        e.appendChild(small);
        small.innerHTML = '<i class="fa fa-info-circle"></i> Tekan spasi untuk menyimpan.';
        aksi.appendChild(hapus);
        hapus.innerHTML = '<button type="button" onclick="deleteAdditional('+id+','+pid+')" class="btn btn-small btn-default"><i class="fa fa-trash"></i></button>';

        i++;
    }
</script>
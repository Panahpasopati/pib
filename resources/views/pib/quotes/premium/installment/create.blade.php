<div>
    <input type="hidden" value="{{ $data->id }}" name="temp_id">
    <div class="form-group">
        <label class="control-label col-md-4">Premium Warranty</label>

        <table style="margin-left: 20px;" class="table table-condensed">
            <thead>
            <tr>
                <th width="20%">Installment Number</th>
                <th width="60%">Due Date</th>
                <th width="10%">Divider %</th>
                <th></th>
            </tr>
            </thead>
            <!--elemet sebagai target append-->
            <tbody id="itemlist">
            <tr>
                <td>
                    <input name="instalment_number[0]" class="form-control" type="number" min="1" required="">
                </td>
                <td>
                    <input type="text" class="form-control due_date_inst" name="due_date[0]" required="">
                </td>
                <td>
                    <input type="text" class="form-control" name="value_[0]" required="">
                </td>
                <td></td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2"></td>
                <td>
                    <button class="btn btn-small btn-default" type="button" onclick="additem(); return false"><i class="fa fa-plus"></i></button>

                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
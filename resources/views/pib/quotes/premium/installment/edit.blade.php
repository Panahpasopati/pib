<div>
    <input type="hidden" value="{{ $data->id }}" name="temp_id">
    <div class="form-group">
        <label class="control-label col-md-4">Premium Warranty</label>
        <table style="margin-left: 20px;" class="table table-condensed">
            <thead>
            <tr>
                <th width="20%">Installment Number</th>
                <th width="60%">Due Date</th>
                <th width="10%">Divider %</th>
                <th></th>
            </tr>
            </thead>
            <!--elemet sebagai target append-->
            <tbody id="itemlist">
            @foreach($data->Installment as $i => $val)
                <tr id="tr-{{ $val->id }}">
                    <td>
                        <input name="instalment_number[{{ $i }}]" class="form-control" type="number" min="1" value="{{ $val->instalment_number }}">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="due_date[{{ $i }}]" value="{{ $val->due_date }}">
                    </td>
                    <td>
                        <input type="text" class="form-control" name="value_[{{ $i }}]" value="{{ $val->value_ }}">
                    </td>
                    <td><button type="button" class="btn btn-small btn-default" onclick="deletePremiumWarranty({{ $val->id }})"><i class="fa fa-trash"></i></button></td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2"></td>
                <td>
                    <button class="btn btn-small btn-default" type="button" onclick="additem(); return false"><i class="fa fa-plus"></i></button>

                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
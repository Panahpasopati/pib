<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
<!-- MAIN -->
<br>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <h4 class="page-title">Add Cargo Premium</h4>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8">
                    <form method="post" action="{{ url('update_cargo_premium') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label">Sum Insured</label>
                            <div class="input-group">
                                <span class="input-group-addon">IDR</span>
                                <input type="text" class="form-control" value="{{ $data->sum_insured }}" name="sum_insured" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Rate</label>
                            <div class="input-group">
                                <input type="text" class="form-control" value="{{ $data->rate }}" name="rate" required="">
                                <span class="input-group-addon">%</span>
                            </div>
                            <input type="hidden" class="form-control" name="quotes_id" value="{{ $id }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Policy Cost & Stamp Duty</label>
                            <div class="input-group">
                                <span class="input-group-addon">IDR</span>
                                <input type="text" class="form-control" value="{{ $data->policy_cost_stamp_duty }}" name="policy_cost_stamp_duty" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
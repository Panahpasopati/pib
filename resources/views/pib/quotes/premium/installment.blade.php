@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="page-title">Premium Installment</h4>
                <form method="post" class="form-horizontal" action="{{ url('quotation',['premium','save_premium_warranty']) }}" id="form-create-quote">
                    {{ csrf_field() }}
                    @php $installment_count = 1 @endphp
                    @if($data->Installment && count($data->Installment))
                        @php $installment_count = count($data->Installment) @endphp
                        @include('pib.quotes.premium.installment.edit')
                        @else
                        @include('pib.quotes.premium.installment.create')
                        @endif
                    <hr>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Commission</label>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Intermediary</th>
                                <th>Brokerage</th>
                                <th>VAT</th>
                                <th>WHT</th>
                                <th>Policy Cost</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <input type="text" {{ !$success ? 'disabled' : '' }} value="{{ $intermediary }}" name="intermediary_name" class="form-control comm">
                                </td>
                                <td>
                                    <div class="input-group">
                                        <input required type="text" {{ !$success ? 'disabled' : '' }}  name="brokerage" class="form-control comm">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <input required type="text" {{ !$success ? 'disabled' : '' }}  name="vat" class="form-control comm">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <input required type="text" {{ !$success ? 'disabled' : '' }}  name="wht" class="form-control comm">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <input type="text" {{ !$success ? 'disabled' : '' }}  name="policy_cost" class="form-control comm">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5"><p class="text text-info result-comm">@if(!$success){{ $message }} @for($i = 0; $i < count($fix_link); $i++)<a href="{{ $fix_link[$i] }}" target="_blank">Fix</a>@endfor @endif</p> </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <br>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel panel-default" id="filter">
                                <div class="panel-body text-center">
                                    <ul class="font-md font-bold">
                                        <button class="btn-link" type="submit" ><li class="hvr-blue">Save</li></button>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script>
    var i = {!! $installment_count !!};
    function additem() {
        var itemlist = document.getElementById('itemlist');

        var row = document.createElement('tr');
        var b = document.createElement('td');
        var c = document.createElement('td');
        var d = document.createElement('td');
        var aksi = document.createElement('td');
        itemlist.appendChild(row);
        row.appendChild(b);
        row.appendChild(c);
        row.appendChild(d);
        row.appendChild(aksi);
        var insured_name = document.createElement('input');
        insured_name.setAttribute('name', 'instalment_number[' + i + ']');
        insured_name.setAttribute('class', 'form-control');
        insured_name.setAttribute('required', '');
//
        var val = $(".quotation-status").val(), due_inst = '';

        if(val != 'New'){
            //due_inst += 'datepicker'
        }
        var insured_address = document.createElement('input');
        insured_address.setAttribute('name', 'due_date[' + i + ']');
        insured_address.setAttribute('class', 'form-control due_inst '+due_inst);
        insured_address.setAttribute('required', '');

        var insured_value_ = document.createElement('input');
        insured_value_.setAttribute('name', 'value_[' + i + ']');
        insured_value_.setAttribute('class', 'form-control');
        insured_value_.setAttribute('required', '');
//
//
        var hapus = document.createElement('span');
        b.appendChild(insured_name);
        c.appendChild(insured_address);
        d.appendChild(insured_value_);
        aksi.appendChild(hapus);
        hapus.innerHTML = '<button type="button" class="btn btn-small btn-default"><i class="fa fa-trash"></i></button>';
//                membuat aksi delete element
        hapus.onclick = function () {
            row.parentNode.removeChild(row);
        };
        $('.datepicker').datepicker({
            format: "mm/dd/yyyy"
        });

        i++;
    }
    function deletePremiumWarranty(id){
        $("#tr-"+id).remove();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: '{!! url('quotation',['premium','delete_premium_warranty']) !!}',
            data: {id:id},
            success: function (data) {
            }
        })

    }
</script>
@endsection
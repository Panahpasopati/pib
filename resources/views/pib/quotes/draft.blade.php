@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <!-- BORDERED TABLE -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Draft Quotes List</h3>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered data-table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Quotation Status</th>
                                        <th>Type</th>
                                        <th>Ref.</th>
                                        <th>Created At</th>
                                        <th>Created By</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $i => $d)
                                        <tr>
                                            <td>{{ $i + 1}}</td>
                                            <td>{{ $d->quotation_status }}</td>
                                            <td>{{ @$d->QuoteType->name }}</td>
                                            <td>{{ $d->our_ref }}</td>
                                            <td><strong>{{ date('d M Y H:i',strtotime($d->created_at)) }}</strong></td>
                                            <td><i>{{ @$d->CreatedBy->name }}</i></td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-warning" href="{{ route('quote.edit',$d->id) }}" title="Continue?"><span class="fa fa-edit"></span></a>
                                                    <a class="btn btn-danger" href="{{ url('quotation',['quotes_draft_delete',$d->id]) }}" title="Delete?"><span class="fa fa-trash"></span></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $data->render() }}
                            </div>
                        </div>
                        <!-- END BORDERED TABLE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

    <!-- END MAIN -->
@endsection
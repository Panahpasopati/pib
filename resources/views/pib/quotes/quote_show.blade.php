@extends('layouts.pib')
@section('content')
    @include('pib.quotes._part.style')
<div class="main" id="print-area">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class=WordSection1>
                        @include('pib.quotes._part.header')
                        @include('pib.quotes._part.insurer')
                        @include('pib.quotes._part.vessels')
                        @include('pib.quotes._part.period')
                        @include('pib.quotes._part.trading_limit')
                        @include('pib.quotes._part.condition')
                        @include('pib.quotes._part.warranty')
                        @include('pib.quotes._part.limit_of_cover')
                        @include('pib.quotes._part.deductibles')
                        @include('pib.quotes._part.general_conditions')
                        @include('pib.quotes._part.important_info')
                        @include('pib.quotes._part.premium')

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>All
amounts are in <b>{{ $data->currency }} </b>per annum and pro rata, unless otherwise stated. </span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>The
above premium is the amount due to the insurer. Any local taxes must be added
to the above amount and paid separately. They may not be
deducted                     from the premium due to the insurer.</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Premium
Warranty :    </span></b></p>

                        <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
                               style='border-collapse:collapse;border:none'>
                            <tr>
                                <td width=139 valign=top style='width:1.45in;border:solid #A5A5A5 1.0pt;
  border-right:none;background:#A5A5A5;padding:0in 5.4pt 0in 5.4pt'>
                                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Instalment
  Number </span></b></p>
                                </td>
                                <td width=227 valign=top style='width:170.35pt;border:solid #A5A5A5 1.0pt;
  border-left:none;background:#A5A5A5;padding:0in 5.4pt 0in 5.4pt'>
                                    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><b><span style='font-size:9.0pt;font-family:Consolas;color:white'>Due
  Date</span></b></p>
                                </td>
                            </tr>
                            @if($premium_warranty)
                                @foreach($premium_warranty as $pw)
                                    <tr>
                                        <td width=139 valign=top style='width:1.45in;border:solid #C9C9C9 1.0pt;
  border-top:none;background:#EDEDED;padding:0in 5.4pt 0in 5.4pt'>
                                            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'>
                                                <b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b>{{ $pw->instalment_number }}
                                            </p>
                                        </td>
                                        <td width=227 valign=top style='width:170.35pt;border-top:none;border-left:
  none;border-bottom:solid #C9C9C9 1.0pt;border-right:solid #C9C9C9 1.0pt;
  background:#EDEDED;padding:0in 5.4pt 0in 5.4pt'>
                                            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
  none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'> </span>{{ date('Y',strtotime($pw->due_date)) == '1970' ? $pw->due_date : date('d M Y',strtotime($pw->due_date)) }}</p>
                                        </td>
                                    </tr>
                                @endforeach
                                @endif
                        </table>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>The above instalment pattern may be subject to change depending
on the bound cover period.</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Warranted
full annual premium to be paid in case of an Actual Total Loss and/or Constructive
Total Loss of Vessel.</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Breach
of premium warranty may lead to rejection of all claims whether arising before
or after the breach.</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Security
:    </span></b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ @$data->Security->assured_name }}</span></p>
                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>

                                    </span></b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ @$data->Security->description }}</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'> </span></p>

                        @if($message = $data->subjectivities)
                            @foreach(__add_mark_html_tag($message) as $c)
                                <p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:1.25in;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.25in;text-autospace:none'><span style='font-size:9.0pt;
font-family:Consolas;color:#262626'>{{ $c }} </span></p>
                            @endforeach
                        @endif
                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Information
given : </span></b></p>
                        @if($list = get_list_content($data->information_given))
                        @foreach($list as $ig)
                            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $ig }}</span></p>
                        @endforeach
                        @endif
                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><u><span style='font-size:9.0pt;font-family:Consolas;color:red'>Information
required prior to cover :</span></u></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:red'>-</span></b><b><span
                                        style='font-size:9.0pt;font-family:Consolas;color:red'>Please confirm the info
under information section are of accuracy. </span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:red'>&nbsp;</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Comment
:</span></b></p>

                        @if($list = get_list_content($data->comment))
                        @foreach($list as $com)
                            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>{{ $com }}</span></p>
                        @endforeach
                        @endif

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>E
&amp;.O.E                                               </span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;color:#262626'><img border=0 width=137
                                                       height=53 id="Picture 1"
                                                       src="{{ asset('assets/img/image_ttd.png') }}"></span>
                        </p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>For and
on behalf of </span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>PT.
Pacific Indonesia Berjaya</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Insurance
Brokers &amp; Risk Consultants                     Authorized signatory</span></p>
                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Agreed and accepted by</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>


                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>


                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'> ………………………………………………………………</span></p>
                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Authorized signatory</span></p>



                        <p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Disclaimer:</span></b></p>

                        <p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>To the best of our knowledge, the information supplied in this
document is accurate.</span></p>

                        <p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>PT. Pacific Indonesia Berjaya Insurance Brokers &amp; Risk
Consultants accepts no liability </span></p>

                        <p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>for any loss arising out of your reliance on information which
has been supplied to </span></p>

                        <p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>PT. Pacific Indonesia Berjaya Insurance Brokers &amp; Risk
Consultants by or on behalf of </span></p>

                        <p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>PT. Pacific Indonesia Berjaya Insurance Broker &amp; Risk
Consultants’ clients.</span></p>

                        <p class=MsoNormalCxSpMiddle style='text-indent:.5in'><span style='font-size:
9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Duty
of Disclosure</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>For an
insurer to decide to take a risk and then to make an informed assessment of the
risk it                     faces under a contract of insurance, (and hence
calculate the premium it should charge) all relevant
matters must be disclosed in the utmost good faith to it. The duty to make full
and frank disclosure                     rests upon the party seeking insurance
cover (or renewal or variation of it) and not upon the
broker                     who acts on that party behalf.</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Your
Duty of Disclosure</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>We would
highlight that an Assured under a policy has a duty to disclose to the Insurer
of every                     matter that they know, or could reasonably be
expected to know is relevant to the Insurer’s decision
whether to accept and/or renew the risk of the insurance and, if so, on what
terms.&nbsp; You have the                     same duty to disclose those
matter to the insurer before you renew, extend, vary or reinstate
a                     contract of general insurance.<b>&nbsp;</b>Should there
be a failure to comply with the Duty of Disclosure, the Insurer may be entitled
to                     reduce his liability under the Contract of Insurance
in&nbsp;respect of a claim or may cancel the                     Contract of
Insurance and if the non-disclosure is fraudulent, the Insurer may also have
the option                     of avoiding the contract from its beginning</span></p>

                        <p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Utmost
Good Faith</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Every
contract of insurance is based on “utmost good faith” which requires each party
(i.e. you and                     the insurer) to act towards the other party
in respect of any matter arising under the contract,                     with
the utmost good faith. If you fail to do so you may prejudice any claim.</span></p>

                        <p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></p>

                        <p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>Client’s Responsibilities</span></b></p>

                        <p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>The Client is responsible for the accuracy of all information
given to us. While we will assist the Client with the completion of proposals,
claims forms or other documents relating to the services, we do not accept
responsibility and shall not be held liable for the accuracy of any
answers,                     statements or information, nor can we sign any
document on the Client’s behalf.</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><u><span style='font-size:9.0pt;font-family:Consolas;color:#262626'><span
                                                style='text-decoration:none'>&nbsp;</span></span></u></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Essentials
Reading of Policy Wording</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>A full
copy of your policy wording has been issued or will be passed to you as soon as
it is                     received from Insurers.&nbsp;It is absolutely
essentials that you should read this document without delay
and advise PT Pacific Indonesia Berjaya in writing of any aspects which are not
clear or where the                     cover does not meet with your
requirements.</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Warranties,
Subjectivities and Special Conditions</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>If any
warranties, subjectivities and special conditions are shown on your cover note,
quotation                     slips, insurance coverage summary or policy,
please make sure that you understand them and are able                     to
follow their requirements exactly. If not, please advise us immediately, as a
breach of warranty,                     subjectivity and special condition will
enable the underwriter to terminate the policy from the
date                     of that breach. This is the position regardless of
whether there is any connection between the                     warranty,
subjectivity and special condition breached and the loss which leads to that
breach                     becoming evident.</span></p>

                        <p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>Insurer’s Security &amp; Claims</span></b></p>

                        <p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>We cannot and does not guarantee the solvency or continued
solvency of any insurer or its ability                     or willingness to
pay claims, return premiums or to meet its other financial obligations.</span></p>

                        <p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>Payment of Premium </span></b></p>

                        <p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>It is a condition that the premium shall be paid to insurers on
or before the due date. In the                     event the premium not having
been paid to insurers by such date the insurance will be lapsed on
such                     due date and insurers will entitled to a pro-rata time
on risk premium. Premium payment to us at                     least 7 (seven)
days before due date will enable us to pay insurers by the required date. We
will                     not be responsible for any consequences that may arise
from any delay or failure by you to pay us                     the amount
payable by the indicated date.</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><b><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>Claims
Notification</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span style='font-size:9.0pt;font-family:Consolas;color:#262626'>In case
of an incident which may give rise to a claim under the insurance, prompt
notification must                     be given</span><span style='font-size:
9.0pt;font-family:Consolas;color:#262626'>. All policies are subject to claims
reporting obligations that require you to notify your
insurers                     as soon as any potential claim is known to you. This
is to avoid denial of coverage by insurers due                     to late
reporting. If you have any questions about reporting claims, please contact us
(PT Pacific                     Indonesia Berjaya) for more information.</span></p>

                        <p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>Confidentiality </span></b></p>

                        <p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>Unless the Client advise us not to, we may disclose the
information provides to us, to other                     organizations where we
believes it is necessary to assist us and the other organizations
in                     providing the services. Recipients will but not
restricted to be insurers, reinsurers, other                     insurance
intermediaries, employers, health workers, investigators, lawyers and loss
adjusters.</span></p>

                        <p class=MsoNormalCxSpMiddle style='margin-left:.5in'><span style='font-size:
9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>

                        <p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>Services</span></b></p>

                        <p class=MsoNormalCxSpMiddle><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>The services we will perform are: </span></p>

                        <p class=MsoNormalCxSpMiddle style='margin-bottom:0in;margin-bottom:.0001pt'><span
                                    style='font-size:9.0pt;font-family:Consolas;color:#262626'>-to gather
information from the Client to enable us to obtain insurance quotes from the
insurance                     market;</span></p>

                        <p class=MsoNormalCxSpMiddle style='margin-bottom:0in;margin-bottom:.0001pt'><span
                                    style='font-size:9.0pt;font-family:Consolas;color:#262626'>-to provide the
quotes obtained from the insurance market to the Client;</span></p>

                        <p class=MsoNormalCxSpMiddle style='margin-bottom:0in;margin-bottom:.0001pt'><span
                                    style='font-size:9.0pt;font-family:Consolas;color:#262626'>-upon receipt of
instructions from the Client, to place the cover as instructed;</span></p>

                        <p class=MsoNormalCxSpMiddle style='margin-bottom:0in;margin-bottom:.0001pt'><span
                                    style='font-size:9.0pt;font-family:Consolas;color:#262626'>-to assist the Client
to lodge claims and where requested negotiate claims settlements;</span></p>

                        <p class=MsoNormalCxSpMiddle style='margin-bottom:0in;margin-bottom:.0001pt'><span
                                    style='font-size:9.0pt;font-family:Consolas;color:#262626'>-any other action
that we has specifically agreed in writing to perform.</span></p>

                        <p class=MsoNormalCxSpMiddle style='margin-bottom:0in;margin-bottom:.0001pt'><span
                                    style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>

                        <p class=MsoNormalCxSpMiddle style='margin-bottom:0in;margin-bottom:.0001pt'><span
                                    style='font-size:9.0pt;font-family:Consolas;color:#262626;display:none'>Top of
Form</span></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span lang=IN style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>

                        <p class=MsoNormalCxSpMiddle><b><span style='font-size:9.0pt;font-family:Consolas;
color:#262626'>&nbsp;</span></b></p>

                        <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-autospace:
none'><span lang=IN style='font-size:9.0pt;font-family:Consolas;color:#262626'>&nbsp;</span></p>

                    </div>

                </div>
                <div class="col-md-4">
                    <div class="btn btn-group btn-lg">
                        <a href="{{ route('quote.edit',$data->id) }}" class="btn btn-success btn-lg" style="color:#fff; text-decoration: none;" >Edit</a>
                        <!--<a onclick="print()" class="btn btn-danger btn-lg" style="color:#fff; text-decoration: none;" >Print</a>!-->
                        @if($data->finish == 0)
                            <form id="publish" method="post" action="{{ url('quotation',['quotes_publish',$data->id]) }}">
                                {{ csrf_field() }}
                                <button class="btn btn-primary btn-lg submit-btn" type="button">Publish</button>
                            </form>
                            <input type="hidden" id="id" value="{{ $data->id }}">
                            <input type="hidden" id="q_type" value="{{ $data->quote_type }}">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script>
    $(".submit-btn").click(function(){
        GenerateDocxFile($("#id").val(),$("#q_type").val())
    });
    function GenerateDocxFile(id,t){
        var req;
        if(t === 2){
            req = 'push_cargo.php';
        }else{
            req = 'pushs.php';
        }
        $.ajax({
            url:  '{{ env('DOCUMENT_GENERATOR_URL') }}'+req,
            type: 'POST',
            async: false,
            data: {id:id},
            cache: false,
            success: function(data){
                alert('Generated document with status : success');
                $("#publish").submit();
            },
            error: function(){}
        });
    }
    function print(){
        var content = document.getElementById('print-area').innerHTML;
        var mywindow = window.open('', 'Print', 'height=600,width=800');

        mywindow.document.write('<html><head><title>Print</title>');
        mywindow.document.write('</head><body >');
        mywindow.document.write(content);
        mywindow.document.write('</body></html>');

        mywindow.document.close();
        mywindow.focus()
        mywindow.print();
        mywindow.close();
        return true;
    }
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>
@endsection
@extends('layouts.pib')
@section('content')
        <!-- MAIN -->
<div class="main">
    <link rel="stylesheet" href="{{ asset('assets/vendor/dist/summernote.css') }}">
    <style>
        .note-btn-group.btn-group.note-para .btn-sm{
            padding: 5px 18px;
        }
    </style>
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                @if (Session::get('resume'))
                    <div class="quote_option" style="margin-bottom: 10px;border: 1px #ddd solid;padding: 10px;background: antiquewhite;">
                        <div class="alert alert-success alert-dismissable">
                            Anda punya data quote yang belum selesai.
                        </div>
                        <div class="btn-group-sm"><a href="#" onclick="location.reload()" class="btn btn-info">Lanjutkan</a> <a onclick="event.preventDefault(); document.getElementById('action_quote_new').submit();" href="{{ url('new_quote_delete_old') }}" class="btn btn-primary">Quote Baru*</a> </div>
                        <small class="text-info">*Jika pilih membuat quote baru, data lama akan dihapus.</small>
                        <form method="post" accept-charset="utf-8" id="action_quote_new" action="{{ url('quotation/new_quote_delete_old') }}">
                            {{ csrf_field() }}
                        </form>
                    </div>
                @endif
            <h4 class="page-title">Edit Quote</h4>
            @if($data->status ==  'granted' || $data->status == 'approve')
                <div class="col-md-12" style="padding-bottom: 50px;">
                    <div class="col-md-2 col-md-offset-10">
                        <div class="form-inline"><input type="checkbox" id="approve" {{ $data->status == 'approve' ? 'checked' : '' }} class="checkbox"> Approve </div>
                    </div>
                </div>
            @endif
            <form method="post" action="{{ route('quote.update',$data->id) }}">
                <input type="hidden" name="_method" value="PATCH">
                {{ csrf_field() }}
                <div>
                    @include('pib.quotes.edits.row1')
                    @include('pib.quotes.edits.row2')
                    @include('pib.quotes.edits.row3')
                    <hr><br>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
            <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<!-- /.container-fluid-->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add More Vessels</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Vessel Name</label>
                        <input type="text" class="form-control" id="vessel_name">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Vessel Type</label>
                        <input type="text" class="form-control" id="vessel_type">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Built</label>
                        <input type="text" class="form-control" id="built">
                    </div>
                    <div class="form-group">
                        <label class="control-label">GT</label>
                        <input type="text" class="form-control" id="gt">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Flag</label>
                        <input type="text" class="form-control" id="flag">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Class</label>
                        <input type="text" class="form-control" id="class">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Crew</label>
                        <input type="text" class="form-control" id="crew">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Port of Registry</label>
                        <input type="text" class="form-control" id="port_of_registry">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <button type="button" class="btn btn-default" onclick="saveMoreVessel()">Save</button>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('assets/vendor/dist/summernote.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.summernote-text').summernote({
            height:150,
            width:'100%'
        });
        $('.note-btn-group.btn-group.note-style').remove();
        $('.note-btn-group.btn-group.note-font').remove();
        $('.note-fontname').remove();
        $('.note-table').remove();
        $('.note-insert').remove();
        $('.note-view').remove();
        $('.note-color').remove();
    })
</script>
<script>
    $(document).ready(function(){
        $(".assured").select2({
            width:'100%'
        });
        $("#prorata_from").blur(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: '{!! url('quotation','get_prorata_days') !!}',
                data: {value: $("#prorata_from").val(),id:{!! $data->id !!}},
                success: function (data) {
                    $("#prorata_days").val(data)
                }
            })
        });
        $("#approve").change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var approve = 0;
            if($("#approve").is(':checked')){
                approve = 1;
            }
            $.post('{{ url('quotation','is_approve_quote') }}',{approve:approve,id:{!! $data->id !!}},function(){
                //location.reload();
            })
        });
        $(".valid_until").change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: '{!! url('quotation/get_valid_until') !!}',
                data: {value: $(".date_issued").val(),i:$(".valid_until").val()},
                success: function (data) {
                    $(".until").val(data)
                }
            })
        })
        $("#brokerage_count").click(function(){
            $.post('{{ url('quotation','brokerage_count') }}',{ammount:$("#brokerage_ammount").val(),value:$("#brokerage").val(),id:{!! $data->id !!}},function(data){
                $("#brokerage_ammount").val(data);
            })
        });
        $(".quotation-status").change(function(){
            var val = $(".quotation-status").val();
            if(val == 'New'){
                $("#periode-new").prop('disabled',false);
                $(".periode-renew").prop('disabled',true)
            }else{
                $(".periode-renew").removeAttr('disabled');
                $("#periode-new").prop('disabled',true)
            }
        });
    })
</script>


@endsection

<div class="row panel-tabs" id="row-5">
    <div class="col-md-12">
        <div class="col-md-6">
            @if(in_array('insurer_security_claim',$array))
                <div class="form-group">
                    <label class="control-label">Insurer Security Claim</label>
                    <textarea class="form-control" name="insurer_security_claim" rows="3"></textarea>
                </div>
            @endif
            @if(in_array('payment_and_premium',$array))
                <div class="form-group">
                    <label class="control-label">Payment and Premium</label>
                    <textarea class="form-control" name="payment_and_premium" rows="3"></textarea>
                </div>
            @endif
            @if(in_array('claim_notification',$array))
                <div class="form-group">
                    <label class="control-label">Claim Notification</label>
                    <textarea class="form-control" name="claim_notification" rows="3"></textarea>
                </div>
            @endif
            @if(in_array('confidentiality',$array))
                <div class="form-group">
                    <label class="control-label">Confidentiality</label>
                    <textarea class="form-control" name="confidentiality" rows="3"></textarea>
                </div>
            @endif
            @if(in_array('services',$array))
                <div class="form-group">
                    <label class="control-label">Services</label>
                    <textarea class="form-control" name="services" rows="3"></textarea>
                </div>
            @endif
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Prem Type</label>
                <select name="prem_type" class="form-control">
                    <option value="" disabled selected>--Choose--</option>
                    <option value="ETC">ETC</option>
                    <option value="FIX">FIX</option>
                    <option value="MUT">MUT</option>
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">Payment Term</label>
                <select name="prem_type" class="form-control">
                    <option value="" disabled selected>--Choose--</option>
                    <option value="Half_Yearly">Half Yearly</option>
                    <option value="Quarterly">Quarterly</option>
                    <option value="2">2 Instalments</option>
                    <option value="3">3 Instalments</option>
                    <option value="4">4 Instalments</option>
                    <option value="5">5 Instalments</option>
                </select>
            </div>
        </div>
    </div>
</div>
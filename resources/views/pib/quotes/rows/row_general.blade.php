<style>
    label.control-label.col-md-4 {
        padding-left: 0px !important;
    }
    input.form-control.our-ref {
        font-weight: 700;
    }
</style>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-4">Invoice Number : </label>
            <div class="col-md-8">
                <input type="text" name="our_ref" readonly class="form-control our-ref" value="{{ $newRef }} ">
            </div>
            <br>
        </div>
        <div class="form-group">
            <label class="control-label">Insured</label>
            <select class="form-control assured" name="assured" required="">
                <option value="" selected disabled>--Choose--</option>
                @foreach($company as $c)
                    <option value="{{ $c->id."-".$c->insured }}">{{ $c->insured }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">Address</label>
            <textarea class="form-control" name="address" rows="4"></textarea>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-md-2 control-label periode" style="padding-left: 0px !important;">Date</label>
            <div class="col-md-4">
                <input type="date" class="form-control" id=""  name="date_issued">
            </div>
            <label class="col-md-2 control-label periode">Remark</label>
            <div class="col-md-4">
                <input type="text" name="remark"  class="form-control periode-renew" >
            </div>
            <br><br>
        </div>

        <div class="form-group">
            <label class="control-label">Attention</label>
            <input type="text" name="attention" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">Interest Insured</label>
            <textarea class="form-control" name="address" rows="3"></textarea>
        </div>
    </div>
    <hr>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Cover</label>
            <input type="text" name="cover" class="form-control col-md-4">
        </div>
        <div class="form-group">
            <label class="control-label">Class</label>
            <input type="text" name="class_construction" class="form-control col-md-4">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-md-2 control-label periode" style="padding-left: 0px !important;">Policy Ref</label>
            <div class="col-md-4">
                <input type="text" name="policy_ref" class="form-control">
            </div>
            <label class="col-md-2 control-label periode">Plan</label>
            <div class="col-md-4">
                <input type="text" name="pan" class="form-control">
            </div>
            <br><br>
        </div>
        <div class="form-group">
            <div class="col-md-12" style="padding-left: 0px !important;">
                <label class="col-md-2 control-label periode" style="padding-left: 0px !important;">From</label>
                <div class="col-md-4">
                    <input type="date" class="form-control periode-renew" id="" name="period_from">
                </div>
                <label class="col-md-2 control-label periode">To</label>
                <div class="col-md-4">
                    <input type="date" class="form-control periode-renew" name="period_to" id="">
                </div>
            </div>
            <input type="hidden" class="form-control" name="quote_type" value="8">
        </div>
        <!--
        <div class="form-group">
            <label class="control-label">Jumlah Hari</label>
            <input type="text" name="jumlah_hari" class="form-control">
        </div>
        !-->
    </div>
    <!--
    <div class="form-group">
        </div>
        <div class="form-group">
            <label class="control-label">Occupation</label>
            <input type="text" class="form-control" name="occupation">
        </div>
        <div class="form-group">
            <label class="control-label">Class Constuction</label>
            <input type="text" class="form-control" name="class_construction">
        </div>
        <div class="form-group">
            <label class="control-label">Location Of Risk</label>
            <textarea class="form-control" name="location_of_risk" rows="3"></textarea>
        </div>
    !-->
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Insurer</label>
            <table style="margin-left: 20px;" class="table table-condensed">
                <thead>
                <tr>
                    <th width="50%">Insurer</th>
                    <th width="20%">(%)</th>
                    <th></th>
                </tr>
                </thead>
                <!--elemet sebagai target append-->
                <tbody id="insurer-list">
                <tr>
                    <td>
                        <input name="insurer_name[0]" class="form-control" type="text" required="" placeholder="Insurer Name">
                    </td>
                    <td>
                        <input type="number" min="1" class="form-control" name="part[0]" required="" placeholder="Part in Percent">
                    </td>
                    <td></td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="2"></td>
                    <td>
                        <button class="btn btn-small btn-default" type="button" onclick="addRowInsurer(); return false"><i class="fa fa-plus"></i></button>

                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" onclick="openModal()" class="btn btn-danger">Attach File</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Attach File</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('upload_file_general') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label">File</label>
                        <input type="file" class="form-control" required="" name="file_general">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Upload</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    function openModal(){
        $("#myModal").modal('show')
    }
    $(document).ready(function() {
        $(".assured").select2({
            width: '100%'
        });
        $(".valid_until").change(function(){

            var choosen = new Date($(".date_issued").val())
            console.log(choosen)
            var valid_until = new Date(choosen.getFullYear(), choosen.getMonth(), choosen.getDate() +  parseInt($(".valid_until").val()));
            console.log(valid_until)
            $(".until").val(valid_until.getMonth() + '/' + valid_until.getDate() + '/' + valid_until.getFullYear());
        })
    });
    var i = 1;
    function addRowInsurer() {
        var itemlist = document.getElementById('insurer-list');

        var row = document.createElement('tr');
        var b = document.createElement('td');
        var c = document.createElement('td');
        var d = document.createElement('td');
        var aksi = document.createElement('td');
        itemlist.appendChild(row);
        row.appendChild(b);
        row.appendChild(c);
        //row.appendChild(d);
        row.appendChild(aksi);
        var insured_name = document.createElement('input');
        insured_name.setAttribute('name', 'insurer_name[' + i + ']');
        insured_name.setAttribute('class', 'form-control');
        insured_name.setAttribute('placeholder', 'Insurer Name');
        insured_name.setAttribute('required', '');
//
        var insured_address = document.createElement('input');
        insured_address.setAttribute('name', 'part[' + i + ']');
        insured_address.setAttribute('type', 'number');
        insured_address.setAttribute('min', '1');
        insured_address.setAttribute('placeholder', 'Part in Percent');
        insured_address.setAttribute('class', 'form-control');
        insured_address.setAttribute('required', '');

        //var insured_value_ = document.createElement('input');
        //insured_value_.setAttribute('name', 'as_[' + i + ']');
        //insured_value_.setAttribute('class', 'form-control');
        //insured_value_.setAttribute('placeholder', 'Leader/Member');
        //insured_value_.setAttribute('required', '');
//
//
        var hapus = document.createElement('span');
        b.appendChild(insured_name);
        c.appendChild(insured_address);
        //d.appendChild(insured_value_);
        aksi.appendChild(hapus);
        hapus.innerHTML = '<button type="button" class="btn btn-small btn-default"><i class="fa fa-trash"></i></button>';
//                membuat aksi delete element
        hapus.onclick = function () {
            row.parentNode.removeChild(row);
        };
        $('.iframe_premi')[0].contentWindow.location.reload(true);
        i++;
    }
</script>
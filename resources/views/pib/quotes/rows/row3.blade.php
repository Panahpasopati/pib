<div class="row" id="row-3">
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-4 control-label">Excluded Risks</label>
                <div class="col-md-6">
                    <input type="text" name="excluded_risks" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Crew Contract Restricted</label>
                <div class="col-md-6">
                    <input type="text" name="crew_contract_restricted" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Class Warranty</label>
                <div class="col-md-6">
                    <input type="text" name="class_warranty" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Applicable Clauses</label>
                <div class="col-md-6">
                    <input type="text" name="applicable_clauses" class="form-control">
                </div>
            </div>
            <!--
            <div class="form-group">
                <label class="col-md-4 control-label">Warranties</label>
                <div class="col-md-6">
                    <input type="text" name="warranty" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Sanctions Clause</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="sanctions_clause">
                </div>
            </div>
            !-->
        </div>
        <div class="col-md-6">
            <!--
            <div class="form-group">
                <label class="control-label col-md-4">Crew Contract</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="crew_contracts">
                </div>
            </div>
            !-->
            <div class="form-group">
                <label class="control-label col-md-4">Estimate Time Departure</label>
                <div class="col-md-6">
                    <input type="date" class="form-control col-md-4" name="estimate_time_departure">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Interest Insured</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="interest_insured">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Vessel/Conveyance</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="vessel_conveyance">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Sum Insured</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="sum_insured">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Rate</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="rate_temp" >
                </div>
            </div>
        </div>
    </div>
    <!--
    <div class="col-md-8">
        <div class="form-group">
            <label class="col-md-4 control-label">Warranty</label>
            <div class="col-md-6">
                <table class="table">
                    <tbody id="warranty">
                    <tr>
                        <td><textarea class="form-control" name="warranty_temp" ></textarea> </td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    !-->
    <div class="col-md-4"></div>
</div>
<script>
    var war = 1;
    function addWarranty(){
        $("#warranty").append('<tr id="_warranty-'+war+'"> ' +
                '<td><textarea class="form-control"  name="warranty_temp['+war+']"></textarea> </td> ' +
                '<td><button type="button" class="btn btn-small btn-default" onclick="deleteWarranty('+war+');"><i class="fa fa-trash"></i></button></td> ' +
                '</tr>');
        war++;
    }

    function deleteWarranty(id){
        $("#_warranty-"+id).remove();
    }
</script>
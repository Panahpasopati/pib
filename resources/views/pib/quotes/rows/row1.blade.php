<style>
    .col-md-6 label.control-label.col-md-4 {
        padding-left: 0px !important;
    }
    input.form-control.our-ref {
        font-weight: 700;
    }
</style>
<div class="row" id="row-1">
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-4">Our Ref : </label>
                <div class="col-md-6">
                    <input type="text" name="our_ref" readonly class="form-control our-ref" value="{{ $newRef }} ">
                    <input type="hidden" name="temp_id" id="temp_id" readonly class="form-control" value="{{ $temp->id }} ">
                </div>
                <div class="col-md-1">
                    <a onclick="refreshRef()"><span class="fa fa-refresh"></span> </a>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Type Quotation</label>
                <div class="col-md-6">
                    <select name="quote_type" class="form-control type_quote" required="">
                        <option value="" disabled selected>--Choose an Option--</option>
                        @foreach($type as $t)
                            @if($t->id_type != 8)
                                <option value="{{ $t->id_type }}">{{ $t->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Quotation Status</label>
                <div class="col-md-6">
                    <select name="quotation_status" required="" class="form-control quotation-status">
                        <option value="" disabled selected>--Choose an Option--</option>
                        <option value="New" >New</option>
                        <option value="Renewal">Renewal</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Date Issued</label>
                <div class="col-md-6">
                    <input type="text" name="date_issued" class="form-control date_issued datepicker">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Term Date</label>
                <div class="col-md-6">
                    <select name="term_date" class="form-control valid_until col-md-4" required="">
                        <option value="" selected disabled>--Choose--</option>
                        <option value="7">7 Days</option>
                        <option value="14">14 Days</option>
                        <option value="30">30 Days</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">Due Date</label>
                <div class="col-md-6">
                    <input type="text" class="form-control until  col-md-4" name="valid_until" value="" readonly>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-4">Periode</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" value="12 months from date to be advised">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12" style="padding-left: 0px !important;">
                    <label class="col-md-2 control-label periode" style="padding-left: 0px !important;">From</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control periode-renew datepicker" id="" name="period_from">
                    </div>
                    <label class="col-md-2 control-label periode">To</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control periode-renew datepicker" name="period_to" id="">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <hr><br>
            <div class="form-group">
                <div class="col-md-12 three" style="padding-left: 0px !important;">
                    <label class="control-label col-md-1" style="padding-left: 0px !important;">Assured</label>
                    <div class="col-md-4">
                        <select class="form-control assured" multiple="multiple" name="insurer[]" required="">
                        </select>
                        <a class="btn-link" href="{{ route('insurer.create') }}" target="_blank"><i class="fa fa-plus"></i> Add Company</a>
                    </div>
                    <div class="col-md-1">
                        <a onclick="refreshInsurer()"><span title="Refresh insurer company" class="fa fa-refresh"></span> </a>
                    </div>
                    <label class="control-label col-md-1" style="padding-left: 0px !important;">Mortgage</label>
                    <div class="col-md-2">
                        <select class="form-control banks" name="mortgage_bank">
                            @foreach($banks as $b)
                                <option value="{{ $b->id }}">{{ $b->bank_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Security</label>
                        <div class="col-md-6">
                            <select name="security" required class="form-control coa">
                                @foreach($assured as $ass)
                                    <option value="{{ $ass->id }}">{{ $ass->assured_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Currency</label>
                        <div class="col-md-6">
                            <select name="currency" required class="form-control">
                                <option value="USD" selected>USD</option>
                                <option value="IDR">IDR</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
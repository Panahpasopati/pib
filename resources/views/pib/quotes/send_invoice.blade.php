@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <style>input.form-control {
                        background: #efefef;
                    }</style>
                <h4 class="page-title">Tinjau Invoice</h4>
                <form method="post" class="form-horizontal" action="{{ route('invoice.store') }}" id="form-create-quote">
                    {{ csrf_field() }}
                    <div>
                        <div class="row" id="row-1">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Invoice No</label>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <span class="input-group-addon">Custom :</span>
                                                <input type="text" class="form-control" name="invoice_no_custom" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <span class="input-group-addon">Running :</span>
                                                <input type="text" class="form-control" readonly name="invoice_no_running" value="{{ $newRef }}" required>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Comm. Note No</label>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <span class="input-group-addon">Custom :</span>
                                                <input type="text" class="form-control" name="comm_note_no" value="" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <span class="input-group-addon">Date Issued :</span>
                                                <input type="text" class="form-control" name="date_issued" value="" required>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Detail Quotation</label>
                                        <table style="margin-left: 20px;" class="table table-condensed">
                                            <tbody>
                                            <tr><td>Ref. Quote </td><td>: {{ $data->our_ref }}</td></tr>
                                            <tr><td>Insurer </td><td>: @foreach($data->Insurers as $insurer) &bullet; {{ $insurer->Insurer->insured }} @endforeach</td></tr>
                                            <tr><td>Quoation Status </td><td>: {{ $data->quotation_status }}</td></tr>
                                            <tr><td>Assured/Security </td><td>: {{ $data->Security->assured_name }}</td></tr>
                                            <tr><td>Amount </td><td>: {{ $data->currency }} {{ number_format($data->PremiumAmount->amount_total,2) }}</td></tr>
                                            <tr><td>Is Prorata? </td><td><input type="checkbox" class="checkbox-inline" name="is_prorata" value="1"> </td></tr>
                                            <input type="hidden" value="{{ $data->id }}" name="quote_id">
                                            <input type="hidden" value="{{ $newRef }}" name="newRef">
                                            <input type="hidden" value="{{ $commRef }}" name="commRef">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Period</label>
                                        <table style="margin-left: 20px;" class="table table-condensed">
                                            <tbody>
                                            @if($data->quotation_status == 'New')
                                                <tr><td>Period From </td><td><input type="text" class="form-control datepicker" name="period_from" required> </td></tr>
                                                <tr><td>Period To </td><td><input type="text" class="form-control datepicker" name="period_to" required></td></tr>
                                                @else
                                                <tr><td>Period From </td><td><input type="text" value="{{ $data->period_from }}" class="form-control" name="period_from" required> </td></tr>
                                                <tr><td>Period To </td><td><input type="text" value="{{ $data->period_to }}" class="form-control" name="period_to" required></td></tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Installment</label>
                                        <table style="margin-left: 20px;" class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th width="30%">Installment Number</th>
                                                <th width="60%">Due Date</th>
                                                <th width="10%">Divider %</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <!--elemet sebagai target append-->
                                            <tbody id="itemlist">
                                            @foreach($data->Installment as $i => $val)
                                                <tr>
                                                    <td>
                                                        <input name="installment_number[{{ $i }}]" class="form-control" type="number" min="1" value="{{ $val->instalment_number }}">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control datepicker" name="due_date[{{ $i }}]" required>
                                                    </td>
                                                    <td>
                                                        <input type="text" readonly class="form-control" name="value_[{{ $i }}]" value="{{ $val->value_ }}">
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Policy Ref.</label>
                                    <style>
                                        table.table-scroll>tbody{
                                            display: block;max-height: 300px;overflow-y: scroll;
                                        }
                                        table.table-scroll>thead{display: table;width: 100%;table-layout: fixed;}
                                        table.table-scroll>tfoot>tr{display: table;width: 100%;table-layout: fixed;}
                                        table.table-scroll>tbody>tr{display: table;width: 100%;table-layout: fixed;}
                                        table.table-scroll>thead>tr>th{text-align: center;}
                                        table.table-scroll>tbody>tr>td{text-align: center;}
                                    </style>
                                    <div class="col-md-6">
                                        <table style="margin-left: 20px;" class="table table-scroll table-condensed table-striped">
                                            <thead>
                                            <tr>
                                                <th>Cover</th>
                                                <th>Policy Ref</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($premium as $v => $ves)
                                                <tr>
                                                    <td>
                                                        {{ $ves->Vessel->vessel_name }}
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" placeholder="policy_ref" name="policy_ref[{{ $ves->id }}]" required>
                                                    </td>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr><br>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="panel panel-default" id="filter">
                                    <div class="panel-body text-center">
                                        <ul class="font-md font-bold">
                                            <button class="btn-link" type="submit" ><li class="hvr-blue">Submit Invoice</li></button>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <!-- /.container-fluid-->


@endsection

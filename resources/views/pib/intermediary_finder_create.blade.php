@extends('layouts.pib')
@section('content')
    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="page-title">New Finder Assured</h4>
                <form method="post" action="{{ url('intermediary_finder_store') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Finder</label>
                                    <input type="text" name="finder" class="form-control" value="{{ old('finder') }}" required="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <table style="margin-left: 20px;" class="table table-condensed">
                                <thead>
                                <tr>
                                    <th width="50%">Assured</th>
                                    <th width="25%">Comm</th>
                                    <th width="25%">Remark</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <!--elemet sebagai target append-->
                                <tbody id="itemlist">
                                <tr>
                                    <td>
                                        <input name="assured_name[0]" class="form-control" required="" type="text">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="comm[0]" >
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="remark[0]">
                                    </td>
                                    <td></td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="3"></td>
                                    <td>
                                        <button class="btn btn-small btn-default" type="button" onclick="additem(); return false"><i class="fa fa-plus"></i></button>

                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-md">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <!-- /.container-fluid-->
    <script>
        var i = 1;
        function additem() {
            var itemlist = document.getElementById('itemlist');

            var row = document.createElement('tr');
            var b = document.createElement('td');
            var c = document.createElement('td');
            var d = document.createElement('td');
            var aksi = document.createElement('td');
            itemlist.appendChild(row);
            row.appendChild(b);
            row.appendChild(c);
            row.appendChild(d);
            row.appendChild(aksi);
            var insured_name = document.createElement('input');
            insured_name.setAttribute('name', 'assured_name[' + i + ']');
            insured_name.setAttribute('class', 'form-control');

            var insured_address = document.createElement('input');
            insured_address.setAttribute('name', 'comm[' + i + ']');
            insured_address.setAttribute('class', 'form-control');

            var insured_telp = document.createElement('input');
            insured_telp.setAttribute('name', 'remark[' + i + ']');
            insured_telp.setAttribute('class', 'form-control');


            var hapus = document.createElement('span');
            b.appendChild(insured_name);
            c.appendChild(insured_address);
            d.appendChild(insured_telp);
            aksi.appendChild(hapus);
            hapus.innerHTML = '<button type="button" class="btn btn-small btn-default"><i class="fa fa-trash"></i></button>';
//                membuat aksi delete element
            hapus.onclick = function () {
                row.parentNode.removeChild(row);
            };
            i++;
        }
    </script>



@endsection

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/linearicons/style.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/select2.min.css')}}">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/main.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="https://trubus.id/themes/images/favicon.ico">
    <link rel="icon" type="image/png" sizes="96x96" href="https://trubus.id/themes/images/favicon.ico">
    <title>{{ config('app.name', 'Pacific Indonesia Bersama') }}</title>
    <style>
        li.line-garis {
            border-top: 1px solid #0af;
        }
        #wrapper .main{
            background-color: #ececec;
        }
    </style>
    <style>
        .dlk-radio input[type="radio"],
        .dlk-radio input[type="checkbox"]
        {
            margin-left:-99999px;
            display:none;
        }
        .dlk-radio input[type="radio"] + .fa ,
        .dlk-radio input[type="checkbox"] + .fa {
            opacity:0.15
        }
        .dlk-radio input[type="radio"]:checked + .fa,
        .dlk-radio input[type="checkbox"]:checked + .fa{
            opacity:1
        }

    </style>

    <script src="{{ asset('assets/vendor/jquery/jquery.min.js')}}"></script>
</head>
<body>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="brand">
            <a href="" style="font-weight: 700;">{{ env('APP_NAME') }}</a>
        </div>
        <div class="container-fluid">
            <div class="navbar-btn">
                <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
            </div>
            <form class="navbar-form navbar-left">
                <div class="input-group">
                    <input type="text" value="" class="form-control" placeholder="Search dashboard...">
                    <span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
                </div>
            </form>
            <div id="navbar-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span>{{ @Auth::user()->name }} </span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('change-password') }}"><i class="lnr lnr-cog"></i> <span>Change Password</span></a></li>
                            <li><a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </nav>
    <!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->
    <div id="sidebar-nav" class="sidebar">
        <div class="sidebar-scroll">
            <nav>
                <ul class="nav">
                    <li><a href="{{ url('/')  }}" class="active"><i class="lnr lnr-arrow-left"></i> <span>Back to Main</span></a></li>
                    <li>
                        <a href="#quotes" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> Quotes Reports<i class="icon-submenu lnr lnr-chevron-down"></i></a>
                        <div id="quotes" class="collapse">
                            <ul class="nav">
                                <li><a href="">Quotes Summary</a></li>
                                <li><a href="">Detailed Detail</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#inv" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> Invoicing Reports<i class="icon-submenu lnr lnr-chevron-down"></i></a>
                        <div id="inv" class="collapse">
                            <ul class="nav">
                                <li><a href="">Invoicing Summary</a></li>
                                <li><a href="">Invoicing Detail</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#claim" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> Claim Reports<i class="icon-submenu lnr lnr-chevron-down"></i></a>
                        <div id="claim" class="collapse">
                            <ul class="nav">
                                <li><a href="">Claim Report 1</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#claim" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> Claim Reports<i class="icon-submenu lnr lnr-chevron-down"></i></a>
                        <div id="claim" class="collapse">
                            <ul class="nav">
                                <li><a href="">Claim Report 1</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    @yield('content')
    <!-- END MAIN -->
    <div class="clearfix"></div>
    <footer>
        <div class="container-fluid">
            <p class="copyright">&copy; {{ date('Y') }} <a href="#" target="_blank"></a>. All Rights Reserved.</p>
        </div>
    </footer>
</div>
<!-- END WRAPPER -->

<!-- END WRAPPER -->
<!-- Javascript -->
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{ asset('assets/scripts/klorofil-common.js') }}"></script>
<script src="{{ asset('assets/vendor/select2.min.js') }}"></script>
</body>

</html>
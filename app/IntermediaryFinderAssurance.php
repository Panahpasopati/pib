<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntermediaryFinderAssurance extends Model
{
    protected $table = 'intermediary_finder_assured_table';
    protected $primaryKey = 'id';
    protected $fillable = [
        'finder_id', 'assured_name', 'comm','remark'
    ];

    public function finder()
    {
        return $this->belongsTo('\App\IntermediaryFinders','finder_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentVoucherDetail extends Model
{
    protected $table = 'payment_vouchers_detail';
    protected $fillable = [
        'payment_voucher_id', 'receipt_voucher_id'
    ];

    public function ReceiptVoucher()
    {
        return $this->hasOne('App\ReceiptVoucher','id','receipt_voucher_id');
    }

    public function PaymentVoucher()
    {
        return $this->hasOne('App\PaymentVoucher','payment_voucher_id');
    }
}

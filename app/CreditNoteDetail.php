<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditNoteDetail extends Model
{
    protected $table = 'credit_note_detail';
    protected $fillable = [
        'credit_note_id', 'installment_no','annual_premium', 'policy_cost', 'premium_prorate_due'
    ];

    public function Vessel()
    {
        return $this->belongsTo('App\VesselDetails','vessel');
    }
}

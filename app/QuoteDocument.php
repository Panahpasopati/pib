<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteDocument extends Model
{
    protected $table = 'quotes_table_document';
}

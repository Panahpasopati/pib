<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insurer extends Model
{
    protected $table = 'insurer';
    protected $primaryKey = 'id';
    protected $fillable = [
        'insured', 'address', 'phone', 'email', 'channel', 'channel_id', 'pic','status'
    ];

    public function Broker()
    {
        return $this->belongsTo('App\ChannelGroup','channel_id');
    }

    public function Vessels()
    {
        return $this->hasMany('App\Vessels','insurer_id');
    }
}

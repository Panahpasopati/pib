<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PremiumAmount extends Model
{
    protected $table = 'premium_amount';
    protected $fillable = [
        'quote_id','amount_total','amount_prorata'
    ];
}

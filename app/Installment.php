<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Installment extends Model
{
    protected $table = 'installment_table';
    protected $fillable = [
        'invoice_id', 'installment_no', 'due_date', 'amount', 'is_paid'
    ];
}

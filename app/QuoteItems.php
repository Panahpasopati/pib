<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteItems extends Model
{
    protected $table = 'form_items';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name','default_value','is_dynamic'
    ];
}

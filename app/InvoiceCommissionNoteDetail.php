<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceCommissionNoteDetail extends Model
{
    protected $table = 'invoice_commission_note_detail';
    protected $fillable = [
        'commission_note_id', 'premi_id', 'co_broke', 'vat', 'wht', 'net_amount'
    ];

    public function Premium()
    {
        return $this->belongsTo('App\QuotesPremium','premi_id','id');
    }
}

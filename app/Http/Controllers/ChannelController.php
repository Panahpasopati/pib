<?php

namespace App\Http\Controllers;

use App\ChannelGroup;
use Illuminate\Http\Request;

class ChannelController extends BaseController
{
    public function __construct()
    {
        $this->middleware('admin1');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ChannelGroup::where('status',1)
            ->orderBy('id','desc')
            ->paginate(20);

        return view('pib.channels.index',compact('data'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search()
    {
        $data = ChannelGroup::where('status',1)
            ->where('name','like','%'.$_GET['query'].'%')
            ->orderBy('id','desc')
            ->paginate(20);

        return view('pib.channels.index',compact('data'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pib.channels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ChannelGroup::create($request->all());

        return redirect()
            ->route('channel.index')
            ->with('success','Data inserted.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ChannelGroup::find($id);

        return view('pib.channels.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ChannelGroup::find($id);

        return view('pib.channels.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ChannelGroup::find($id)
            ->update($request->all());

        return redirect()
            ->route('channel.index')
            ->with('success','Data updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ChannelGroup::find($id)
            ->update(['status' => 0]);

        return redirect()
            ->route('channel.index')
            ->with('success','Data deleted.');
    }
}

<?php

namespace App\Http\Controllers;

use App\CreditNote;
use App\CreditNoteDetail;
use App\QuoteItems;
use App\Quotes;
use App\QuotesPremium;
use Illuminate\Http\Request;

class CreditNoteController extends BaseController
{
    public function create($id)
    {
        $data = Quotes::findOrFail($id);
        if(!$data->period_to){
            return redirect('invoices_list')
                ->with('success','Cek kembali invoice atau quote.');
        }
        if(date('Y-m-d') >= date('Y-m-d',strtotime($data->period_to))){
            return redirect('invoices_list')
                ->with('success','Invoice telah kadaluarsa.');
        }
        return view('pib.invoice.credit_note.create',compact('data'));
    }

    public function store(Request $request)
    {
        $store = CreditNote::create([
            'credit_note_no' => $request->credit_note_no,
            'quote_id' => $request->id_,
            'premi_ids' => serialize($request->premium_ids),
            'cn_date' => $request->credit_end,
            'credit_days_count' => $request->credit_days_count,
            'amount_due' => $request->amount_total,
            'policy_cost' => $request->policy_cost
        ]);
        foreach($request->installment as $i => $v){
            CreditNoteDetail::create([
                'credit_note_id' => $store->id,
                'installment_no' => $v,
                'annual_premium' => $request->premium_amount[$i],
                'premium_prorate_due' => $request->premium_prorate_due[$i]
            ]);
        }
        return redirect('invoices_list')
            ->with('success','Credit note berhasil disimpan.');

    }

    public function show($id)
    {
        $data = CreditNote::findOrFail($id);

        return view('pib.invoice.credit_note.show',compact('data'));
    }


    public function NewCNDeleteOld(Request $request,$id)
    {
        $quote = Quotes::findOrFail($id);
        $cn = CreditNote::where('quote_id',$quote->id);
        foreach($cn->get()  as $c){
            CreditNoteDetail::where('credit_note_id',$c->id)->delete();
        }
        $cn->delete();
        return redirect('inv/notes/create_credit_note/'.$quote->id)
            ->with('info','Menghapus Credit Note Sukses.');
    }
    public function changeCN(Request $request)
    {
        $div = '';
        $quote = Quotes::find($request->quote);
        $total = 0;
        foreach($request->id as $y => $id){
            $premium = $this->findPremium($request->quote,$id);
            $total += $premium
                ->annual_premium;
            $div .= '<input type="hidden" name="premium_ids['.$y.']" value="'.$premium->id.'">';
        }
        $amount_due = 0;
        $count = $quote->Installment->count();
        foreach ($quote->Installment as $z => $item) {
            $prorata_rate = $total * ($request->days/366);
            $tr = '<tr>';
            $tr .='<td><p>#'. $item->instalment_number .'</p><p>Off Risk - Return of premium [Prorata '.$quote->currency.' '.number_format($total).' x '.$request->days.'/366 days]</p></td>';
            $tr .= '<td>'. $item->due_date .'</td>';
            $tr .= '<td>'.$quote->currency .' '.number_format($total).'</td>';
            $tr .= '<td>'.$quote->currency.' '.number_format($prorata_rate/$count).'</td>';
            $tr .= '</tr>';
            $div .= $tr;
            $div .= '<input type="hidden" name="installment['.$z.']" value="'.($z+1).'">';
            $div .= '<input type="hidden" name="premium_amount['.$z.']" value="'.$total.'">';
            $div .= '<input type="hidden" name="premium_prorate_due['.$z.']" value="'.($prorata_rate/$count).'">';
            $amount_due += ($prorata_rate/3);
        }
        $div .= '<tr><td align="right" colspan="5">Total : '.$quote->currency.' '.number_format($amount_due).'</td></tr>';
        $div .= '<input type="hidden" name="amount_total" value="'.$amount_due.'">';
        return $div;

    }

    public function findPremium($quote,$vessel)
    {
        return QuotesPremium::where('quotes_id',$quote)
            ->where('id_vessel',$vessel)
            ->first();
    }
}

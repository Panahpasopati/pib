<?php

namespace App\Http\Controllers;

use App\ClientInsurrances;
use App\Clients;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MaxMind\WebService\Client;

class ClientsController extends Controller
{
    public function index()
    {
        //$data = DB::select("select clients_table.client_name as client_name,clients_table.id as client_id,clients_table.alamat as client_address,clients_table.telpon as client_phone, GROUP_CONCAT(client_insurrances_table.insured_name) as insured_name,GROUP_CONCAT(client_insurrances_table.insured_address) as insured_address, GROUP_CONCAT(client_insurrances_table.telp) as insured_phone, GROUP_CONCAT(client_insurrances_table.pic) as insured_pic, GROUP_CONCAT(client_insurrances_table.email) as insured_email from clients_table join client_insurrances_table on client_insurrances_table.id_client = clients_table.id GROUP BY clients_table.id ");

        $data = Clients::orderBy('id','DESC')
            ->paginate(20);
        return view('pib.client.clients',compact('data'));
    }

    public function createClient()
    {
        return view('pib.client.new_client');
    }

    public function storeClient(Request $request)
    {
        Clients::create([
            'client_name' => $request->client_name,
            'alamat' => $request->alamat,
            'telpon' => $request->telpon
        ]);
        $client_id = Clients::orderBy('id')
            ->first();
        foreach($request->insured_name as $i => $value){
            ClientInsurrances::create([
                'id_client' => $client_id->id,
                'insured_name' => $value,
                'insured_address' => $request->insured_address[$i],
                'telp' => $request->telp[$i],
                'pic' => $request->pic[$i],
                'email' => $request->email[$i]
            ]);
        }
        return redirect('clients_list')
            ->with('success','sukses input data client');


    }

    public function editClient($id)
    {
        $data = Clients::find($id);
        $client_insurances = ClientInsurrances::where('id_client',$id)
            ->get();
        return view('pib.client.client_edit',compact('data','client_insurances'));
    }

    public function updateClient(Request $request,$id)
    {
        $client = Clients::find($id);
        $client->update($request->post());
        ClientInsurrances::where('id_client',$id)
            ->delete();

        foreach($request->insured_name as $i => $value){
            if($value){
                ClientInsurrances::create([
                    'id_client' => $id,
                    'insured_name' => $value,
                    'insured_address' => $request->insured_address[$i],
                    'telp' => $request->telp[$i],
                    'pic' => $request->pic[$i],
                    'email' => $request->email[$i]
                ]);
            }

        }
        return redirect('clients_list')
            ->with('success','sukses update data client');
    }
    public function requestClientInsured(Request $request)
    {
        $data = ClientInsurrances::where('id_client',$request->id)
            ->get();

        return response()->json($data);
    }

    public function search()
    {
        $query = $_GET['query'];
        $data = Clients::where('client_name','like','%'. $query .'%')
            ->orWhere('alamat','like','%'. $query .'%')
            ->orderBy('id','DESC')
            ->paginate(7);
        return view('pib.client.clients',compact('data','query'));
    }

}

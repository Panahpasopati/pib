<?php

namespace App\Http\Controllers;

use App\VesselDetails;
use App\Vessels;
use Illuminate\Http\Request;

class VesselController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin1');
    }

    public function index()
    {
        $data = Vessels::orderBy('id','Desc')
            ->where('status',1)
            ->paginate(10);
        return view('pib.vessels.vessels',compact('data'));
    }


    public function search()
    {
        $query = $_GET['query'];
        $data = Vessels::where('insured','like','%'.$query.'%')
            ->orWhereHas('VesselDetail',function($q){
                $q->where('vessel_name','like','%'.strip_tags($_GET['query']).'%')
                    ->where('status',1);
            })
            ->orderBy('id','desc')
            ->paginate(15);
        return view('pib.vessels.vessels',compact('data','query'));
    }

    public static function is_in_vessel($id,$query)
    {
        $find = VesselDetails::where('vessel_name','like','%'.$query.'%')
            ->where('vessel_id',$id)
            ->where('status',1)
            ->get();
        return ($find) ? count($find) : 0;
    }
    public function create()
    {
        return view('pib.vessels.vessel_create');
    }

    public function storeVessel(Request $request)
    {
        Vessels::create($request->post());
        $id = Vessels::orderBy('id','DESC')
            ->first()
            ->id;
        foreach($request->vessel_name as $i => $val){
            VesselDetails::create([
                'vessel_name' => $val,
                'vessel_type' => $request->vessel_type[$i],
                'built' => $request->built[$i],
                'gt' => $request->gt[$i],
                'flag' => $request->flag[$i],
                'class' => $request->class[$i],
                'crew' => $request->crew[$i],
                'port_of_registry' => $request->port_of_registry[$i],
                'trading' => $request->trading[$i],
                'term' => $request->term[$i],
                'vessel_id' => $id
            ]);
        }
        return redirect('vessels_list')
            ->with('success','Add new vessel company success');
    }

    public function edit($id)
    {
        $data = Vessels::find($id);
        $detail = VesselDetails::where('vessel_id',$id)
            ->where('status',1)
            ->get();

        return view('pib.vessels.vessel_edit',compact('data','detail'));
    }

    public function updateVessel(Request $request)
    {

        Vessels::find($request->vessel_id)
            ->update($request->all());
        VesselDetails::where('vessel_id',$request->vessel_id)
            ->update(['status' => 0]);
        foreach ($request->vessel_name as $i => $val) {
            if(isset($request->id_v[$i])){
                VesselDetails::find($request->id_v[$i])
                    ->update([
                    'vessel_name' => $val,
                    'vessel_type' => $request->vessel_type[$i],
                    'built' => isset($request->built[$i]) ? $request->built[$i] : 0,
                    'gt' => isset($request->gt[$i]) ? $request->gt[$i] : 0,
                    'flag' => $request->flag[$i],
                    'class' => $request->class[$i],
                    'crew' => $request->crew[$i],
                    'port_of_registry' => $request->port_of_registry[$i],
                    'trading' => $request->trading[$i],
                    'term' => $request->term[$i],
                    'vessel_id' => $request->vessel_id,
                        'status' => 1
                ]);
            }else{
                VesselDetails::create([
                    'vessel_name' => $val,
                    'vessel_type' => $request->vessel_type[$i],
                    'built' => isset($request->built[$i]) ? $request->built[$i] : 0,
                    'gt' => isset($request->gt[$i]) ? $request->gt[$i] : 0,
                    'flag' => $request->flag[$i],
                    'class' => $request->class[$i],
                    'crew' => $request->crew[$i],
                    'port_of_registry' => $request->port_of_registry[$i],
                    'trading' => $request->trading[$i],
                    'term' => $request->term[$i],
                    'vessel_id' => $request->vessel_id
                ]);
            }

        }

        return redirect('vessels_list')
            ->with('success','data updated.');


    }

    public function delete($id)
    {
        Vessels::find($id)
            ->update([
                'status' => 0
            ]);
        VesselDetails::where('vessel_id',$id)
            ->update(['status' => 0]);
        return redirect('vessels_list')
            ->with('success','Data deleted.');
    }
}

<?php

namespace App\Http\Controllers;

use App\Assured;
use Illuminate\Http\Request;

class AssuredController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin1');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Assured::where('status',1)->orderBy('id','desc')->paginate(10);
        return view('pib.assured',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pib.assured_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Assured::create($request->all());
        return redirect()
            ->route('assured.index')
            ->with('success','Sukses menambahkan data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Assured::find($id);
        return view('pib.assured_edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $assured = Assured::find($id);
        $assured->update($request->all());
        return redirect()
            ->route('assured.index')
            ->with('success','Sukses edit data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Assured::destroy($id);
        return redirect()
            ->route('assured.index')
            ->with('success','Sukses hapus data');
    }

    public function search()
    {

    }
}

<?php

namespace App\Http\Controllers;

use App\Invoices;
use App\Quotes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountingController extends Controller
{

    public function request_data_from_invoice_no(Request $request)
    {
        $data = Invoices::find($request->inv);

        $gross = 0;
        if($data->Quotes->quote_type != 2 AND $data->Quotes->quote_type != 8){
            $gross = $data->Quotes->Premium->ammount_total;
        }else{
            $gross = $data->Quotes->Premium->sum_insured;
        }

        return response()
            ->json([
                'from' => explode("-",$data->Quotes->assured)[1],
                'gross' => $gross,
                'installment' => ($data->Quotes->installment) ? count($data->Quotes->installment) : 0
            ]);
    }

    public function get_invoice_no(Request $request)
    {
        $data = DB::select("select invoice_table.id,invoice_table.invoice_no  from quotes_table join invoice_table on invoice_table.quotes_id = quotes_table.id where quotes_table.status = 'granted' AND quotes_table.security = ".$request->security);

        return response()
            ->json($data);
    }
}

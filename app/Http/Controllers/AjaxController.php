<?php

namespace App\Http\Controllers;

use App\ClientInsurrances;
use App\Insurer;
use App\Quotes;
use App\QuotesPremium;
use App\QuotesVessels;
use App\QuoteTypes;
use App\Vessels;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVesselItems(Request $request)
    {
        $data = Vessels::where('insurer_id',$request->id)
            ->get();
        $address = Vessels::find($request->id);
        return response()
            ->json([
                'data' => $data,
                'addres' => $address
            ]);
    }

    public function storeVessel(Request $request)
    {
        $data = $request->data;
        parse_str($data,$array);
        Vessels::create($array);
    }
    public function getVesselsDetail(Request $request)
    {
        if(is_array($request->items)){
            foreach($request->items as $val){
                $find = Vessels::find($val);
                QuotesVessels::firstOrCreate([
                    'id_vessel' => $val,
                    'quotes_id' => $request->temp_id
                ]);
                QuotesPremium::firstOrCreate([
                    'id_vessel' => $val,
                    'quotes_id' => $request->temp_id,
                    'type_' => $find->vessel_type
                ]);
            }

        }else{
            QuotesPremium::where('quotes_id',$request->temp_id)
                ->delete();
            QuotesVessels::where('quotes_id',$request->temp_id)
                ->delete();
        }

    }

    public function showPremiumTable(Request $request)
    {
        $data = Vessels::whereIn('id',$request->items)
            ->get();
        echo '<table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Vessel Name</th>
                            <th>Type</th>
                            <th>Premium Type</th>
                            <th>Annual Premium</th>
                        </tr>
                        </thead>
                        <tbody>';

        foreach($data as $i => $d){
            echo '<tr>
                            <td>'.$d->vessel_name.'</td>
                            <td><input type="text" name="type['.$i.']" class="form-control"></td>
                            <td><input type="text" name="premium_type['.$i.']" class="form-control"></td>
                            <td><input type="text" name="annual_premium['.$i.']" class="form-control"></td>
                        </tr>';

        }
        echo ' </tbody>
                    </table>';
    }

    public function getVesselPastData(Request $request)
    {
        $row = Vessels::find($request->id);
        $past = $request->past;
        echo $row->$past;
    }

    public function reqInvData(Request $request)
    {
        $data = Quotes::find($request->id);

        echo '<table class="table table-bordered"><tr><td>Quote ref. </td><td>'.$data->our_ref.'</td></tr><tr><td>Assured. </td><td>'.explode("-",$data->assured)[1].'</td></tr></table>';
        echo '<a href="'.url('endorsements_list').'" class="btn btn-default">Cancel</a> <a href="'.url('vessel_endorsement_create',$data->id).'" class="btn btn-primary">Next</a>';
    }

    public function getCondition(Request $request)
    {
        $data = QuoteTypes::find($request->id);

        echo $data->conditions;
    }

    public function getValidUntil(Request $request)
    {
        echo date('Y-m-d', strtotime('+'.$request->i.' days', strtotime($request->value)));
    }

    public function saveDraft(Request $request)
    {
        Quotes::find($request->temp_id)
            ->update([
                'assured' => $request->assured,
                'quote_type' => $request->tipe,
                'status' => $request->status,
                'date_issued' => $request->date_issued,
                'valid_until' => $request->valid_until
            ]);
    }

    public function refreshNoRef(Request $request)
    {

    }

    public function check_insured_name(Request $request)
    {
        $check = Insurer::where('insured','like','%'.$request->input.'%')
            ->where('status',1)
            ->orderBy('id','desc')
            ->limit(3)
            ->get();

        if(($check) && count($check) > 0){
            $array = [];
            foreach($check as $c){
                $array[] = [
                    'name' => $c->insured,
                    'link' => route('insurer.edit',$c->id)
                ];
            }
            return response()->json([
                'response' => 1, 'data' => 'Ditemukan nama perusahaan yang sama.','rows' => $array
            ]);
        }else{
            return [
                'response' => 0, 'data' => ''
            ];
        }
    }

    public function getProrataDays(Request $request)
    {
        $quote = Quotes::find($request->id);
        $start = explode('to',$quote->period)[1];
        //echo date('Y-m-d',strtotime('14 Mar 2020'));
        if($quote->quotation_status == 'Renewal'){
            $start = $quote->period_from;
        }
        $date1=date_create(date('Y-m-d',strtotime($request->value)));
        $date2=date_create(date('Y-m-d',strtotime($start)));
        $diff=date_diff($date1,$date2);
        $response = intval($diff->format("%R%a"));
        return $response;
    }

    public function store_new_company_ajax(Request $request)
    {
        $str_data = $request->data;
        parse_str($str_data,$data);
        $store = Vessels::create([
            'insured' => $data['insured'],
            'address' => $data['address'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'channel' => $data['channel'],
            'channel_id' => $data['channel_id'],
            'pic' => $data['pic']
        ]);

        return response()
            ->json([
                'status' => 'sukses',
                'data' => $store
            ]);
    }
}

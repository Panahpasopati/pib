<?php

namespace App\Http\Controllers;

use App\Coas;
use Illuminate\Http\Request;

class CoaController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin1');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Coas::where('is_deleted',0)
            ->orderBy('id','DESC')
            ->paginate(20);
        return view('pib.coa_index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pib.coa_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Coas::create($request->all());

        return redirect()
            ->route('coa.index')
            ->with('success','Success insert data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Coas::find($id);

        return view('pib.coa_edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Coas::find($id)
            ->update($request->all());

        return redirect()
            ->route('coa.index')
            ->with('success','Success edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Coas::find($id)
            ->update(['is_deleted' => 1]);

        return redirect()
            ->route('coa.index')
            ->with('success','Success deleted');
    }
}

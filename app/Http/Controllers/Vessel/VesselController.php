<?php

namespace App\Http\Controllers\Vessel;

use App\Http\Controllers\BaseController;
use App\Vessels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VesselController extends BaseController
{
    public function vesselDetail(Request $request)
    {
        $data = Vessels::where('insurer_id',$request->id)
            ->where('status',1)
            ->get();

        $row = Vessels::find($request->id);
        $channel = [
            'channel' => $row->channel,
            'name' => @$row->Broker->name,
            'pic' => $row->pic,
        ];
        return response()->json(['data' => $data,'channel' => $channel]);
    }

}

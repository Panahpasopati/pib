<?php

namespace App\Http\Controllers;

use App\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CurrencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin1');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $y = date('Y');
        $data = DB::select("select * from currency where YEAR(date_) = '$y' order by date_ asc");

        return view('pib.currency.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $start    = new \DateTime(date('Y-').'01-01');
        $end      = new \DateTime(date('Y-').'12-31');
        $interval = \DateInterval::createFromDateString('1 day');
        $lists   = new \DatePeriod($start, $interval, $end);
        return view('pib.currency.create',compact('lists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->date as $i => $item) {
            Currency::updateOrCreate(
                ['date_' => $item ],
                [
                'rate' => $request->rate[$i]
            ]);
        }
        return redirect()
            ->route('currency.index')
            ->with('success','Success insert new data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Currency::find($id);

        return view('pib.currency.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Currency::find($id)
            ->update($request->all());
        return redirect()
            ->route('currency.index')
            ->with('success','Success edit data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

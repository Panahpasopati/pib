<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IncomesController extends Controller
{
    public function index()
    {
        $data = DB::table('quotes_table')
            ->select(DB::raw('quotes_table.our_ref,quotes_table.assured,quotes_table.ammount_total,quotes_table_assurance_company.*,our_incomes.*'))
            ->join('quotes_table_assurance_company','quotes_table_assurance_company.quote_id','=','quotes_table.id')
            ->join('our_incomes','our_incomes.quote_id','=','quotes_table.id')
            ->orderBy('quotes_table.id','DESC')
            ->paginate(10);

        return view('pib.invoice.commision_index',compact('data'));
    }
}

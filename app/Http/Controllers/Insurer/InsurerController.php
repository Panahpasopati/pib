<?php

namespace App\Http\Controllers\Insurer;

use App\Http\Controllers\BaseController;
use App\Insurer;
use App\Vessels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InsurerController extends BaseController
{
    public function __construct()
    {
        $this->middleware('admin1');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Insurer::orderBy('id','Desc')
            ->where('status',1)
            ->paginate(10);
        return view('pib.insurer.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pib.insurer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Insurer::create($request->post());
        $id = Insurer::orderBy('id','DESC')
            ->first()
            ->id;
        foreach($request->vessel_name as $i => $val){
            Vessels::create([
                'vessel_name' => $val,
                'vessel_type' => $request->vessel_type[$i],
                'built' => $request->built[$i],
                'gt' => $request->gt[$i],
                'flag' => $request->flag[$i],
                'class' => $request->class[$i],
                'crew' => $request->crew[$i],
                'port_of_registry' => $request->port_of_registry[$i],
                'trading' => $request->trading[$i],
                'term' => $request->term[$i],
                'insurer_id' => $id
            ]);
        }
        return redirect()->route('insurer.index')
            ->with('success','Add new insurer company success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = insurer::find($id);
        $detail = Vessels::where('insurer_id',$id)
            ->where('status',1)
            ->get();

        return view('pib.insurer.edit',compact('data','detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Insurer::find($request->vessel_id)
            ->update($request->all());
        Vessels::where('insurer_id',$request->vessel_id)
            ->update(['status' => 0]);
        foreach ($request->vessel_name as $i => $val) {
            if(isset($request->id_v[$i])){
                Vessels::find($request->id_v[$i])
                    ->update([
                        'vessel_name' => $val,
                        'vessel_type' => $request->vessel_type[$i],
                        'built' => isset($request->built[$i]) ? $request->built[$i] : 0,
                        'gt' => isset($request->gt[$i]) ? $request->gt[$i] : 0,
                        'flag' => $request->flag[$i],
                        'class' => $request->class[$i],
                        'crew' => $request->crew[$i],
                        'port_of_registry' => $request->port_of_registry[$i],
                        'trading' => $request->trading[$i],
                        'term' => $request->term[$i],
                        'insurer_id' => $request->vessel_id,
                        'status' => 1
                    ]);
            }else{
                Vessels::create([
                    'vessel_name' => $val,
                    'vessel_type' => $request->vessel_type[$i],
                    'built' => isset($request->built[$i]) ? $request->built[$i] : 0,
                    'gt' => isset($request->gt[$i]) ? $request->gt[$i] : 0,
                    'flag' => $request->flag[$i],
                    'class' => $request->class[$i],
                    'crew' => $request->crew[$i],
                    'port_of_registry' => $request->port_of_registry[$i],
                    'trading' => $request->trading[$i],
                    'term' => $request->term[$i],
                    'insurer_id' => $request->vessel_id
                ]);
            }

        }

        return redirect()->route('insurer.index')
            ->with('success','data updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Insurer::find($id)
            ->update([
                'status' => 0
            ]);
        Vessels::where('insurer_id',$id)
            ->update(['status' => 0]);
        return redirect()->route('insurer.index')
            ->with('success','Data deleted.');
    }
}

<?php

namespace App\Http\Controllers;

use App\Assured;
use App\Banks;
use App\Invoices;
use App\OurIncomes;
use App\Quotes;
use App\QuotesAdditional;
use App\QuotesAssuranceCompany;
use App\QuotesGeneralDetail;
use App\QuotesGeneralPremium;
use App\QuotesPremium;
use App\QuotesPremiumWarranty;
use App\QuotesVessels;
use App\QuoteTypes;
use App\Vessels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuotesController extends BaseController
{


    public function index()
    {
        $data = Quotes::where('finish',1)
            ->where('is_deleted',0)
            ->orderBy('id','Desc')
            ->paginate(15);
        $draft = Quotes::where('finish',0)
            ->where('is_deleted',0)
            ->count();
        return view('pib.quotes.quotes_index',compact('data','draft'));
    }

    public function draftList()
    {
        $data = Quotes::where('finish',0)
            ->where('is_deleted',0)
            ->orderBy('id','Desc')
            ->paginate(15);
        return view('pib.quotes.draft',compact('data'));
    }

    public function createQuotes()
    {
        $array = ['quotation_status', 'quote_type', 'our_ref', 'date_issued', 'valid_until', 'assured', 'address', 'estimate_time_departure', 'interest_insured', 'voyage', 'vessel_conveyance', 'sum_insured', 'join_assured', 'interest_parties', 'mortgage', 'period', 'trading_limit', 'condition_', 'condition_optimus', 'cover_restricted_to', 'luminor_7', 'excluded_risks', 'limit_of_cover', 'deductibles', 'class_waranty', 'warranties', 'application_form_warranty', 'rate', 'general_conditions', 'application_clauses', 'mlc_2006_exclusion', 'sanctions_clause', 'warranty', 'spob_warranty_clause', 'tank_barge_warranty', 'barge_warranty_clause', 'deck_cargo_warranty', 'crew_contracts', 'survey_warranty', 'premium', 'premium_warranty', 'security', 'information_given', 'information_required_prior_to_cover', 'comment', 'disclaimer', 'duty_of_disclosure', 'your_duty_of_disclosure', 'utmost_good_faith', 'client_responsibilites', 'essentials_reading_policy', 'warranties_subjectivities', 'insurer_security_claim', 'payment_and_premium', 'claim_notification', 'confidentiality', 'services'];
        $type = QuoteTypes::all();
        $last = Quotes::orderBy('id','DESC')
            ->first();
        if(!$last){
            $lastRef = 'QTE/240818/PNI/00000000';
        }else{
            $lastRef = $last->our_ref;
        }

        $assured = Assured::all();
        $splitter = explode("/",$lastRef)[3];
        $before = '';
        for($i = 0; $i<7 - count(str_split(intval($splitter))); $i++){
            $before .= '0';
        }
        $banks = Banks::all();
        $newRef = 'QTE/'.date('dmy').'/PNI/'.$before.(intval($splitter) + 1);
        $check = Quotes::where('session_id',session()->getId())
            ->where('finish',0)
            ->where('is_deleted',0)
            ->orderBy('id','DESC')
            ->first();
        if($check){
            return redirect('quotes_edit/'.$check->id)
                ->with('resume',true);
        }
        //Quotes::firstOrCreate(
        //    [
        //        'session_id' => session()->getId(),
        //        'finish' => 0,
        //        'is_draft' => 0
        //    ],
        //    [
        //        'our_ref' => $newRef,
        //        'created_by' => Auth::id(),
        //
        //    ]);
        Quotes::create([
            'our_ref' => $newRef,
            'created_by' => Auth::id(),
            'session_id' => session()->getId(),
            'finish' => 0,
            'is_draft' => 0
        ]);
        $temp = Quotes::where('our_ref',$newRef)
            ->first();
        $company = Vessels::all();
        return view('pib.quotes.quote_create',compact('type','array','company','newRef','banks','temp','assured'));
    }
    public function mini_quote()
    {
        $array = ['quotation_status', 'quote_type', 'our_ref', 'date_issued', 'valid_until', 'assured', 'address', 'estimate_time_departure', 'interest_insured', 'voyage', 'vessel_conveyance', 'sum_insured', 'join_assured', 'interest_parties', 'mortgage', 'period', 'trading_limit', 'condition_', 'condition_optimus', 'cover_restricted_to', 'luminor_7', 'excluded_risks', 'limit_of_cover', 'deductibles', 'class_waranty', 'warranties', 'application_form_warranty', 'rate', 'general_conditions', 'application_clauses', 'mlc_2006_exclusion', 'sanctions_clause', 'warranty', 'spob_warranty_clause', 'tank_barge_warranty', 'barge_warranty_clause', 'deck_cargo_warranty', 'crew_contracts', 'survey_warranty', 'premium', 'premium_warranty', 'security', 'information_given', 'information_required_prior_to_cover', 'comment', 'disclaimer', 'duty_of_disclosure', 'your_duty_of_disclosure', 'utmost_good_faith', 'client_responsibilites', 'essentials_reading_policy', 'warranties_subjectivities', 'insurer_security_claim', 'payment_and_premium', 'claim_notification', 'confidentiality', 'services'];
        $type = QuoteTypes::all();
        $last = Quotes::orderBy('id','DESC')
            ->first();
        if(!$last){
            $lastRef = 'QTE/240818/PNI/00000000';
        }else{
            $lastRef = $last->our_ref;
        }

        $assured = Assured::all();
        $splitter = explode("/",$lastRef)[3];
        $before = '';
        for($i = 0; $i<7 - count(str_split(intval($splitter))); $i++){
            $before .= '0';
        }
        $banks = Banks::all();
        $newRef = 'QTE/'.date('dmy').'/PNI/'.$before.(intval($splitter) + 1);
        $check = Quotes::where('session_id',session()->getId())
            ->where('finish',0)
            ->orderBy('id','DESC')
            ->first();
        if($check){
            $newRef = $check->our_ref;
        }
        Quotes::firstOrCreate(
            [
                'session_id' => session()->getId(),
                'finish' => 0,
                'is_draft' => 0
            ],
            [
                'our_ref' => $newRef,
                'created_by' => Auth::id()
            ]);
        $temp = Quotes::where('our_ref',$newRef)
            ->first();
        $company = Vessels::all();
        return view('pib.quotes.mini_quote',compact('type','array','company','newRef','banks','temp','assured'));
    }

    public function publish($id)
    {
        Quotes::find($id)
            ->update([
                'finish' => 1
            ]);

        return redirect('quotes_list')
            ->with('success','publish new quote : success');
    }
    public function createGeneralQuotes()
    {
        $lastRef = Quotes::orderBy('id','DESC')
            ->first()
            ->our_ref;
        $splitter = explode("/",$lastRef)[3];
        $before = '';
        for($i = 0; $i<7 - count(str_split(intval($splitter))); $i++){
            $before .= '0';
        }
        $newRef = 'quo/'.date('dmy').'/####/'.$before.(intval($splitter) + 1);
        Quotes::create([
            'our_ref' => $newRef
        ]);
        $temp = Quotes::orderBy('our_ref',$newRef)
            ->first();
        $company = Vessels::all();
        return view('pib.quotes.quotes_general_create',compact('company','newRef','temp'));
    }

    public function storeGeneralQuotes(Request $request)
    {
        Quotes::create($request->post());
        $quote_id = Quotes::orderBy('id','DESC')
            ->first()
            ->id;

        foreach($request->insurer_name as $i => $value){
            QuotesGeneralDetail::create([
                'id_quotes' => $quote_id,
                'insurer_name' => $value,
                'part' => $request->part[$i],
                'as_' => $request->as_[$i],
                'total_sum_insured' => 0,
                'total_regulation_rate' => 0,
                'total_premi' => 0
            ]);
        }
        return redirect('create_general_premium_quotes/'.$quote_id);
    }

    public function createGeneralPremium($id)
    {
        $data = QuotesGeneralDetail::where('id_quotes',$id)
            ->get();
        return view('pib.quotes.quotes_general_premium',compact('data'));
    }

    public function updateGeneralPremium(Request $request)
    {
        foreach($request->interest_insured as $i => $inter){
            QuotesGeneralPremium::updateOrCreate(
                [
                    'id_general_detail' => $i,
                ],
                [
                    'interest_insured' => $inter,
                    'sum_insured' => $request->sum_insured[$i],
                    'regulation_rate_name' => $request->regulation_rate_name[$i],
                    'regulation_rate_value' => $request->regulation_rate_value[$i],
                    'premi' => $request->premi[$i]
                ]);
            QuotesGeneralDetail::find($i)
                ->update([
                    'total_sum_insured' => $request->total_sum_insured,
                    'total_regulation_rate' => $request->total_regulation_rate,
                    'total_premi' => $request->total_premi
                ]);
        }

        return redirect('quotes_list')
            ->with('success','Success store new quotes');
    }

    public function editGeneralPremium($id)
    {
        $data = QuotesGeneralDetail::where('id_quotes',$id)
            ->get();
        return view('pib.quotes.update_general_premium',compact('data'));
    }

    public function showGeneralQuotes($id)
    {
        $data = QuotesGeneralDetail::where('id_quotes',$id)
            ->get();
        $quote = Quotes::find($id);
        return view('pib.quotes.show_general_quotes',compact('data','quote'));
    }

    public function editQuotes($id)
    {
        $array = ['quotation_status', 'quote_type', 'our_ref', 'date_issued', 'valid_until', 'assured', 'address', 'estimate_time_departure', 'interest_insured', 'voyage', 'vessel_conveyance', 'sum_insured', 'join_assured', 'interest_parties', 'mortgage', 'period', 'trading_limit', 'condition_', 'condition_optimus', 'cover_restricted_to', 'luminor_7', 'excluded_risks', 'limit_of_cover', 'deductibles', 'class_waranty', 'warranties', 'application_form_warranty', 'rate', 'general_conditions', 'application_clauses', 'mlc_2006_exclusion', 'sanctions_clause', 'warranty', 'spob_warranty_clause', 'tank_barge_warranty', 'barge_warranty_clause', 'deck_cargo_warranty', 'crew_contracts', 'survey_warranty', 'premium', 'premium_warranty', 'security', 'information_given', 'information_required_prior_to_cover', 'comment', 'disclaimer', 'duty_of_disclosure', 'your_duty_of_disclosure', 'utmost_good_faith', 'client_responsibilites', 'essentials_reading_policy', 'warranties_subjectivities', 'insurer_security_claim', 'payment_and_premium', 'claim_notification', 'confidentiality', 'services'];
        $type = QuoteTypes::all();
        $data = Quotes::find($id);
        $assured = Assured::all();
        $company = Vessels::all();
        $selected_vessel = QuotesVessels::select('id_vessel')
            ->where('quotes_id',$id)
            ->get();
        $banks = Banks::all();
        $premium_warranty = DB::select("SELECT * FROM `quotes_table_premium_warranty` where quotes_id = ".$id);
        return view('pib.quotes.quotes_edit',compact('data','id','type','array','company','selected_vessel','premium_warranty','banks','assured'));

    }

    public function showQuote($id)
    {
        $data = Quotes::find($id);
        $premium_warranty = DB::select("SELECT * FROM `quotes_table_premium_warranty` where quotes_id = ".$id);
        if($data->quote_type == 2){

            return view('pib.quotes.quote_cargo_show', compact('data','premium_warranty'));
        }else{
            $mortgage = DB::select("SELECT * FROM `quotes_table_vessels` join vessel_company_details on vessel_company_details.id = quotes_table_vessels.id_vessel WHERE quotes_table_vessels.quotes_id = ".$id);
            $premium_warranty = DB::select("SELECT * FROM `quotes_table_premium_warranty` where quotes_id = ".$id);
            $vessels = DB::select("SELECT *,quotes_table_premium.id as premi_id FROM `quotes_table_premium` join vessel_company_details on vessel_company_details.id = quotes_table_premium.id_vessel WHERE quotes_table_premium.quotes_id = ".$id);
            if($data->quote_type == 9){
                return view('pib.quotes.quote_hm_show',compact('data','mortgage','premium_warranty','vessels'));
            }else{
                return view('pib.quotes.quote_show',compact('data','mortgage','premium_warranty','vessels'));
            }
        }
    }

    public function storeQuotes(Request $request)
    {
        $select = Quotes::find($request->temp_id);

        $select->update($request->post());
        //$select->update([
        //    'condition_' => join("|||",array_values($request->condition_temp)),
        //    'comment' => join('|||',array_values($request->comment_temp)),
        //    'deductibles' => join("|||",array_values($request->deductibles_temp)),
        //    'information_given' => join('|||',array_values($request->information_given_temp)),
        //    'survey_warranty' => join('|||',array_values($request->survey_warranty_temp)),
        //    'rate' => ($request->rate_temp) ? $request->rate_temp : $select->rate,
        //    'warranty' => join('|||',array_values($request->warranty_temp)),
        //    'created_by' => Auth::id(),
        //    'status' => $select->status == 'approve' ? 'approve' : 'granted'
        //]);

        $select->update([
            'condition_' => $request->condition_temp,
            'comment' => $request->comment_temp,
            'deductibles' => $request->deductibles_temp,
            'information_given' => $request->information_given_temp,
            'survey_warranty' => $request->survey_warranty_temp,
            'rate' => ($request->rate_temp) ? $request->rate_temp : $select->rate,
            'warranty' => $request->warranty_temp,
            'war_risk_etc' => $request->war_risk_etc_temp,
            'subjectivities' => $request->subjectivities_temp,
            'information_required' => join("|||",array_values($request->information_required_temp)),
            'created_by' => Auth::id(),
            'status' => $select->status == 'approve' ? 'approve' : 'granted'
        ]);

        $quote_id = Quotes::orderBy('id','DESC')
            ->first()
            ->id;

        //foreach($request->mortgage as $val){
        //    QuotesVessels::create([
        //        'id_vessel' => $val,
        //        'quotes_id' => $quote_id
        //    ]);
        //}
//
        //foreach($request->mortgage as $val){
        //    QuotesPremium::create([
        //        'id_vessel' => $val,
        //        'quotes_id' => $quote_id
        //    ]);
        //}

        foreach($request->instalment_number as $i => $value){
            QuotesPremiumWarranty::create([
                'instalment_number' => $value,
                'due_date' => $request->due_date[$i],
                'quotes_id' => $request->temp_id,
                'value_' => $request->value_[$i]
            ]);
        }

        if($request->quote_type == 2){
            return redirect('show_quote/'.$request->temp_id);
        }else{
            return redirect('show_quote/'.$request->temp_id);
        }
    }

    public function saveTemporaryQuote(Request $request)
    {
        parse_str($request->data,$data);
        $select = Quotes::find($request->id);

        $select->update($data);

        /**
        $select->update([
            'condition_' => join("|||",array_values($data['condition_temp'])),
            'comment' => join('|||',array_values($data['comment_temp'])),
            'deductibles' => join("|||",array_values($data['deductibles_temp'])),
            'information_given' => join('|||',array_values($data['information_given_temp'])),
            'survey_warranty' => join('|||',array_values($data['survey_warranty_temp'])),
            'rate' => ($data['rate_temp']) ? $data['rate_temp'] : $select->rate,
            'warranty' => join('|||',array_values($data['warranty_temp'])),
            'created_by' => Auth::id()
        ]);
         * */
        $select->update([
            'condition_' => $request->condition_temp,
            'comment' => $request->comment_temp,
            'deductibles' => $request->deductibles_temp,
            'information_given' => $request->information_given_temp,
            'survey_warranty' => $request->survey_warranty_temp,
            'rate' => ($request->rate_temp) ? $request->rate_temp : $select->rate,
            'warranty' => $request->warranty_temp,
            'war_risk_etc' => $request->war_risk_etc_temp,
            'subjectivities' => $request->subjectivities_temp,
            'information_required' => join("|||",array_values($request->information_required_temp)),
            'created_by' => Auth::id(),
            'status' => $select->status == 'approve' ? 'approve' : 'granted'
        ]);

        foreach($data['instalment_number'] as $i => $value){
            QuotesPremiumWarranty::create([
                'instalment_number' => $value,
                'due_date' => $data['due_date'][$i],
                'quotes_id' => $request->id,
                'value_' => $data['value_'][$i]
            ]);
        }
        return 'Data saved';
    }
    public function updateQuotes(Request $request)
    {
        $select = Quotes::find($request->temp_id);
        $select->update($request->post());
        /**
        $select->update([
            'condition_' => join("|||",array_values($request->condition_temp)),
            'comment' => join('|||',array_values($request->comment_temp)),
            'deductibles' => join("|||",array_values($request->deductibles_temp)),
            'information_given' => join('|||',array_values($request->information_given_temp)),
            'survey_warranty' => join('|||',array_values($request->survey_warranty_temp)),
            'warranty' => join('|||',array_values($request->warranty_temp)),
            'finish' => 0
        ]);
         **/
        $select->update([
            'condition_' => $request->condition_temp,
            'comment' => $request->comment_temp,
            'deductibles' => $request->deductibles_temp,
            'information_given' => $request->information_given_temp,
            'survey_warranty' => $request->survey_warranty_temp,
            'rate' => ($request->rate_temp) ? $request->rate_temp : $select->rate,
            'warranty' => $request->warranty_temp,
            'war_risk_etc' => $request->war_risk_etc_temp,
            'subjectivities' => $request->subjectivities_temp,
            'information_required' => join("|||",array_values($request->information_required_temp)),
            'created_by' => Auth::id(),
            'status' => $select->status == 'approve' ? 'approve' : 'granted',
            'finish' => 0
        ]);
        //QuotesVessels::where('quotes_id',$request->quote_id)
        //    ->delete();
        //foreach($request->mortgage as $val){
        //    QuotesVessels::create([
        //        'id_vessel' => $val,
        //        'quotes_id' => $request->quote_id
        //    ]);
        //}
//
        //QuotesPremium::where('quotes_id',$request->quote_id)
        //    ->delete();
        //foreach($request->mortgage as $val){
        //    QuotesPremium::create([
        //        'id_vessel' => $val,
        //        'quotes_id' => $request->quote_id
        //    ]);
        //}

        //QuotesPremiumWarranty::where('quotes_id',$request->quote_id)
        //  ->delete();
        QuotesPremiumWarranty::where('quotes_id',$request->temp_id)
            ->delete();
        foreach($request->instalment_number as $i => $value){
            QuotesPremiumWarranty::create([
                'instalment_number' => $value,
                'due_date' => $request->due_date[$i],
                'quotes_id' => $request->temp_id,
                'value_' => $request->value_[$i]
            ]);
        }

        if($request->quote_type == 2){
            return redirect('show_quote/'.$request->temp_id);
        }else{
            return redirect('show_quote/'.$request->temp_id);
        }

    }
    public function sendInvoice($id)
    {
        $quotes = Quotes::find($id);
        if($quotes->status == 'approve'){
            if($quotes->quotation_status == 'New'){
                $number = intval($quotes->period);
                $next = date('d M Y',strtotime('+'.$number.' month'));
                $quotes->update([
                    'status' => 'approve',
                    'period' => date('d M Y',strtotime($quotes->date_issued)). ' to '.$next,
                    'period_from' =>  date('d M Y',strtotime($quotes->date_issued)),
                    'period_to' => $next
                ]);
            }else{
                $quotes->update([
                    'status' => 'approve',
                    'period' => date('d M Y',strtotime($quotes->period_from)). ' to '.date('d M Y',strtotime($quotes->period_to))
                ]);
            }
//
            $premium_warranty = QuotesPremiumWarranty::where('quotes_id',$id)
                ->get();
            foreach($premium_warranty as $p){
                QuotesPremiumWarranty::find($p->id)
                    ->update([
                        'due_date' => date('d M Y', strtotime($p->due_date))
                    ]);
            }

            if(!Invoices::where('quotes_id',$id)->count()){
                $lastRef = Invoices::orderBy('id','DESC')
                    ->first();
                if($lastRef){
                    $splitter = (isset(explode("/",$lastRef->invoice_no)[3])) ? explode("/",$lastRef->invoice_no)[3] : 1;
                }else{
                    $splitter = "0000000";
                }


                $before = '';
                for($i = 0; $i<7 - count(str_split(intval($splitter))); $i++){
                    $before .= '0';
                }
                $newRef = 'inv/'.date('dmy').'/###/'.$before.(intval($splitter) + 1);
                $commRef = 'cn/'.date('dmy').'/###/'.$before.(intval($splitter) + 1);

                Invoices::create([
                    'quotes_id' => $id,
                    'invoice_no' => $newRef,
                    'comm_note_no' => $commRef
                ]);
            }
            return redirect('show_invoice/'.$id)
                ->with('success','Ok.');
        }
        return redirect('quotes_list')
            ->with('success','Status belum approve.');
    }

    public function sendCargoInvoice($id)
    {
        $quotes = Quotes::find($id);
        if($quotes->status == 'pending'){
            $quotes->update([
                'status' => 'granted',
                'period' => date('d M Y',strtotime($quotes->date_issued)). ' to '.date('d M Y', strtotime('+'.intval(filter_var($quotes->period, FILTER_SANITIZE_NUMBER_INT)).' month', strtotime($quotes->date_issued)))
            ]);
            $premium_warranty = QuotesPremiumWarranty::where('quotes_id',$id)
                ->get();
            foreach($premium_warranty as $p){
                QuotesPremiumWarranty::find($p->id)
                    ->update([
                        'due_date' => date('d M Y', strtotime('+'.intval(filter_var($p->due_date, FILTER_SANITIZE_NUMBER_INT)).' days', strtotime($quotes->date_issued)))
                    ]);
            }
            if(!Invoices::where('quotes_id',$id)->count()){
                $lastRef = Invoices::orderBy('id','DESC')
                    ->first()
                    ->invoice_no;
                $splitter = (isset(explode("/",$lastRef)[3])) ? explode("/",$lastRef)[3] : 1;

                $before = '';
                for($i = 0; $i<7 - count(str_split(intval($splitter))); $i++){
                    $before .= '0';
                }
                $newRef = 'inv/'.date('dmy').'/###/'.$before.(intval($splitter) + 1);

                Invoices::create([
                    'quotes_id' => $id,
                    'invoice_no' => $newRef
                ]);

            }
            return redirect('quotes_list')
                ->with('success','Status has changed');
        }
        return redirect('quotes_list')
            ->with('success','Status not change');

    }
    public function createPremium($id)
    {
        $vessels = DB::select("SELECT *,quotes_table_premium.id as premi_id FROM `quotes_table_premium` join vessel_company_details on vessel_company_details.id = quotes_table_premium.id_vessel WHERE quotes_table_premium.quotes_id = ".$id);
        return view('pib.quotes.quote_add_premium',compact('vessels'));
    }

    public static function getAdditionalPremium($id)
    {
        $data = DB::select("select * from quotes_table_premium_additional where premium_table_id = ".$id);
        return $data;
    }

    public function createCargoPremium($id)
    {
        $data = Quotes::find($id);
        return view('pib.quotes.quotes_add_cargo_premium',compact('id','data'));
    }

    public function updatePremium(Request $request)
    {
        $data = $request->data;
        parse_str($data,$array);
        QuotesPremium::find($array['id_premi'])
            ->update([
                'type_' => ($array['type_'][0]) ? $array['type_'][0] : "~",
                'premium_type' => ($array['premium_type'][0]) ? $array['premium_type'][0] : "~",
                'annual_premium' => ($array['annual_premium'][0]) ? $array['annual_premium'][0] : 0,
                'discount' => $array['discount'][0]
            ]);

        for($i = 1; $i<count($array['type_']); $i++){
            if(isset($array['add_id'][$i])){
                if(array_key_exists($i,$array['add_id'])){
                    QuotesAdditional::find($array['add_id'][$i])
                        ->update([
                            'type_' => (array_values($array['type_'])[$i]) ? array_values($array['type_'])[$i] : '~',
                            'premium_type' => (array_values($array['premium_type'])[$i]) ? array_values($array['premium_type'])[$i] : '~',
                            'annual_premium' => (array_values($array['annual_premium'])[$i]) ? array_values($array['annual_premium'])[$i] : 0,
                        ]);
                }
            }else{
                QuotesAdditional::create([
                    'type_' => (array_values($array['type_'])[$i]) ? array_values($array['type_'])[$i] : '~',
                    'premium_type' => (array_values($array['premium_type'])[$i]) ? array_values($array['premium_type'])[$i] : '~',
                    'annual_premium' => (array_values($array['annual_premium'])[$i]) ? array_values($array['annual_premium'])[$i] : 0,
                    'premium_table_id' => $array['id_premi']
                ]);
            }
        }
        return 'Data saved!';
    }

    public function updatePremiumOnchange(Request $request)
    {
        $subtotal_final = 0;
        $data = $request->data;
        parse_str($data,$array);
        $premi = QuotesPremium::find($array['id_premi']);
        $annual = ($array['annual_premium'][0]) ? doubleval($array['annual_premium'][0]) : 0;
        $premi->update([
                'type_' => ($array['type_'][0]) ? $array['type_'][0] : "~",
                'premium_type' => ($array['premium_type'][0]) ? $array['premium_type'][0] : "~",
                'annual_premium' => $annual ,
                'discount' => $array['discount'][0]
            ]);

        $subtotal_final += $annual - (($array['discount'][0]*$annual) /100);
        for($i = 1; $i<count($array['type_']); $i++){
            $annual_value = (array_values($array['annual_premium'])[$i]) ? doubleval(array_values($array['annual_premium'])[$i]) : 0;
            if(isset($array['add_id'][$i])){
                if(array_key_exists($i,$array['add_id'])){
                    QuotesAdditional::find($array['add_id'][$i])
                        ->update([
                            'type_' => (array_values($array['type_'])[$i]) ? array_values($array['type_'])[$i] : '~',
                            'premium_type' => (array_values($array['premium_type'])[$i]) ? array_values($array['premium_type'])[$i] : '~',
                            'annual_premium' => $annual_value,
                        ]);
                    $subtotal_final += $annual_value;
                }
            }else{
                QuotesAdditional::create([
                    'type_' => (array_values($array['type_'])[$i]) ? doubleval(array_values($array['type_'])[$i]) : '~',
                    'premium_type' => (array_values($array['premium_type'])[$i]) ? array_values($array['premium_type'])[$i] : '~',
                    'annual_premium' => $annual_value,
                    'premium_table_id' => $array['id_premi']
                ]);
                $subtotal_final += $annual_value;
            }
        }
        $total = 0;
        foreach($this->showPremi($premi->quotes_id) as $i => $v){
            $min = ($v->discount*$v->annual_premium) /100;
            $subtotal = $v->annual_premium - $min;
            $total += ( $subtotal + $this->sumAnnualPremium($v->premi_id));
            $this->updateTotalDue($v->quotes_id,$total);
        }
        return response()
            ->json(['total' => $total,'subtotal' => $subtotal_final]);
    }
    public function updateHMPremium(Request $request)
    {
        $data = $request->data;
        parse_str($data,$array);
        QuotesPremium::find($array['id_premi'])
            ->update([
                'annual_premium' => $array['annual_premium'][0],
                'sum_insured' => $array['sum_insured'][0]
            ]);
        Quotes::find($request->quote_id)
            ->update([
                'rate' => $request->rate
            ]);
        return 'Data saved!'.$request->rate;
    }

    public static function sumAnnualPremium($id)
    {
        $add = DB::select("SELECT sum(annual_premium) as add_total FROM `quotes_table_premium_additional` where premium_table_id = '$id'");
        if(isset($add[0]->add_total)){
            return $add[0]->add_total;
        }
        return 0;
    }

    public static function updateTotalDue($id,$total)
    {
        Quotes::find($id)
            ->update([
                'ammount_total' => $total
            ]);
    }
    public function updateCargoPremium(Request $request)
    {
        Quotes::find($request->quotes_id)
            ->update([
                'rate' => $request->rate,
                'policy_cost_stamp_duty' => $request->policy_cost_stamp_duty,
                'sum_insured' => $request->sum_insured,
                'ammount_total' => ($request->rate / 100) * $request->sum_insured
            ]);
        return redirect('show_premium_cargo/'.$request->quotes_id)
            ->with('success','Create Quotes Success');
    }

    public function deleteAdditionalPremium(Request $request)
    {
        QuotesAdditional::destroy($request->data);
    }

    public function deletePremiumWarranty(Request $request)
    {
        QuotesPremiumWarranty::destroy($request->id);
    }

    /** Assurance Quotes Company */

    public function createAssuranceCompanyQuotes($id)
    {
        $data = QuotesAssuranceCompany::where('quote_id',$id)->first();
        if($data){
            return view('pib.quotes.edit_assurance_company',compact('id','data'));
        }else{
            return view('pib.quotes.create_assurance_company',compact('id'));
        }
    }

    public function storeAssuranceCompanyQuotes(Request $request)
    {
        QuotesAssuranceCompany::create($request->post());
        $this->StoreOrUpdateIncome($request->quote_id);
        return redirect('quotes_list')
            ->with('success','Store assurance company : success');
    }

    public function updateAssuranceCompanyQuotes(Request $request)
    {
        QuotesAssuranceCompany::where('quote_id',$request->quote_id)
            ->update([
                'assurance_company_name' => $request->assurance_company_name,
                'premium_ammount' => $request->premium_ammount,
                'commision' => $request->commision
            ]);
        $this->StoreOrUpdateIncome($request->quote_id);
        return redirect('quotes_list')
            ->with('success','Update assurance company : success');
    }

    private function StoreOrUpdateIncome($id)
    {
        $quote = Quotes::find($id);
        $compare = QuotesAssuranceCompany::where('quote_id',$id)
            ->first();
        OurIncomes::updateOrCreate(
            [
                'quote_id' => $id
            ],
            [
                'margin_income' => $quote->ammount_total - $compare->premium_ammount,
                'commision_income' => ($compare->commision/100) * $quote->ammount_total,
                'total_income' => ($quote->ammount_total - $compare->premium_ammount) - (($compare->commision/100) * $quote->ammount_total)
            ]
        );
    }

    public function brokerage_count(Request $request)
    {
        //$array = [];
        //$ammount_total = 0;
        //$premium = QuotesPremium::where('quotes_id',$request->id)
        //    ->get();
        //foreach ($premium as $item) {
        //    array_push($array,$item->id);
        //    $ammount_total = $ammount_total + $item->annual_premium;
        //}
        //$array = join(",",$array);
        //$additional = DB::select("SELECT sum(annual_premium) as total FROM `quotes_table_premium_additional` WHERE premium_table_id in (".$array.")");
//
        //$add_total = ($additional[0]->total) ? $additional[0]->total : 0;
//
        //$ammount_total = $ammount_total + $add_total;
        $ammount = $request->ammount;
        eval("\$row = $ammount");
        return $row;
    }


    public function action_quote_new(Request $request)
    {
        Quotes::where('session_id',session()->getId())
            ->where('finish',0)
            ->update(['is_deleted' => 1]);

        return redirect('quotes_create');
    }

    public function deleteVPremium(Request $request)
    {
        QuotesPremium::destroy($request->data);
        QuotesVessels::where('id_vessel',$request->vessel);
    }
}

<?php

namespace App\Http\Controllers;

use App\QuoteItems;
use App\QuoteTypes;
use App\Types;
use Illuminate\Http\Request;

class TypeQuoController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin1');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Types::all();

        return view('pib.type_quotes.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = QuoteTypes::all();
        $items = QuoteItems::all();
        return view('pib.type_quotes.create',compact('types','items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $serialize = serialize($request->chosen_item);

        Types::updateOrCreate(
            ['id_type' => $request->id_type],
            ['form_items' => $serialize]
        );

        return redirect()
            ->route('type_quo.index')
            ->with('success','Success update.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Types::find($id);
        $items = QuoteItems::all();
        $form_items = unserialize($data->form_items);

        return view('pib.type_quotes.edit',compact('data','items','form_items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $serialize = serialize($request->chosen_item);

        Types::find($id)->update(
            ['form_items' => $serialize]
        );

        return redirect()
            ->route('type_quo.index')
            ->with('success','Success update.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Assured;
use App\Banks;
use App\ChannelGroup;
use App\Insurer;
use App\Quotes;
use App\QuotesCommision;
use App\QuotesInsurer;
use App\QuotesPremiumWarranty;
use App\QuotesVessels;
use App\QuoteTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SpecialController extends BaseController
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = QuoteTypes::all();
        $assured = Assured::all();
        $banks = Banks::all();
        $company = Insurer::all();
        return view('pib.special.create',compact('type','assured','banks','company'));
    }

    /**
     * @param Request $request
     * @return string
     */
    public function quoteRefCheck(Request $request)
    {
        $find = Quotes::where('our_ref',$request->ref)
            ->where('is_deleted',0)
            ->first();
        if($find){
            return "Ada quote dengan no. ref yang sama.";
        }
        return "No. ref quote belum terpakai.";
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $quote = Quotes::create([
            'quotation_status' => $request->quotation_status,
            'quote_type' => $request->quote_type,
            'our_ref' => $request->our_ref,
            'mortgage_bank' => $request->mortgage_bank,
            'security' => $request->security,
            'finish' => 1,
            'session_id' => session()->getId(),
            'created_by' => Auth::id()
        ]);

        if(isset($request->insurer) && count($request->insurer)){
            foreach($request->insurer as $item){
                QuotesInsurer::updateOrCreate(
                    [
                        'quote_id' => $quote->id,
                        'insurer_id' => $item
                    ],
                    [
                        'quote_id' => $quote->id,
                        'insurer_id' => $item
                    ]
                );
            }
        }else{
            QuotesInsurer::where('quote_id',$quote->id)
                ->delete();
        }
        if(isset($request->intermediary_name)){
            QuotesCommision::updateOrCreate(
                ['quote_id' => $quote->id],
                [
                    'quote_id' => $quote->id,
                    'intermediary_name' => $request->intermediary_name,
                    'brokerage' => $request->brokerage,
                    'wht' => $request->wht,
                    'vat' => $request->vat,
                    'policy_cost' => $request->policy_cost
                ]
            );
        }
        $inst = 100/intval($request->installment_count);
        for($i = 0; $i<$request->installment_count; $i++){
            QuotesPremiumWarranty::updateOrCreate(
                [
                    'instalment_number' => ($i+1),
                    'quotes_id' => $quote->id
                ],
                [
                    'instalment_number' => ($i+1),
                    'due_date' => '-',
                    'quotes_id' => $quote->id,
                    'value_' => $inst
                ]);
        }
        session(['direct_invoice' => true]);
        return redirect('quotation/premium/show_premium_quotes/'.$quote->id);
    }

    public function getInsurerDetail(Request $request)
    {
        $intermediary = "";
        $success = false;
        $message = "";
        $fix_link = [];
        if($request->value){
            foreach ($request->value as $item) {
                $insurer = Insurer::find($item);
                if($insurer->channel == 'Intermediary'){
                    if($insurer->channel_id){
                        $intermediary .= $insurer->Broker->name." ,";
                        $success = true;
                        $message = "";
                    }else{
                        $message = "Status insurer = intermediary, No broker!";
                        array_push($fix_link,route('insurer.edit',$item));
                    }
                }
            }
        }

        return response()
            ->json([
                'success' => $success,
                'message' => $message,
                'intermediary' => $intermediary,
                'fix_links' => $fix_link
            ]);
    }
}

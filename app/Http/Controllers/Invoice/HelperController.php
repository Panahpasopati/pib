<?php

namespace App\Http\Controllers\Invoice;

use App\Http\Controllers\BaseController;
use App\InvoiceCommissionNote;
use App\InvoiceCommissionNoteDetail;
use App\Invoices;
use App\QuotesCommision;
use App\QuotesPremium;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HelperController extends BaseController
{

    public static function makeCommissionNote($invoice)
    {
        $commission_note = InvoiceCommissionNote::updateOrCreate([
            'invoice_id' => $invoice->id
        ],[
            'invoice_id' => $invoice->id,
            'grand_total' => 0
        ]);

        $grand_total = 0;
        $premium = QuotesPremium::where('quotes_id',$invoice->quotes_id)
            ->get();
        $quoteCommission = QuotesCommision::where('quote_id',$invoice->quotes_id)
            ->first();
        foreach ($premium as $item) {
            $co_broke = $item->amount_total * ($quoteCommission->brokerage/100);
            $vat = $co_broke * ($quoteCommission->vat/100);
            $wht = $co_broke * ($quoteCommission->wht/100);
            $net_amount = ($co_broke + $vat) - $wht;
            InvoiceCommissionNoteDetail::updateOrCreate([
                'commission_note_id' => $commission_note->id,
                'premi_id' => $item->id
            ],[
                'commission_note_id' => $commission_note->id,
                'premi_id' => $item->id,
                'co_broke' => $co_broke,
                'vat' => $vat,
                'wht' => $wht,
                'net_amount' => $net_amount
            ]);
            $grand_total += $net_amount;
        }
        $commission_note->update([
            'grand_total' => $grand_total
        ]);
    }

    public function add_policy_ref($id)
    {
        $invoice = Invoices::findOrFail($id);
        $premium = QuotesPremium::where('quotes_id',$invoice->quotes_id)
            ->get();
        return view('pib.invoice.add_policy_ref',compact('premium','invoice'));
    }
}

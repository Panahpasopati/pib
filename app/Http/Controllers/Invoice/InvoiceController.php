<?php

namespace App\Http\Controllers\Invoice;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Quote\PremiumController;
use App\Installment;
use App\Invoices;
use App\PremiumAmount;
use App\Quotes;
use App\QuotesCommision;
use App\QuotesPremium;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class InvoiceController extends BaseController
{
    /**
     * InvoiceController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('user2');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Invoices::orderBy('date_issued','Desc')
            ->paginate(20);
        return view('pib.invoice.invoice_index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Quotes::find($request->quote_id)
            ->update([
                'status' => 'approve'
            ]);
        $invoice_no = ($request->invoice_no_custom) ? $request->invoice_no_custom : $request->invoice_no_running;
        $invoice = Invoices::updateOrCreate(
            ['quotes_id' => $request->quote_id,
            'invoice_no' => $invoice_no],
            [
                'quotes_id' => $request->quote_id,
                'invoice_no' => $invoice_no,
                'comm_note_no' => $request->comm_note_no,
                'period_from' => date('Y-m-d',strtotime($request->period_from)),
                'period_to' => date('Y-m-d',strtotime($request->period_to)),
                'installment_count' => count($request->installment_number),
                'is_prorata' => $request->is_prorata,
                'date_issued' => date('Y-m-d',strtotime($request->date_issued))
            ]);

        $amount_total = PremiumAmount::where('quote_id',$request->quote_id)
            ->first()
            ->amount_total;

        if($request->is_prorata){
            $date1=date_create($invoice->period_from);
            $date2=date_create($invoice->period_to);
            $diff=date_diff($date1,$date2);
            $prorate = $diff->format('%a');
            $amount_total = PremiumController::do_prorate_premium($invoice->quotes_id,$prorate);
        }

        if(QuotesCommision::where('quote_id',$request->quote_id)->first()){
            HelperController::makeCommissionNote($invoice);
        }

        foreach($request->installment_number as $i => $val){
            $amount = ($request->value_[$i]/100) * $amount_total;
            //$amount = $amount_total - $amount;
            Installment::updateOrCreate(
               [
                   'installment_no' => $val,
                   'invoice_id' => $invoice->id
               ],
                [
                    'invoice_id' => $invoice->id,
                    'installment_no' => $val,
                    'due_date' => date('Y-m-d',strtotime($request->due_date[$i])),
                    'amount' => $amount
                ]
            );
        }
        foreach($request->policy_ref as $x => $y){
            QuotesPremium::find($x)
                ->update([
                    'policy_ref' => $y
                ]);
        }
        return redirect()
            ->route('invoice.index')
            ->with('success','Invoice generated successfuly.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Invoices::findOrFail($id);
        $vessels = DB::select("SELECT *,quotes_table_premium.id as premi_id FROM `quotes_table_premium` join vessels on vessels.id = quotes_table_premium.id_vessel WHERE quotes_table_premium.quotes_id = ".$data->quotes_id);
        $discount = QuotesPremium::where('quotes_id',$data->quotes_id)
            ->where('discount','!=',0)
            ->count();
        $currency = $data->Quotes->currency;
        $commission = QuotesCommision::where('quote_id',$data->quotes_id)
            ->first();
        return view('pib.invoice.show_invoice',compact('data','discount','vessels','currency','commission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Invoices::findOrFail($id);
        $premium = QuotesPremium::where('quotes_id',$data->quotes_id)
            ->get();
        return view('pib.invoice.edit_invoice',compact('data','premium'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

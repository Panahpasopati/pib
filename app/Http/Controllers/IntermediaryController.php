<?php

namespace App\Http\Controllers;

use App\IntermediaryBrokers;
use App\IntermediaryFinderAssurance;
use App\IntermediaryFinders;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class IntermediaryController extends BaseController
{

    public function __construct()
    {
        $this->middleware('admin1');
    }

    public function brokerList()
    {
        $data = IntermediaryBrokers::paginate(10);
        return view('pib.intermediary_broker',compact('data'));
    }

    public function brokerCreate()
    {
        return view('pib.intermediary_broker_create');
    }

    public function brokerStore(Request $request)
    {
        IntermediaryBrokers::create($request->post());
        return redirect('intermediary_broker_list')
            ->with('success','Sukses menambahkan data broker.');
    }


    public function brokerEdit($id)
    {
        $data = IntermediaryBrokers::find($id);
        return view('pib.intermediary_broker_update',compact('data'));
    }

    public function brokerUpdate(Request $request)
    {
        $broker = IntermediaryBrokers::find($request->id);

        $broker->update($request->post());

        return redirect('intermediary_broker_list')
            ->with('success','Sukses mengupdate data broker');
    }

    public function brokerDelete($id)
    {
        IntermediaryBrokers::destroy($id);

        return redirect('intermediary_broker_list')
            ->with('success','Sukses menghapus data broker.');
    }

    public function finderList(Request $request)
    {
        $data = DB::select("SELECT GROUP_CONCAT(intermediary_finder_assured_table.id) as assured_id,GROUP_CONCAT(intermediary_finder_assured_table.assured_name) as assured,GROUP_CONCAT(intermediary_finder_assured_table.comm) as comm,GROUP_CONCAT(intermediary_finder_assured_table.remark) as remark,intermediary_finder_table.finder as finder,intermediary_finder_table.id as fid FROM `intermediary_finder_table`,intermediary_finder_assured_table where intermediary_finder_table.id = intermediary_finder_assured_table.finder_id  GROUP By intermediary_finder_table.finder order by intermediary_finder_table.id DESC");
        $data = $this->arrayPaginator($data, $request);

        return view('pib.intermediary_finder',compact('data'));


    }

    public function finderCreate()
    {
        return view('pib.intermediary_finder_create');
    }

    public function finderEdit($id)
    {

    }

    public function finderStore(Request $request)
    {
        IntermediaryFinders::create([
            'finder' => $request->finder
        ]);
        $select = IntermediaryFinders::orderBy('id','DESC')
            ->first();

        foreach($request->assured_name as $i => $val){
            IntermediaryFinderAssurance::create([
                'finder_id' => $select->id,
                'assured_name' => $val,
                'comm' => $request->comm[$i],
                'remark' => $request->remark[$i]
            ]);
        }

        return redirect('intermediary_finder_list')
            ->with('success','Sukses menambahkan data');
    }

    public function finderUpdate(Request $request)
    {

    }

    public function finderDelete($id)
    {
        IntermediaryFinders::destroy($id);
        IntermediaryFinderAssurance::where('finder_id',$id)
            ->delete();

        return redirect('intermediary_finder_list')
            ->with('success','Sukses delete data');
    }
    public function search(Request $request)
    {
        $identity = $_GET['identity'];
        $query = $_GET['query'];
        if($identity == 'intermediary_finder'){
            if($query != ''){
                $data = DB::select("SELECT GROUP_CONCAT(intermediary_finder_assured_table.id) as assured_id,GROUP_CONCAT(intermediary_finder_assured_table.assured_name) as assured,GROUP_CONCAT(intermediary_finder_assured_table.comm) as comm,GROUP_CONCAT(intermediary_finder_assured_table.remark) as remark,intermediary_finder_table.finder as finder,intermediary_finder_table.id as fid FROM `intermediary_finder_table`,intermediary_finder_assured_table where intermediary_finder_table.id = intermediary_finder_assured_table.finder_id AND intermediary_finder_table.finder like '%$query%' OR intermediary_finder_assured_table.assured_name like '%$query%' GROUP By intermediary_finder_table.finder");
            }else{
                $data = DB::select("SELECT GROUP_CONCAT(intermediary_finder_assured_table.id) as assured_id,GROUP_CONCAT(intermediary_finder_assured_table.assured_name) as assured,GROUP_CONCAT(intermediary_finder_assured_table.comm) as comm,GROUP_CONCAT(intermediary_finder_assured_table.remark) as remark,intermediary_finder_table.finder as finder,intermediary_finder_table.id as fid FROM `intermediary_finder_table`,intermediary_finder_assured_table where intermediary_finder_table.id = intermediary_finder_assured_table.finder_id GROUP By intermediary_finder_table.finder");
            }
            $data = $this->arrayPaginator($data, $request);
            return view('pib.intermediary_finder',compact('data','query'));
        }else{
            if($query != ''){
                $data = IntermediaryBrokers::where('broker','like','%'. $query . '%')->paginate(10);
            }else{
                $data = IntermediaryBrokers::paginate(10);
            }
            return view('pib.intermediary_broker',compact('data','query'));
        }
    }
    public function arrayPaginator($array, $request)
    {
        $page = Input::get('page', 1);
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $request->url(), 'query' => $request->query()]);
    }



}

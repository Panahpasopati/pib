<?php

namespace App\Http\Controllers\Voucher;

use App\Assured;
use App\Http\Controllers\BaseController;
use App\Installment;
use App\Invoices;
use App\ReceiptVoucher;
use App\User;
use Illuminate\Http\Request;
use NumberToWords\NumberToWords;


class VoucherController extends BaseController
{
    public function requestDetail(Request $request)
    {
        $invoice = Invoices::findOrFail($request->id);
        $installment = Installment::where('invoice_id',$invoice->id)
            ->where('is_paid',0)
            ->orderBy('installment_no','asc')
            ->get();
        $insurers = "";
        foreach($invoice->Quotes->Insurers as $insurer){
            $insurers .= $insurer->Insurer->insured.',';
        }
        $response = [
            'quotation' => [
                'id' => $invoice->Quotes->id,
                'insured' => $insurers,
                'insurer' => $invoice->Quotes->Security->assured_name,
                'insurer_id' => $invoice->Quotes->security,
                'vessels' => formatVesselsName($invoice->Quotes->Vessels),
            ],
            'invoice' => [
                'id' => $invoice->id,
                'invoice_no' => $invoice->invoice_no,
                'currency' => $invoice->Quotes->currency,
                'bank' => $invoice->Quotes->currency == 'USD' ? 'DBS' : 'Mandiri',
                'installment_count' => $invoice->installment_count
            ],
            'installment' => $installment
        ];
        return response()
            ->json([
                'success' => 1,
                'data' => $response
            ]);
    }

    public function requestDetailInstallment(Request $request)
    {
        $data = Installment::find($request->id);
        $numberToWords = new NumberToWords();
        $currencyTransformer = $numberToWords->getCurrencyTransformer('en');
        $str = $currencyTransformer->toWords($data->amount,'USD');
        return response()
            ->json([
                'success' => 1,
                'data' => [
                    'amount' => $data->amount,
                    'amount_formatted' => number_format($data->amount,2),
                    'due_date' => date('d-m-Y',strtotime($data->due_date)),
                    'to_words' => $str
                ]
            ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createPremiumPayment(Request $request)
    {
        session()->forget('premium_receipt_id');
        session()->forget('assured_id');
        session()->save();

        session(['premium_receipt_id' => $request->premium_receipt_id]);
        session(['assured_id' => $request->security]);

        return redirect()
            ->route('premium_payment_voucher.create');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function beforeCreatePV()
    {
        $assured = Assured::all();
        return view('pib.voucher.payment.before_create',compact('assured'));
    }


    public function getReceiptVoucher(Request $request)
    {
        $security = $request->security;
        $data = ReceiptVoucher::whereRaw("DATE(created_at) >= DATE('$request->pfrom') AND DATE(created_at) <= DATE('$request->pto')")
            ->whereHas('Quotes',function($query) use($security){
                $query->where('security',$security);
        })->orderBy('id','desc')
            ->get();

        $result = [];
        foreach ($data as $item) {
            if(!$item->PaymentVoucher){
                $result[] = [
                    'id_' => $item->id,
                    'voucher_no' => $item->voucher_no,
                    'created_at' => $item->created_at,
                    'received_date' => $item->received_date,
                    'amount' => number_format($item->amount),
                    'invoice' => [
                        'id_' => $item->invoice_id,
                        'invoice_no' => $item->Invoice->invoice_no,
                        'installment_count' => $item->Invoice->installment_count
                    ],
                    'installment' => [
                        'id_' => $item->installment_id,
                        'installment_no' => $item->Installment->installment_no,
                        'due_date' => $item->Installment->due_date
                    ],
                    'quote' => [
                        'id_' => $item->quote_id,
                        'insurers' => formatInsurersName($item->Quotes->insurers),
                        'vessels' => formatVesselsName($item->Quotes->Vessels)
                    ]
                ];
            }
        }
        return response()
            ->json([
                'success' => 1,
                'data' => $result
            ]);
    }

}

<?php

namespace App\Http\Controllers\Voucher;

use App\Assured;
use App\Http\Controllers\BaseController;
use App\PaymentVoucher;
use App\PaymentVoucherDetail;
use App\QuotesCommision;
use App\ReceiptVoucher;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentVoucherController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PaymentVoucher::paginate(30);
        return view('pib.voucher.payment.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(session('premium_receipt_id')){
            $data = ReceiptVoucher::whereIn('id',session('premium_receipt_id'));
            $gross = $data->sum('gross_premium');
            echo $gross;
            print_r(session('premium_receipt_id'));
            $data = $data->get();
            $users = User::all();
            $assured = Assured::findOrFail(session('assured_id'));
            $brokerage = 0;
            foreach ($data as $item) {
                $brokerage = $brokerage + (($item->Commision->brokerage/100) * $item->amount);
            }
            $lastRef = PaymentVoucher::orderBy('id','DESC')
                ->first();
            if($lastRef){
                $splitter = (isset(explode("/",$lastRef->voucher_no)[3])) ? explode("/",$lastRef->voucher_no)[3] : 1;
            }else{
                $splitter = "000";
            }


            $before = '';
            for($i = 0; $i<4 - count(str_split(intval($splitter))); $i++){
                $before .= '0';
            }
            $newRef = 'DBS-PP/'.date('ym').'/'.$before.(intval($splitter) + 1);
            return  view('pib.voucher.payment.create',compact('data','users','assured','gross','brokerage','newRef'));
        }
        return redirect('voucher/payment/before-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payment = PaymentVoucher::create([
            'assured_id' => session('assured_id'),
            'voucher_no' => $request->voucher_no,
            'amount' => $request->amount,
            'payment_date' => date('Y-m-d',strtotime($request->payment_date)),
            'bank' => $request->bank,
            'currency' => $request->currency,
            'inception_date' => $request->inception_date,
            'policy_no' => $request->policy_no,
            'gross_premium' => $request->gross_premium,
            'disc_from_insurer' => $request->disc_from_insurer,
            'brokerage' => $request->brokerage,
            'vat' => $request->vat,
            'wht' => $request->wht,
            'others' => $request->others,
            'bank_charge' => $request->bank_charge,
            'net_to_insurer' => $request->net_to_insurer,
            'accounting_entries' => $request->accounting_entries,
            'prepared_by' => $request->prepared_by,
            'approved_by' => $request->approved_by,
            'amount_in_words' => $request->amount_in_words
        ]);

        foreach ($request->receipt_id as $item) {
            PaymentVoucherDetail::updateOrCreate(
                [
                    'payment_voucher_id' => $payment->id,
                    'receipt_voucher_id' => $item
                ],
                [
                    'payment_voucher_id' => $payment->id,
                    'receipt_voucher_id' => $item
                ]
            );
        }

        return redirect()
            ->route('premium_payment_voucher.index')
            ->with('success','Premium payment created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = PaymentVoucher::findOrFail($id);
        return  view('pib.voucher.payment.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Voucher;

use App\Assured;
use App\Http\Controllers\BaseController;
use App\Installment;
use App\Invoices;
use App\ReceiptVoucher;
use App\User;
use Illuminate\Http\Request;

class ReceiptVoucherController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assured = Assured::all();
        $data = ReceiptVoucher::orderBy('id','desc');
        $paginate = false;
        if(isset($_GET['assured'])){
            $data = $data->whereHas('Quotes',function($query){
                $query->where('security',$_GET['assured']);
            });
        }
        if(isset($_GET['rows'])){
            $data = $data->limit($_GET['rows'])
                ->get();
        }else{
            $data = $data->paginate(30);
            $paginate = true;
        }
        return view('pib.voucher.receipt.index',compact('data','assured','paginate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $invoices = Invoices::orderBy('id','desc')
            ->limit(100)
            ->get();
        $lastRef = ReceiptVoucher::orderBy('id','DESC')
            ->first();
        if($lastRef){
            $splitter = (isset(explode("/",$lastRef->voucher_no)[3])) ? explode("/",$lastRef->voucher_no)[3] : 1;
        }else{
            $splitter = "000";
        }
        $before = '';
        for($i = 0; $i<4 - count(str_split(intval($splitter))); $i++){
            $before .= '0';
        }
        $newRef = 'DBS-PR/'.date('y').'/'.date('m').'/'.$before.(intval($splitter) + 1);
        $users = User::all();
        return view('pib.voucher.receipt.create',compact('invoices','users','newRef'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $receipt = ReceiptVoucher::create($request->all());
        $receipt->update([
            'received_date' => date('Y-m-d',strtotime($request->received_date))
        ]);
        Installment::find($request->installment_id)
            ->update([
                'is_paid' => 1
            ]);
        return redirect()
            ->route('premium_receipt_voucher.index')
            ->with('success','Receipt voucher successfully generated.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ReceiptVoucher::findOrFail($id);
        return view('pib.voucher.receipt.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

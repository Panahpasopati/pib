<?php

namespace App\Http\Controllers;

use App\Banks;
use Illuminate\Http\Request;

class BankController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin1');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Banks::where('is_deleted',0)
            ->orderBy('id','desc')
            ->paginate(20);

        return view('pib.banks.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pib.banks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Banks::create($request->all());

        return redirect()
            ->route('banks.index')
            ->with('success','Success insert new data.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Banks::find($id);
        return view('pib.banks.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Banks::find($id)
            ->update($request->all());

        return redirect()
            ->route('banks.index')
            ->with('success','data updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Banks::find($id)
            ->update(['is_deleted' => 1]);

        return redirect()
            ->route('banks.index')
            ->with('success','Data deleted');
    }
}

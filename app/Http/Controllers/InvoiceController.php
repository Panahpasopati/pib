<?php

namespace App\Http\Controllers;

use App\Invoices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('user2');
    }

    public function index()
    {
        $data = \App\Quotes::where('status','approve')
            ->where('is_deleted',0)
            ->orderBy('id','Desc')
            ->paginate(15);
        return view('pib.invoice.invoice_index',compact('data'));
    }

    public function editInvoice($id)
    {
        $data = Invoices::where('quotes_id',$id)
            ->first();
        return view('pib.invoice.edit_invoice',compact('data'));
    }

    public function showInvoice($id)
    {
        $data = Invoices::where('quotes_id',$id)
            ->first();
        $premium_warranty = DB::select("SELECT * FROM `quotes_table_premium_warranty` where quotes_id = ".$id);
        $vessels = DB::select("SELECT *,quotes_table_premium.id as premi_id FROM `quotes_table_premium` join vessel_company_details on vessel_company_details.id = quotes_table_premium.id_vessel WHERE quotes_table_premium.quotes_id = ".$id);

        return view('pib.invoice.show_invoice',compact('data','premium_warranty','vessels'));
    }

    public function showCargoInvoice($id)
    {
        $data = Invoices::where('quotes_id',$id)
            ->first();
        $premium_warranty = DB::select("SELECT * FROM `quotes_table_premium_warranty` where quotes_id = ".$id);
        $vessels = DB::select("SELECT *,quotes_table_premium.id as premi_id FROM `quotes_table_premium` join vessel_company_details on vessel_company_details.id = quotes_table_premium.id_vessel WHERE quotes_table_premium.quotes_id = ".$id);
        return view('pib.invoice.show_cargo_invoice',compact('data','premium_warranty','vessels'));
    }

    public function createEndorsement($id)
    {

        return view('pib.invoice.create_endorsment',compact('id'));
    }
}

<?php

namespace App\Http\Controllers\Quote;

use App\Http\Controllers\BaseController;
use App\Invoices;
use App\PremiumAmount;
use App\Quotes;
use App\QuotesAdditional;
use App\QuotesInsurer;
use App\QuotesPremium;
use App\QuotesPremiumWarranty;
use App\QuotesVessels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HelperController extends BaseController
{

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function publish($id)
    {
        Quotes::find($id)
            ->update([
                'finish' => 1
            ]);

        return redirect('quotes_list')
            ->with('success','publish new quote : success');
    }

    /**
     * @param Request $request
     * @return string
     */
    public function saveTemporaryQuote(Request $request)
    {
        parse_str($request->data,$data);
        $select = Quotes::find($request->id);
        $select->update($data);
        $select->update([
            'condition_' => $data['condition_temp'],
            'comment' => $data['comment_temp'],
            'deductibles' => $data['deductibles_temp'],
            'information_given' => $data['information_given_temp'],
            'survey_warranty' => $data['survey_warranty_temp'],
            'rate' => ($data['rate_temp']) ? $data['rate_temp'] : $select->rate,
            'warranty' => $data['warranty_temp'],
            'war_risk_etc' => $data['war_risk_etc_temp'],
            'subjectivities' => $data['subjectivities_temp'],
            'information_required' => join("|||",array_values($data['information_required_temp'])),
            'created_by' => Auth::id(),
            'status' => $select->status == 'approve' ? 'approve' : 'granted'
        ]);

        if(isset($data['insurer']) && count($data['insurer'])){
            foreach($data['insurer'] as $item){
                QuotesInsurer::updateOrCreate(
                    [
                    'quote_id' => $request->id,
                        'insurer_id' => $item
                    ],
                    [
                        'quote_id' => $request->id,
                        'insurer_id' => $item
                    ]
                );
            }
        }else{
            QuotesInsurer::where('quote_id',$request->id)
                ->delete();
        }


        return 'Data saved';
    }


    public function sendInvoice($id)
    {
        $invoice = Invoices::where('quotes_id',$id)
            ->first();
        if($invoice){
            return redirect()
                ->route('invoice.edit',$invoice->id);
        }
        $data = Quotes::where('is_deleted',0)
            ->where('finish',1)
            ->where('id',$id)
            ->firstOrFail();
        $lastRef = Invoices::orderBy('id','DESC')
            ->first();
        if($lastRef){
            $splitter = (isset(explode("/",$lastRef->invoice_no)[2])) ? explode("/",$lastRef->invoice_no)[2] : 1;
        }else{
            $splitter = "0000";
        }


        $before = '';
        for($i = 0; $i<4 - count(str_split(intval($splitter))); $i++){
            $before .= '0';
        }
        $newRef = 'DN/'.date('ym').'/'.$before.(intval($splitter) + 1);
        $commRef = 'cn/'.date('dmy').'/'.$before.(intval($splitter) + 1);
        $premium = QuotesPremium::where('quotes_id',$id)
            ->get();
        return view('pib.quotes.send_invoice',compact('data','newRef','commRef','premium'));
    }
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendInvoiceOld($id)
    {
        $quotes = Quotes::find($id);
        if($quotes->status == 'approve'){
            if($quotes->quotation_status == 'New'){
                $number = intval($quotes->period);
                $next = date('d M Y',strtotime('+'.$number.' month'));
                $quotes->update([
                    'status' => 'approve',
                    'period' => date('d M Y',strtotime($quotes->date_issued)). ' to '.$next,
                    'period_from' =>  date('d M Y',strtotime($quotes->date_issued)),
                    'period_to' => $next
                ]);
            }else{
                $quotes->update([
                    'status' => 'approve',
                    'period' => date('d M Y',strtotime($quotes->period_from)). ' to '.date('d M Y',strtotime($quotes->period_to))
                ]);
            }
//
            $premium_warranty = QuotesPremiumWarranty::where('quotes_id',$id)
                ->get();
            foreach($premium_warranty as $p){
                QuotesPremiumWarranty::find($p->id)
                    ->update([
                        'due_date' => date('d M Y', strtotime($p->due_date))
                    ]);
            }

            if(!Invoices::where('quotes_id',$id)->count()){
                $lastRef = Invoices::orderBy('id','DESC')
                    ->first();
                if($lastRef){
                    $splitter = (isset(explode("/",$lastRef->invoice_no)[3])) ? explode("/",$lastRef->invoice_no)[3] : 1;
                }else{
                    $splitter = "0000000";
                }


                $before = '';
                for($i = 0; $i<7 - count(str_split(intval($splitter))); $i++){
                    $before .= '0';
                }
                $newRef = 'inv/'.date('dmy').'/###/'.$before.(intval($splitter) + 1);
                $commRef = 'cn/'.date('dmy').'/###/'.$before.(intval($splitter) + 1);

                Invoices::create([
                    'quotes_id' => $id,
                    'invoice_no' => $newRef,
                    'comm_note_no' => $commRef
                ]);
            }
            return redirect('show_invoice/'.$id)
                ->with('success','Ok.');
        }
        return redirect('quotes_list')
            ->with('success','Status belum approve.');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function brokerage_count(Request $request)
    {
        $ammount = $request->ammount;
        eval("\$row = $ammount");
        return $row;
    }


    public function action_quote_new(Request $request)
    {
        $quote = Quotes::where('session_id',session()->getId())
            ->where('finish',0);
        $premium = QuotesPremium::where('quotes_id',$quote->first()->id);
        foreach ($premium->get() as $item) {
            QuotesAdditional::where('premium_table_id',$item->id)
                ->delete();
        }
        QuotesVessels::where('quotes_id',$quote->first()->id)->delete();
        PremiumAmount::where('quote_id',$quote->first()->id)->delete();
        $premium->delete();
        $quote->delete();

        return redirect()->route('quote.create');
    }

    public function deleteVPremium(Request $request)
    {
        \App\QuotesPremium::destroy($request->data);
        \App\QuotesVessels::where('id_vessel',$request->vessel);
    }

    public function quotesDraft()
    {
        $data = Quotes::where('finish',0)
            ->where('is_deleted',0)
            ->orderBy('id','Desc')
            ->paginate(15);
        return view('pib.quotes.draft',compact('data'));
    }
}

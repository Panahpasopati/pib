<?php

namespace App\Http\Controllers\Quote;

use App\Http\Controllers\BaseController;
use App\Insurer;
use App\PremiumAmount;
use App\Quotes;
use App\QuotesAdditional;
use App\QuotesCommision;
use App\QuotesPremium;
use App\QuotesPremiumWarranty;
use App\QuotesVessels;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use NumberToWords\NumberToWords;

class PremiumController extends BaseController
{
    public function createPremium($id)
    {
        $vessels = DB::select("SELECT *,quotes_table_premium.id as premi_id FROM `quotes_table_premium` join vessels on vessels.id = quotes_table_premium.id_vessel WHERE quotes_table_premium.quotes_id = ".$id);
        return view('pib.quotes.quote_add_premium',compact('vessels'));
    }

    public static function getAdditionalPremium($id)
    {
        $data = DB::select("select * from quotes_table_premium_additional where premium_table_id = ".$id);
        return $data;
    }

    public function updatePremium(Request $request)
    {
        $data = $request->data;
        parse_str($data,$array);
        QuotesPremium::find($array['id_premi'])
            ->update([
                'type_' => ($array['type_'][0]) ? $array['type_'][0] : "~",
                'premium_type' => ($array['premium_type'][0]) ? $array['premium_type'][0] : "~",
                'annual_premium' => ($array['annual_premium'][0]) ? $array['annual_premium'][0] : 0,
                'discount' => $array['discount'][0]
            ]);

        for($i = 1; $i<count($array['type_']); $i++){
            if(isset($array['add_id'][$i])){
                if(array_key_exists($i,$array['add_id'])){
                    QuotesAdditional::find($array['add_id'][$i])
                        ->update([
                            'type_' => (array_values($array['type_'])[$i]) ? array_values($array['type_'])[$i] : '~',
                            'premium_type' => (array_values($array['premium_type'])[$i]) ? array_values($array['premium_type'])[$i] : '~',
                            'annual_premium' => (array_values($array['annual_premium'])[$i]) ? array_values($array['annual_premium'])[$i] : 0,
                        ]);
                }
            }else{
                QuotesAdditional::create([
                    'type_' => (array_values($array['type_'])[$i]) ? array_values($array['type_'])[$i] : '~',
                    'premium_type' => (array_values($array['premium_type'])[$i]) ? array_values($array['premium_type'])[$i] : '~',
                    'annual_premium' => (array_values($array['annual_premium'])[$i]) ? array_values($array['annual_premium'])[$i] : 0,
                    'premium_table_id' => $array['id_premi']
                ]);
            }
        }
        return 'Data saved!';
    }

    public function CurrentPremi($id)
    {
        $vessels = DB::select("SELECT *,quotes_table_premium.id as premi_id FROM `quotes_table_premium` join vessels on vessels.id = quotes_table_premium.id_vessel WHERE quotes_table_premium.quotes_id = ".$id);
        return $vessels;
    }

    public function updatePremiumOnchange(Request $request)
    {
        $subtotal_final = 0;
        $data = $request->data;
        parse_str($data,$array);
        $premi = QuotesPremium::find($array['id_premi']);
        $annual = ($array['annual_premium'][0]) ? doubleval($array['annual_premium'][0]) : 0;
        $premi->update([
            'type_' => ($array['type_'][0]) ? $array['type_'][0] : "~",
            'premium_type' => ($array['premium_type'][0]) ? $array['premium_type'][0] : "~",
            'annual_premium' => $annual ,
            'discount' => doubleval($array['discount'][0])
        ]);

        $subtotal_final += $annual - ((doubleval($array['discount'][0])*$annual) /100);
        $adds = [];
        if(isset($array['add_type_'])){
            for($x = 0; $x < count($array['add_type_']); $x++){
                $annual_value = (array_values($array['add_annual_premium'])[$x]) ? doubleval(array_values($array['add_annual_premium'])[$x]) : 0;
                if(isset($array['add_id'][$x])){
                    QuotesAdditional::find($array['add_id'][$x])
                        ->update([
                            'type_' => (array_values($array['add_type_'])[$x]) ? array_values($array['add_type_'])[$x] : '~',
                            'premium_type' => (array_values($array['add_premium_type'])[$x]) ? array_values($array['add_premium_type'])[$x] : '~',
                            'annual_premium' => $annual_value,
                        ]);
                    $subtotal_final += $annual_value;
                    array_push($adds,$array['add_id'][$x]);
                }else{
                    $ids_add = QuotesAdditional::updateOrCreate(
                        [
                            'type_' => (array_values($array['add_type_'])[$x]) ? array_values($array['add_type_'])[$x] : '~',
                            'premium_type' => (array_values($array['add_premium_type'])[$x]) ? array_values($array['add_premium_type'])[$x] : '~',
                            'premium_table_id' => $array['id_premi']
                        ],
                        [
                            'type_' => (array_values($array['add_type_'])[$x]) ? array_values($array['add_type_'])[$x] : '~',
                            'premium_type' => (array_values($array['add_premium_type'])[$x]) ? array_values($array['add_premium_type'])[$x] : '~',
                            'annual_premium' => $annual_value,
                            'premium_table_id' => $array['id_premi']
                    ]);
                    array_push($adds,$ids_add->id);
                    $subtotal_final += $annual_value;
                }
            }
        }
        if(count($adds)){QuotesAdditional::where('premium_table_id',$array['id_premi'])->whereNotIn('id',$adds)->delete();}
        $premi->update([
            'amount_total' => $subtotal_final
        ]);
        $total = 0;
        foreach($this->CurrentPremi($premi->quotes_id) as $i => $v){
            $min = ($v->discount*$v->annual_premium) /100;
            $subtotal = $v->annual_premium - $min;
            $total += ( $subtotal + $this->sumAnnualPremium($v->premi_id));
        }
        $this->updateTotalDue($premi->quotes_id,$total);
        return response()
            ->json(['total' => $total,'subtotal' => $subtotal_final,'ids' => $adds]);
    }
    public function updateHMPremium(Request $request)
    {
        $data = $request->data;
        parse_str($data,$array);
        QuotesPremium::find($array['id_premi'])
            ->update([
                'annual_premium' => $array['annual_premium'][0],
                'sum_insured' => $array['sum_insured'][0]
            ]);
        Quotes::find($request->quote_id)
            ->update([
                'rate' => $request->rate
            ]);
        return 'Data saved!'.$request->rate;
    }

    public static function sumAnnualPremium($id)
    {
        $add = DB::select("SELECT sum(annual_premium) as add_total FROM `quotes_table_premium_additional` where premium_table_id = '$id'");
        if(isset($add[0]->add_total)){
            return $add[0]->add_total;
        }
        return 0;
    }

    public static function updateTotalDue($id,$total)
    {
        PremiumAmount::updateOrCreate(
            [
                'quote_id' => $id
            ],
            [
                'amount_total' => $total
            ]);
    }

    public function deleteAdditionalPremium(Request $request)
    {
        QuotesAdditional::destroy($request->data);
    }

    public function deletePremiumWarranty(Request $request)
    {
        QuotesPremiumWarranty::destroy($request->id);
    }

    public function showPremi($id)
    {
        $quoteVessels = [];
        $data = Quotes::findOrFail($id);
        $Vessels = QuotesVessels::select('id_vessel')->where('quotes_id',$data->id)->get();
        if($Vessels){
            foreach($Vessels as $vessel){
                array_push($quoteVessels,$vessel->id_vessel);
            }
        }

        return view('pib.quotes.premium.show_premi',compact('data','quoteVessels'));
    }

    public function showPremiIframe($id)
    {
        $vessels = DB::select("SELECT *,quotes_table_premium.id as premi_id FROM `quotes_table_premium` join vessels on vessels.id = quotes_table_premium.id_vessel WHERE quotes_table_premium.quotes_id = ".$id);
        if($vessels){
            return view('pib.quotes.premium.show_premi_iframe',compact('vessels'));
        }
    }
    public function showHMPremi($id)
    {
        $vessels = DB::select("SELECT *,quotes_table_premium.id as premi_id FROM `quotes_table_premium` join vessels on vessels.id = quotes_table_premium.id_vessel WHERE quotes_table_premium.quotes_id = ".$id);
        $quotes = Quotes::find($id);
        if($vessels){
            return view('pib.show_hm_premi',compact('vessels','quotes'));
        }
    }

    public function showCargoPremi($id)
    {
        $data = \App\Quotes::find($id);
        if($data){
            return view('pib.show_premi_cargo',compact('data','id'));
        }
    }
    public function deleteVPremium(Request $request)
    {
        QuotesPremium::destroy($request->data);
        QuotesVessels::where('id_vessel',$request->vessel)->delete();
    }

    public function submitQuotePremi(Request $request)
    {
        if(session('direct_invoice')){
            return redirect('quotation/send_invoice/'.$request->temp_id);
        }
        return redirect('quotation/premium/create_premium_warranty/'.$request->temp_id);
    }

    public function createPremiumWarranty($id)
    {
        $data = Quotes::findOrFail($id);
        $intermediary = "";
        $success = false;
        $message = "";
        $fix_link = [];
        if($data->Insurers){
            foreach ($data->Insurers as $item) {
                $insurer = Insurer::find($item->insurer_id);
                if($insurer->channel == 'Intermediary'){
                    if($insurer->channel_id){
                        $intermediary .= $insurer->Broker->name." ,";
                        $success = true;
                        $message = "";
                    }else{
                        $message = "Status insurer = intermediary, No broker!";
                        array_push($fix_link,route('insurer.edit',$item));
                    }
                }
            }
        }
        return view('pib.quotes.premium.installment',compact('data','intermediary','success','message','fix_link'));
    }

    public function submitPremiInstallment(Request $request)
    {
        foreach($request->instalment_number as $i => $value){
            QuotesPremiumWarranty::updateOrCreate(
                [
                    'instalment_number' => $value,
                    'quotes_id' => $request->temp_id
                ],
                [
                'instalment_number' => $value,
                'due_date' => $request->due_date[$i],
                'quotes_id' => $request->temp_id,
                'value_' => $request->value_[$i]
            ]);
        }
        QuotesCommision::updateOrCreate(
            ['quote_id' => $request->temp_id],
            [
                'quote_id' => $request->temp_id,
                'brokerage' => $request->brokerage
            ]
        );
        return redirect()
            ->route('quote.show',$request->temp_id)
            ->with('success','Update quote sukses.');
    }

    public static function do_prorate_premium($quote,$prorate)
    {
        $premium = QuotesPremium::where('quotes_id',$quote)
            ->get();
        $multi = $prorate/365;
        $total  = 0;
        foreach($premium as $item){
            $subtotal = 0;
            $annual = ($item->discount) ? ($item->annual_premium - ($item->annual_premium * ($item->discount/100))) : $item->annual_premium;
            $prorata_premium = round(($annual * $multi),2);
            $subtotal += $prorata_premium;
            if($item->Additional){
                foreach ($item->Additional as $x) {
                    $prorata_premium_add = round(($x->annual_premium*$multi),2);
                    $x->update([
                        'prorata_premium' => $prorata_premium_add
                    ]);
                    $subtotal += $prorata_premium_add;
                }
            }
            $item->update(['prorata_premium' => $prorata_premium,'amount_total' => $subtotal]);
            $total += $subtotal;
        }
        PremiumAmount::where('quote_id',$quote)
            ->update([
                'amount_prorata' => $total
            ]);
        return $total;
    }

}

<?php

namespace App\Http\Controllers\Quote;

use App\Assured;
use App\Banks;
use App\Http\Controllers\BaseController;
use App\Insurer;
use App\Quotes;
use App\QuotesInsurer;
use App\QuotesPremium;
use App\QuotesPremiumWarranty;
use App\QuotesVessels;
use App\QuoteTypes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuoteController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Quotes::where('finish',1)
            ->where('is_deleted',0)
            ->orderBy('id','Desc')
            ->paginate(15);
        $draft = Quotes::where('finish',0)
            ->where('is_deleted',0)
            ->count();
        return view('pib.quotes.quotes_index',compact('data','draft'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = QuoteTypes::all();
        $last = Quotes::orderBy('id','DESC')
            ->first();
        if(!$last){
            $lastRef = 'QTE/240818/PNI/00000000';
        }else{
            $lastRef = $last->our_ref;
        }

        $assured = Assured::all();
        $splitter = explode("/",$lastRef)[3];
        $before = '';
        for($i = 0; $i<7 - count(str_split(intval($splitter))); $i++){
            $before .= '0';
        }
        $banks = Banks::all();
        $newRef = 'QTE/'.date('dmy').'/PNI/'.$before.(intval($splitter) + 1);
        $check = Quotes::where('session_id',session()->getId())
            ->where('finish',0)
            ->where('is_deleted',0)
            ->orderBy('id','DESC')
            ->first();
        if($check){
            return redirect()->route('quote.edit',$check->id)
                ->with('resume',true);
        }
        Quotes::create([
            'our_ref' => $newRef,
            'created_by' => Auth::id(),
            'session_id' => session()->getId(),
            'finish' => 0,
            'is_draft' => 0
        ]);
        $temp = Quotes::where('our_ref',$newRef)
            ->first();
        $company = Insurer::all();
        session()->forget('direct_invoice');
        session()->save();
        return view('pib.quotes.quote_create',compact('type','array','company','newRef','banks','temp','assured'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $select = Quotes::find($request->temp_id);

        $select->update($request->post());

        $select->update([
            'condition_' => $request->condition_temp,
            'comment' => $request->comment_temp,
            'deductibles' => $request->deductibles_temp,
            'information_given' => $request->information_given_temp,
            'survey_warranty' => $request->survey_warranty_temp,
            'rate' => ($request->rate_temp) ? $request->rate_temp : $select->rate,
            'warranty' => $request->warranty_temp,
            'war_risk_etc' => $request->war_risk_etc_temp,
            'subjectivities' => $request->subjectivities_temp,
            'information_required' => join("|||",array_values($request->information_required_temp)),
            'created_by' => Auth::id(),
            'status' => $select->status == 'approve' ? 'approve' : 'granted'
        ]);

        if(isset($request->insurer) && count($request->insurer)){
            foreach($request->insurer as $item){
                QuotesInsurer::updateOrCreate(
                    [
                        'quote_id' => $request->temp_id,
                        'insurer_id' => $item
                    ],
                    [
                        'quote_id' => $request->temp_id,
                        'insurer_id' => $item
                    ]
                );
            }
        }else{
            QuotesInsurer::where('quote_id',$request->id)
                ->delete();
        }
        /**
        foreach($request->instalment_number as $i => $value){
            QuotesPremiumWarranty::create([
                'instalment_number' => $value,
                'due_date' => $request->due_date[$i],
                'quotes_id' => $request->temp_id,
                'value_' => $request->value_[$i]
            ]);
        }
         * **/
        session()->forget('direct_invoice');
        session()->save();
        return redirect('quotation/premium/show_premium_quotes/'.$select->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Quotes::find($id);
        $premium_warranty = DB::select("SELECT * FROM `quotes_table_premium_warranty` where quotes_id = ".$id);
        if($data->quote_type == 2){

            return view('pib.quotes.quote_cargo_show', compact('data','premium_warranty'));
        }else{
            $mortgage = DB::select("SELECT * FROM `quotes_table_vessels` join vessels on vessels.id = quotes_table_vessels.id_vessel join insurer on insurer.id = vessels.insurer_id WHERE quotes_table_vessels.quotes_id = ".$id);
            $premium_warranty = DB::select("SELECT * FROM `quotes_table_premium_warranty` where quotes_id = ".$id);
            $vessels = DB::select("SELECT *,quotes_table_premium.id as premi_id FROM `quotes_table_premium` join vessels on vessels.id = quotes_table_premium.id_vessel WHERE quotes_table_premium.quotes_id = ".$id);
            if($data->quote_type == 9){
                return view('pib.quotes.quote_hm_show',compact('data','mortgage','premium_warranty','vessels'));
            }else{
                return view('pib.quotes.quote_show',compact('data','mortgage','premium_warranty','vessels'));
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = QuoteTypes::all();
        $data = Quotes::find($id);
        $assured = Assured::all();
        $company = Insurer::all();
        $insurers = [];
        $data = Quotes::findOrFail($id);
        $Insurers = QuotesInsurer::select('insurer_id')->where('quote_id',$data->id)->get();
        if($Insurers){
            foreach($Insurers as $i){
                array_push($insurers,$i->insurer_id);
            }
        }
        $banks = Banks::all();
        return view('pib.quotes.quotes_edit',compact('data','id','type','array','company','insurers','banks','assured'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $select = Quotes::find($id);
        $select->update($request->post());
        $select->update([
            'condition_' => $request->condition_temp,
            'comment' => $request->comment_temp,
            'deductibles' => $request->deductibles_temp,
            'information_given' => $request->information_given_temp,
            'survey_warranty' => $request->survey_warranty_temp,
            'rate' => ($request->rate_temp) ? $request->rate_temp : $select->rate,
            'warranty' => $request->warranty_temp,
            'war_risk_etc' => $request->war_risk_etc_temp,
            'subjectivities' => $request->subjectivities_temp,
            'information_required' => join("|||",array_values($request->information_required_temp)),
            'created_by' => Auth::id(),
            'status' => $select->status == 'approve' ? 'approve' : 'granted',
            'finish' => 0
        ]);
        if(isset($request->insurer) && count($request->insurer)){
            foreach($request->insurer as $item){
                QuotesInsurer::updateOrCreate(
                    [
                        'quote_id' => $request->temp_id,
                        'insurer_id' => $item
                    ],
                    [
                        'quote_id' => $request->temp_id,
                        'insurer_id' => $item
                    ]
                );
            }
        }else{
            QuotesInsurer::where('quote_id',$request->id)
                ->delete();
        }
        session()->forget('direct_invoice');
        session()->save();
        return redirect('quotation/premium/show_premium_quotes/'.$id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

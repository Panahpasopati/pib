<?php

namespace App\Http\Controllers;

use App\Berita;
use App\Kanal;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }


}

<?php

namespace App\Http\Controllers;

use App\Invoices;
use App\PremiumAdditionalEndorsement;
use App\PremiumEndorsement;
use App\Quotes;
use App\VesselDetails;
use App\VesselEndorsement;
use App\Vessels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class EndorsementController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = VesselEndorsement::orderBy('id','desc')
            ->paginate(20);

        return view('pib.endorsement.index',compact('data'));
    }

    public function create()
    {
        $data = Invoices::all();
        return view('pib.endorsement.create',compact('data'));
    }
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewEndorsement($id)
    {
        $data = VesselEndorsement::where('id_vessel',$id)
            ->orderBy('created_at','desc')
            ->get();
        return view('pib.vessels.endorsment.view_endorsment',compact('data'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createEndorsement($id)
    {
        $data = Invoices::where('quotes_id',$id)
            ->first();
        $premium_warranty = DB::select("SELECT * FROM `quotes_table_premium_warranty` where quotes_id = ".$id);
        //$vessels = DB::select("SELECT *,vessel_details.id as v_id, quotes_table_premium.id as premi_id FROM `quotes_table_premium` join vessel_details on vessel_details.id = quotes_table_premium.id_vessel WHERE quotes_table_premium.quotes_id = ".$id);
        $vessels = DB::table('quotes_table_premium')
            ->select(DB::raw('*,quotes_table_premium.id as premi_id,vessel_details.id as v_id'))
            ->join('vessel_details','vessel_details.id','=','quotes_table_premium.id_vessel')
            ->where('quotes_table_premium.quotes_id','=',$id)
            ->paginate(10);
        return view('pib.vessels.endorsment.create_endorsment',compact('data','vessels'));
    }

    public function createVesselEndorsement($id)
    {
        $data = Invoices::where('quotes_id',$_GET['quote'])
            ->first();
        $detail = \App\VesselDetails::find($id);
        return view('pib.vessels.endorsment.create_detail_endorsement',compact('detail','data'));
    }
    public function storeEndorsement(Request $request)
    {
        $past = VesselDetails::find($request->id);
        $endorsement = "";
        $quote = Quotes::find($request->id_quote);
        foreach(explode(",",$request->selected_value) as $s){
            if($s == 'condition'){
                    $quote->update(['condition_' => $request->condition_]);
                $endorsement .= $s.' ('.$quote->$s .' changed to '.$quote->$s.'),';
            }elseif($s == 'assured'){
                $quote->update([
                    'assured' => $request->assured
                ]);
                $endorsement .= $s.' ('.$quote->$s .' changed to '.$request->$assured.'),';
            }else{
                $endorsement .= $s.' ('.$past->$s .' changed to '.$request->$s.'),';
            }
        }

        $filename = 'endorsement-doc-'.date('ymd').'.'.request()->document_endorsement->getClientOriginalExtension();
        Storage::putFileAs('public/endorsement', $request->file('document_endorsement'),$filename);
        VesselEndorsement::create([
            'id_vessel' => $request->id,
            'id_invoice' => $request->id_invoice,
            'id_quote' => $request->id_quote,
            'is_premi_change' => $request->premium_change == 'Yes' ? 1 : 0,
            'endorsement_value' => $endorsement,
            'changed' => '',
            'changed_to',
            'doc_endorsement' => $filename

        ]);
        VesselDetails::find($request->id)
            ->update($request->post());
        if($request->premium_change){
            PremiumEndorsement::firstOrCreate([
                'id_vessel' => $request->id,
                'quotes_id' => $request->id_quote
            ]);
            return redirect('premi_endorsement/'.$request->id_quote.'?vessel='.$request->id);
        }else{
            return redirect('invoices_list')
                ->with('success','Create Endorsement return : success');
        }
    }

    public function premiEndorsement($id)
    {
        $idv = $_GET['vessel'];
        $vessels = DB::select("SELECT *,quotes_table_premium_endorsement.id as premi_id FROM `quotes_table_premium_endorsement` join vessel_details on vessel_details.id = quotes_table_premium_endorsement.id_vessel WHERE vessel_details.id = $idv AND quotes_table_premium_endorsement.quotes_id = ".$id);
        return view('pib.vessels.endorsment.premi_endorsement',compact('vessels'));
    }

    public static function getAdditionalPremiumEndorsement($id)
    {
        $data = DB::select("select * from quotes_table_premium_additional_endorsement where premium_table_id = ".$id);
        return $data;
    }

    public static function sumAnnualPremium($id)
    {
        $add = DB::select("SELECT sum(annual_premium) as add_total FROM `quotes_table_premium_additional_endorsement` where premium_table_id = '$id'");
        if(isset($add[0]->add_total)){
            return $add[0]->add_total;
        }
        return 0;
    }

    public static function updateTotalDue($id,$total)
    {
        Quotes::find($id)
            ->update([
                'ammount_total' => $total
            ]);
    }

    public function updatePremium(Request $request)
    {
        $data = $request->data;
        parse_str($data,$array);
        PremiumEndorsement::find($array['id_premi'])
            ->update([
                'type_' => $array['type_'][0],
                'premium_type' => $array['premium_type'][0],
                'annual_premium' => $array['annual_premium'][0]
            ]);

        for($i = 1; $i<count($array['type_']); $i++){
            if(isset($array['add_id'][$i])){
                if(array_key_exists($i,$array['add_id'])){
                    PremiumAdditionalEndorsement::find($array['add_id'][$i])
                        ->update([
                            'type_' => array_values($array['type_'])[$i],
                            'premium_type' => array_values($array['premium_type'])[$i],
                            'annual_premium' => array_values($array['annual_premium'])[$i]
                        ]);
                }
            }else{
                PremiumAdditionalEndorsement::create([
                    'type_' => array_values($array['type_'])[$i],
                    'premium_type' => array_values($array['premium_type'])[$i],
                    'annual_premium' => array_values($array['annual_premium'])[$i],
                    'premium_table_id' => $array['id_premi']
                ]);
            }
        }
        return 'Data saved!';
    }
}

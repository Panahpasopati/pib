<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class Admin1
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return redirect()
                ->route('login');
        }
        $find = User::find(Auth::id());
        if($find->hak_akses != 'admin'){
            abort(404);
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class User1
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $find = \App\User::find(Auth::id());
        if($find->hak_akses != 'user1' AND $find->hak_akses != 'admin'){
            abort(404);
        }
        return $next($request);
    }
}

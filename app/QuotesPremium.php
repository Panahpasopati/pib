<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotesPremium extends Model
{
    protected $table = 'quotes_table_premium';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_vessel', 'type_', 'premium_type', 'annual_premium','discount', 'quotes_id','amount_total','is_endorement','sum_insured','prorata_premium','policy_ref'
    ];

    public function Additional()
    {
        return $this->hasMany('App\QuotesAdditional','premium_table_id');
    }

    public function Vessel()
    {
        return $this->hasOne('App\Vessels','id','id_vessel');
    }
}

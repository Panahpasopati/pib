<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentVoucher extends Model
{
    protected $table = 'payment_vouchers';
    protected $fillable = [
        'assured_id', 'voucher_no', 'amount', 'payment_date', 'bank', 'currency', 'inception_date', 'policy_no', 'gross_premium', 'disc_from_insurer', 'brokerage', 'vat', 'wht', 'others', 'bank_charge', 'net_to_insurer', 'accounting_entries', 'prepared_by', 'approved_by', 'is_approved', 'approve_date', 'amount_in_words'
    ];

    public function PaymentVoucherDetail()
    {
        return $this->hasMany('App\PaymentVoucherDetail','payment_voucher_id');
    }

    public function Security()
    {
        return $this->belongsTo('App\Assured','assured_id');
    }

    public function PreparedBy()
    {
        return $this->belongsTo('App\User','prepared_by');
    }

    public function ApprovedBy()
    {
        return $this->belongsTo('App\User','approved_by');
    }
}

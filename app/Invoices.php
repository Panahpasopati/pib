<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    protected $table ='invoice_table';
    protected $primaryKey = 'id';
    protected $fillable = [
        'quotes_id','invoice_no','invoice_file','comm_note_no','period_from','period_to','installment_count', 'is_prorata','date_issued'
    ];

    public function Quotes()
    {
        return $this->belongsTo('\App\Quotes','quotes_id');
    }

    public function CreditNote()
    {
        return $this->hasOne('App\CreditNote','invoice_id');
    }

    public function Installment()
    {
        return $this->hasMany('App\Installment','invoice_id');
    }

    public function CommissionNote()
    {
        return $this->hasOne('App\InvoiceCommissionNote','invoice_id');
    }


}

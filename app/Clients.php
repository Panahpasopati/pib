<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    protected $table = 'clients_table';
    protected $primaryKey = 'id';
    protected $fillable = [
        'client_name', 'alamat', 'telpon'
    ];

    protected function ClientInsurrance()
    {
        return $this->hasMany('\App\ClientInsurrances','id_client');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotesInsurer extends Model
{
    protected $table = 'quotes_insurer';
    protected $fillable = [
        'quote_id', 'insurer_id'
    ];

    public function Insurer()
    {
        return $this->belongsTo('App\Insurer','insurer_id');
    }
}

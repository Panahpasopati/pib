<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Types extends Model
{

    protected $table = 'form_items_quote';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_type', 'form_items'
    ];

    public function Type()
    {
        return $this->belongsTo('App\QuoteTypes','id_type');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientInsurrances extends Model
{
    protected $table = 'client_insurrances_table';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_client','insured_name', 'insured_address', 'telp', 'pic','email'
    ];

    public function Client()
    {
        return $this->belongsTo('\App\Clients','id_client');
    }
}

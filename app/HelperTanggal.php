<?php
/**
 * Created by PhpStorm.
 * User: Irman
 * Date: 3/26/2018
 * Time: 6:36 PM
 */

if (! function_exists('format_tgl')) {

    function format_tgl($input)
    {
        $bln = ['','Jan','Feb','Mar','April','Mei','Juni','Juli','Agu','Sep','Okt','Nov','Des'];
        $tgl = explode(" ",$input)[0];
        $split = explode("-",$tgl);

        return $split[2].' '.$bln[intval($split[1])].' '.$split[0];
    }
    function __add_mark_html_tag($content){
        $result_op = [];
        $result_li = [];
        $array = explode("<o:p></o:p>",$content);
        if(is_array($array) && count($array) > 1){
            foreach ($array as $item) {
                array_push($result_op,preg_replace("/&nbsp;/",'',htmlspecialchars_decode(strip_tags($item))));
            }
            //return $result;
        }
        $array = explode("<li",$content);
        if(is_array($array) && count($array) > 1){
            foreach ($array as $item) {
                array_push($result_li,strip_tags('<li '.preg_replace("/&nbsp;/",'',strip_tags($item))));
            }
        }
        if(count($result_li) > count($result_op)){
            return array_filter($result_li);
        }else{
            return $result_op;
        }
        return get_list_content($content);
    }
    function get_list_content($content){

        if(!$content){
            return false;
        }

        libxml_use_internal_errors(true);
        $doc = new DOMDocument();
        $doc->loadHTML($content);
        $tags = $doc->getElementsByTagName('li');
        $pushs = [];
        if(count($tags) > 1){
            foreach ($tags as $tag){
                array_push($pushs,$tag->nodeValue);
            }
        }
        return is_array($pushs) ? $pushs : $content;
    }

    function formatVesselsName($vessels){
        $count = $vessels->count();
        if($vessels->count() > 3){
            return $vessels[0]->VesselDetail->vessel_name. ' & '.($count-1).' others.';
        }
        $vessel_text = "";
        foreach($vessels as $vessel){
            $vessel_text .= $vessel->VesselDetail->vessel_name.', ';
        }
        return $vessel_text;
    }

    function formatInsurersName($insurers){
        $array = [];
        foreach($insurers as $insurer){
            array_push($array,$insurer->Insurer->insured);
        }
        return $array;
    }
}

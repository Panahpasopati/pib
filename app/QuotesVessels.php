<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotesVessels extends Model
{
    protected $table = 'quotes_table_vessels';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_vessel', 'quotes_id'
    ];

    public function VesselDetail()
    {
        return $this->belongsTo('App\Vessels','id_vessel');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banks extends Model
{
    protected $table = 'banks_table';
    protected $primaryKey = 'id';
    protected $fillable = [
        'bank_name','is_deleted'
    ];
}

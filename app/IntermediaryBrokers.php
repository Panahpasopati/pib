<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntermediaryBrokers extends Model
{
    protected $table = 'intermediary_broker_table';
    protected $primaryKey = 'id';
    protected $fillable = [
        'broker','brokerage',	'add_comm',	'invoicing', 'npwp', 'alamat'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotesCommision extends Model
{
    protected $table = 'quotes_commision';
    protected $fillable = [
        'quote_id','brokerage','intermediary_name', 'vat', 'wht', 'policy_cost'
    ];
    public function Quote()
    {
        return $this->belongsTo('\App\Quotes','quote_id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OurIncomes extends Model
{
    protected $table = 'our_incomes';
    protected $primaryKey = 'id';
    protected $fillable = [
        'quote_id', 'margin_income', 'commision_income', 'total_income'
    ];
}

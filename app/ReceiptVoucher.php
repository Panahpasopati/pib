<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptVoucher extends Model
{
    protected $table = 'receipt_vouchers';
    protected $primaryKey = 'id';
    protected $fillable = [
        'invoice_id', 'quote_id','voucher_no', 'installment_id', 'amount', 'received_date', 'bank', 'currency', 'policy_no','gross_premium', 'client_discount', 'third_party_commision', 'vat', 'wht', 'others', 'bank_charge', 'net_received','accounting_entries','approved_by','prepared_by','is_approved','approve_date','amount_in_words'
    ];

    public function Installment()
    {
        return $this->belongsTo('App\Installment','installment_id');
    }

    public function Quotes()
    {
        return $this->belongsTo('App\Quotes','quote_id');
    }

    public function Invoice()
    {
        return $this->belongsTo('App\Invoices','invoice_id');
    }

    public function PreparedBy()
    {
        return $this->belongsTo('App\User','prepared_by');
    }

    public function ApprovedBy()
    {
        return $this->belongsTo('App\User','approved_by');
    }

    public function PaymentVoucher()
    {
        return $this->hasOne('App\PaymentVoucherDetail','receipt_voucher_id');
    }

    public function Commision()
    {
        return $this->hasOne('App\QuotesCommision','quote_id','quote_id');
    }
}

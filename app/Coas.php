<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coas extends Model
{
    protected $table = 'coas_table';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code','description','is_deleted'
    ];
}

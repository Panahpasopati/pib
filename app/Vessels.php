<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vessels extends Model
{
    protected $table = 'vessels';
    protected $primaryKey = 'id';
    protected $fillable = [
        'vessel_name', 'vessel_type', 'built', 'gt', 'flag', 'class', 'crew', 'port_of_registry', 'cargo', 'trading', 'endorsment', 'term', 'insurer_id', 'status'
    ];

}

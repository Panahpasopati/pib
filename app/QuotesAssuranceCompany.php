<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotesAssuranceCompany extends Model
{
    protected $table = 'quotes_table_assurance_company';
    protected $primaryKey = 'id';
    protected $fillable = [
        'assurance_company_name', 'premium_ammount', 'commision', 'quote_id'
    ];
}

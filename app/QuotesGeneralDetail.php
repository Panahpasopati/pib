<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotesGeneralDetail extends Model
{
    protected $table = 'quotes_general_detail';
    protected $id = 'id';
    protected $fillable = [
        'id_quotes', 'insurer_name', 'part', 'as_', 'total_sum_insured', 'total_regulation_rate', 'total_premi'
    ];

    public function Premium()
    {
        return $this->hasOne('App\QuotesGeneralPremium','id_general_detail');
    }
}

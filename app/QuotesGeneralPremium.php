<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotesGeneralPremium extends Model
{
    protected $table = 'quotes_general_premium';
    protected $id = 'id';
    protected $fillable = [
        'id_general_detail', 'interest_insured', 'sum_insured', 'regulation_rate_name', 'regulation_rate_value', 'premi',
    ];

    public function Detail()
    {
        return $this->belongsTo('App\QuotesGeneralDetail','id_general_detail');
    }
}

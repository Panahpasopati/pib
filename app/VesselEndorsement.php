<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VesselEndorsement extends Model
{
    protected $table = 'vessel_endorsement';
    protected $primaryKet = 'id';
    protected $fillable = [
        'id_vessel', 'id_invoice', 'id_quote', 'is_premi_change', 'endorsement_value', 'created_at', 'updated_at', 'changed', 'changed_to', 'doc_endorsement'
    ];

    public function Vessel()
    {
        return $this->belongsTo('\App\VesselDetails','id_vessel');
    }

    public function Invoice()
    {
        return $this->belongsTo('App\Invoices','id_invoice');
    }

    public function Quote()
    {
        return $this->belongsTo('App\Quotes','id_quote');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntermediaryFinders extends Model
{
    protected $table = 'intermediary_finder_table';
    protected $primaryKey = 'id';
    protected $fillable = [
        'finder'
    ];

    public function Assurance()
    {
        return $this->hasMany('\App\IntermediaryFinderAssurance','finder_id');
    }
}

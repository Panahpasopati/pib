<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditNote extends Model
{
    protected $table = 'credit_notes';
    protected $fillable = [
        'credit_note_no','quote_id','premi_ids','cn_date','credit_days_count','amount_total','policy_cost'
    ];

    public function Quote()
    {
        return $this->belongsTo('App\Quotes','quote_id');
    }

    public function Details()
    {
        return $this->hasMany('App\CreditNoteDetail','credit_note_id');
    }
}

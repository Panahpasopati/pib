<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChannelGroup extends Model
{
    protected $table = 'channel_group';
    protected $fillable = [
        'name', 'address', 'phone', 'email', 'type_', 'status','npwp'
    ];
}

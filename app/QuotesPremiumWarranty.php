<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotesPremiumWarranty extends Model
{
    protected $table = 'quotes_table_premium_warranty';
    protected $primaryKey = 'id';
    protected $fillable = [
        'instalment_number', 'due_date', 'quotes_id','value_'
    ];
}

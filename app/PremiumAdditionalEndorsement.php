<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PremiumAdditionalEndorsement extends Model
{
    protected $table = 'quotes_table_premium_additional_endorsement';
    protected $primaryKey = 'id';
    protected $fillable = [
        'type_', 'premium_type', 'annual_premium', 'premium_table_id',
    ];
}

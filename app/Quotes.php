<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotes extends Model
{
    protected $table = 'quotes_table';
    protected $primaryKey = 'id';
    protected $fillable = [
        'quotation_status', 'quote_type', 'our_ref', 'date_issued', 'term_date', 'valid_until', 'address', 'sum_insured', 'join_assured', 'mortgage_bank', 'period', 'period_from', 'period_to', 'trading_limit', 'condition_', 'limit_of_cover', 'deductibles', 'warranties', 'general_conditions', 'warranty', 'survey_warranty', 'security', 'information_given', 'comment', 'disclaimer','status', 'agent', 'ammount_total', 'policy_cost_stamp_duty', 'occupation', 'finish', 'cover', 'session_id', 'is_draft', 'created_by', 'brokerage', 'brokerage_ammount', 'co_broke', 'co_broke_tax', 'finder_1_name', 'finder_1_value', 'finder_1_tax', 'finder_2_name', 'finder_2_value', 'finder_2_tax', 'co_broke_ammount', 'co_broke_tax_ammount', 'co_broke_tax_2', 'co_broke_tax_ammount_2', 'finder_1_ammount', 'finder_1_tax_ammount', 'finder_2_ammount', 'finder_2_tax_ammount', 'is_prorata', 'prorata_from', 'prorata_days', 'description', 'important_information', 'is_deleted', 'war_risk_etc', 'subjectivities', 'currency', 'information_required'
    ];

    public function QuoteType()
    {
        return $this->belongsTo('\App\QuoteTypes','quote_type');
    }

    public function Invoice()
    {
        return $this->hasOne('\App\Invoices','quotes_id');
    }

    public function Premium()
    {
        return $this->hasOne('App\QuotesPremium','quotes_id');
    }

    public function GeneralPremium()
    {
        return $this->hasOne('App\QuotesGeneralPremium','quotes_id');
    }

    public function Installment()
    {
        return $this->hasMany('App\QuotesPremiumWarranty','quotes_id');
    }

    public function CreatedBy()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function Security()
    {
        return $this->hasOne('App\Assured','id','security');
    }

    public function Vessels()
    {
        return $this->hasMany('App\QuotesVessels','quotes_id','id');
    }

    public function CreditNote()
    {
        return $this->hasOne('App\CreditNote','quote_id');
    }

    public function PremiumAmount()
    {
        return $this->hasOne('App\PremiumAmount','quote_id');
    }

    public function Document()
    {
        return $this->hasOne('App\QuoteDocument','id_quotes');
    }

    public function Insurers()
    {
        return $this->hasMany('App\QuotesInsurer','quote_id');
    }
}

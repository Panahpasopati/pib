<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteTypes extends Model
{
    protected $table = 'type_quotes_table';
    protected $primaryKey = 'id_type';
    protected $fillable = [
        'group_name','name','conditions','deductibles','acc_akun','gl_akun'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceCommissionNote extends Model
{
    protected $table = 'invoice_commission_note';
    protected $fillable = [
        'invoice_id', 'grand_total'
    ];

    public function CommissionDetail()
    {
        return $this->hasMany('App\InvoiceCommissionNoteDetail','commission_note_id');
    }
}

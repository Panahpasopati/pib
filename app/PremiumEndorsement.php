<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PremiumEndorsement extends Model
{
    protected $table = 'quotes_table_premium_endorsement';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_vessel', 'type_', 'premium_type', 'annual_premium', 'quotes_id','ammount_total'
    ];
}

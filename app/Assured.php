<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assured extends Model
{
    protected $table = 'assured';
    protected $primaryKey = 'id';
    protected $fillable = [
        'assured_name', 'npwp', 'address', 'phone', 'fax', 'email', 'description', 'website','status','brokerage'
    ];
}

/**
 * Created by Irman on 11/1/2019.
 */
$(function(){
    if($("#receipt_invoices_list").length){
        receiptInvoicesList();
    }
});

function receiptInvoicesList(){
    $("#receipt_invoices_list").change(function(){
        $.post(BASE+'/voucher/request_detail',{id:$(this).val()},function(data){
            if(data.success){
                var invoice = data.data.invoice,
                    quote = data.data.quotation;
                $('input[name="insured"]').val(quote.insured);
                $('input[name="insurer"]').val(quote.insurer);
                $('input[name="ship_name"]').val(""+quote.vessels+"");
                $('input[name="currency"]').val(invoice.currency);
                $('input[name="bank"]').val(invoice.bank);
                $('input[name="installment_count"]').val(invoice.installment_count);
                $('input[name="invoice_id"]').val(invoice.id);
                $('input[name="quote_id"]').val(quote.id);
                if(data.data.installment.length){
                    $('select[name="installment_id"]').empty();
                    $('select[name="installment_id"]').append('<option selected disabled>--Pilih installment--</option>')
                    $.each(data.data.installment,function(i,val){
                        $('select[name="installment_id"]').append('<option value="'+val.id+'">'+val.installment_no+'</option>')
                    });
                    $('select[name="installment_id"]').removeAttr('disabled')
                }
            }
        }).fail(function(){
            alert("error!");
        })
    })
}
$('select[name="installment_id"]').change(function(){
    $.post(BASE+'/voucher/request_detail_installment',{id:$(this).val()},function(data){
        $('input[name="due_date"]').val(data.data.due_date)
        $('input[name="gross_premium"]').val(data.data.amount)
        $('#gross_premium').val(data.data.amount_formatted)
        $('textarea[name="amount_in_words"]').val(data.data.to_words);
    })
});
$("#btn-payment-sort").click(function(){
    $(".th-sorting").toggle();
});
$('#select-all').click(function() {
    $('.checkbox-select').prop('checked', this.checked);
    $("#make_payment").show();
});
$('.checkbox-select').click(function(){
    if($('.checkbox-select').length == $('.checkbox-select:checked').length) {
        $("#select-all").prop("checked", "checked");
    } else {
        $("#select-all").removeAttr("checked");
    }
    $("#make_payment").show();
});

$("#make_payment").click(function(){
    $("#form-create-").submit();
})

/**
 * Created by Irman on 11/1/2019.
 */
var BASE = location.origin,
    IMPORT_EXPORT = 'http://192.168.1.23/pib_exp_imp/';
$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(".coa").select2({
        width:'100%',
        placeholder:'Select an Option'
    });
    $('.datepicker').datepicker({
        format: "mm/dd/yyyy"
    });
    $('input[type="text"]').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
})
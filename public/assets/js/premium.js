/**
 * Created by Irman on 11/6/2019.
 */
$(function(){
    $(".mortgage").select2({
        width:'100%',
        placeholder: 'select an option / multiple option'
    });
});
function changeVessels(id){
    $.ajax({
        type: 'post',
        url: BASE+'/quotation/request_vessels_detail',
        data: {items: $("#select-vessel-"+id).val(),temp_id:$("#temp_id").val()},
        success: function (data) {
            //$(".table-vessel").html(data);
            $('.premium_iframe')[0].contentWindow.location.reload(true);
        }
    });
}
function getVesselList() {
    var assured = $(".assured").val().split("-")[0];
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url: BASE+'/quotation/request_vessel_items',
        data: {id: assured,temp_id: $("#temp_id").val()},
        success: function (data) {
            $(".table-vessel").html("");
            $('.mortgage').empty();
            for (var i = 0; i < data.data.length; i++) {
                $('.mortgage').append('<option value=' + data.data[i].id + '>' + data.data[i].vessel_name + '</option>')
            }
            $(".address").val(data.address);
            $(".mortgage").removeAttr('disabled')
            $("#select-all").prop('checked',false)
            $('.premium_iframe')[0].contentWindow.location.reload(true);
        }
    })
}
function selectAllVessel(){
    if($("#select-all").is(":checked")){
        $(".mortgage > option").prop("selected","selected");
        $(".mortgage").trigger("change")
    }else{
        $(".mortgage > option").removeAttr('selected')
        $(".mortgage").trigger("change")
        $(".table-vessel").html('')
    }
}
function openModal(i,q){
    $(".add-vessel-button").html("");
    $("#myModal").modal('show');
    $('input[name="insurer_id"]').val(i);
    var html = '<button type="button" class="btn btn-default" onclick="saveMoreVessel()">Save</button><button class="btn btn-success" onclick="uploadWindow('+i+','+q+')">Upload File</button>';
    $(".add-vessel-button").append(html);
}
function uploadWindow(i,q){
    var strWindowFeatures = "location=yes,height=570,width=520,scrollbars=yes,status=yes";
    var URL = IMPORT_EXPORT+"import.php?id="+i+"&qid="+q;
    var win = window.open(URL, "_blank", strWindowFeatures);
    getVesselList();
}
function saveMoreVessel(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url: BASE+'/quotation/insert_new_vessel',
        data: {data:$("#form-add-new-vessel").serialize()},
        success: function (data) {
            //getVesselList();
            alert('Add new vessel success!');
            $("#myModal").modal('hide')
        }
    })
}
/**
 * Created by Irman on 4/25/2019.
 */
function chooseTab(evt, tab) {
    var i, tabcontent, tablinks;

    tabcontent = $(".panel-tabs");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = $(".tab-link");
    for (i = 1; i < 4; i++) {
        if('row-'+i == tab){
            $(".row-"+i).addClass('active');
        }else{
            $(".row-"+i).removeClass('active');
        }
        //tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    document.getElementById(tab).style.display = "block";
    //evt.currentTarget.className += " active";
}

function getVesselList() {
    var assured = $(".assured").val().split("-")[0];
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url: 'request_vessel_items',
        data: {id: assured,temp_id: $("#temp_id").val()},
        success: function (data) {
            $(".table-vessel").html("");
            $('.mortgage').empty();
            for (var i = 0; i < data.data.length; i++) {
                $('.mortgage').append('<option value=' + data.data[i].id + '>' + data.data[i].vessel_name + '</option')
            }
            $(".address").val(data.address);
            $(".mortgage").removeAttr('disabled')
            $("#select-all").prop('checked',false)
            $('.premium_iframe')[0].contentWindow.location.reload(true);
            $('#hm_iframe')[0].contentWindow.location.reload(true);
        }
    })
}
function openModal(){
    $(".add-vessel-button").html("");
    $("#myModal").modal('show')
    var html = '<button type="button" class="btn btn-default" onclick="saveMoreVessel()">Save</button><button class="btn btn-success" onclick="uploadWindow()">Upload File</button>';
    $(".add-vessel-button").append(html);
}
function uploadWindow(){
    if($(".assured").val()== ""){
        alert('Kolom assured masih kosong!');
    }
    var assured = $(".assured").val().split("-")[0];
    var strWindowFeatures = "location=yes,height=570,width=520,scrollbars=yes,status=yes";
    var URL = "{{ env('EXPORT_IMPORT_URL') }}import.php?id="+assured;
    var win = window.open(URL, "_blank", strWindowFeatures);
    getVesselList();
}
function saveMoreVessel(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url: 'insert_new_vessel',
        data: {
            vessel_name:$("#vessel_name").val(),vessel_type:$("#vessel_type").val(),built:$("#built").val(),gt:$("#gt").val(),
            flag:$("#flag").val(),class:$("#class").val(),crew:$("#crew").val(),port_of_registry:$("#port_of_registry").val(),vessel_id:$(".assured").val().split("-")[0]
        },
        success: function (data) {
            getVesselList();
            alert('Add new vessel success!');
            $("#myModal").modal('hide')
        }
    })
}
function selectAllVessel(){
    if($("#select-all").is(":checked")){
        $(".mortgage > option").prop("selected","selected");
        $(".mortgage").trigger("change")
    }else{
        $(".mortgage > option").removeAttr('selected')
        $(".mortgage").trigger("change")
        $(".table-vessel").html('')
    }
}

var i = 1;
function additem() {
    var itemlist = document.getElementById('itemlist');

    var row = document.createElement('tr');
    var b = document.createElement('td');
    var c = document.createElement('td');
    var d = document.createElement('td');
    var aksi = document.createElement('td');
    itemlist.appendChild(row);
    row.appendChild(b);
    row.appendChild(c);
    row.appendChild(d);
    row.appendChild(aksi);
    var insured_name = document.createElement('input');
    insured_name.setAttribute('name', 'instalment_number[' + i + ']');
    insured_name.setAttribute('class', 'form-control');
    insured_name.setAttribute('required', '');
//
    var insured_address = document.createElement('input');
    insured_address.setAttribute('name', 'due_date[' + i + ']');
    insured_address.setAttribute('class', 'form-control datepicker');
    insured_address.setAttribute('required', '');

    var insured_value_ = document.createElement('input');
    insured_value_.setAttribute('name', 'value_[' + i + ']');
    insured_value_.setAttribute('class', 'form-control');
    insured_value_.setAttribute('required', '');
//
//
    var hapus = document.createElement('span');
    b.appendChild(insured_name);
    c.appendChild(insured_address);
    d.appendChild(insured_value_);
    aksi.appendChild(hapus);
    hapus.innerHTML = '<button type="button" class="btn btn-small btn-default"><i class="fa fa-trash"></i></button>';
//                membuat aksi delete element
    hapus.onclick = function () {
        row.parentNode.removeChild(row);
    };


    i++;
}
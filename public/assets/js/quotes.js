/**
 * Created by Irman on 11/1/2019.
 */
$(document).ready(function(){
    getAssured();
    $('.summernote-text').summernote({
        height:150,
        width:'100%'
    });
    $('.note-btn-group.btn-group.note-style').remove();
    $('.note-btn-group.btn-group.note-font').remove();
    $('.note-fontname').remove();
    $('.note-table').remove();
    $('.note-insert').remove();
    $('.note-view').remove();
    $('.note-color').remove();
    $(".channel").change(function(){
        $.ajax({
            type: 'post',
            url: 'get_sales_or_marketing',
            data: {channel: $(".channel").val()},
            success: function (data) {
                $('.channel_id').empty();
                for (var i = 0; i < data.length; i++) {
                    $('.channel_id').append('<option value=' + data[i].id + '>' + data[i].name + '</option')
                }
                $(".channel_id").removeAttr('disabled')
            }
        })

    });
    $("#add_new_company").click(function(){
        $("#modal-company").modal('show')
    })
    $("#submit-new-company").click(function(){
        $.post('quotation/store_new_company_ajax',{data:$("#form-add-company").serialize()},function(data){
            if(data.status == 'sukses'){
                alert('sukses!');
                getAssured(data.data);
                $("#modal-company").modal('hide')
            }else{
                alert('failed!');
            }
        });
    })
});
function refreshPremiIframe(){
    $('.premium_iframe')[0].contentWindow.location.reload(true);
}
function getAssured(args){
    $(".assured").html('');
    $.get(BASE+'/quotation/get_company_assured',function(data){
        $.each(data.data,function(i,value){
            var opt = "";
            if(args){
                if(value.id == args.id){
                    opt = "selected";
                }
            }
            $(".assured").append('<option '+opt+' value="'+value.id+'">'+value.insured+'</option>');
        })
    });
}
$(".assured").select2({
    width:'100%',
    placeholder:'--choose multiple option--'
});
$(".coa").select2({
    width:'100%'
});
$(".banks").select2({
    width:'100%',
});
$(window).on('beforeunload',function(){
    return '';
});
$("#brokerage_count").click(function(){
    $.post('quotation.brokerage_count',{ammount:$("#brokerage_ammount").val(),value:$("#brokerage").val(),id:$("#temp_id").val()},function(data){
        $("#brokerage_ammount").val(data);
    })
});
$(".btn_save_to_draft").click(function(){
    $(".btn_save_to_draft").html("Loading....");
    $.post(BASE+"/quotation/save_temporary_quote",{data:$("#form-create-quote").serialize(),id:$("#temp_id").val()},function(data){
        setTimeout(function(){
            $(".btn_save_to_draft").html(data)
        },2000)
    }).fail(function(){
        setTimeout(function(){
            $(".btn_save_to_draft").html(data)
        },2000)
    });
    $(".btn_save_to_draft").html("Save to Draft")
});
$(".type_quote").change(function(){
    var val = $(".type_quote").val();
    //$('.premium_iframe')[0].contentWindow.location.reload(true);
    if(val == 2){
        $("#bsm_iframe").css('display','none');
    }else if(val == 9){
        $("#bsm_iframe").css('display','none');
    }else{
        $("#bsm_iframe").css('display','block');
    }
});
$(".quotation-status").change(function(){
    var val = $(".quotation-status").val();
    if(val == 'New'){
        $(".due_date_inst").remove();
        $("#due_date_inst").html('<input type="text" class="form-control due_date_inst" name="due_date[0]" required="">');
        $("#periode-new").prop('disabled',false);
        $(".periode-renew").prop('disabled',true)
    }else{
        $(".due_date_inst").remove();
        $("#due_date_inst").html('<input type="text" class="form-control datepicker due_date_inst" name="due_date[0]" required="">');
        $(".periode-renew").removeAttr('disabled');
        $("#periode-new").prop('disabled',true)
        $('.datepicker').datepicker({
            format: "mm/dd/yyyy"
        });
    }
});

$(".valid_until").change(function(){
    $.ajax({
        type: 'post',
        url: BASE+'/quotation/get_valid_until',
        data: {value: $(".date_issued").val(),i:$(".valid_until").val()},
        success: function (data) {
            $(".until").val(data)
        }
    })
})

function refreshInsurer(){getAssured();}

var i = 1;
function additem() {
    var itemlist = document.getElementById('itemlist');

    var row = document.createElement('tr');
    var b = document.createElement('td');
    var c = document.createElement('td');
    var d = document.createElement('td');
    var aksi = document.createElement('td');
    itemlist.appendChild(row);
    row.appendChild(b);
    row.appendChild(c);
    row.appendChild(d);
    row.appendChild(aksi);
    var insured_name = document.createElement('input');
    insured_name.setAttribute('name', 'instalment_number[' + i + ']');
    insured_name.setAttribute('class', 'form-control');
    insured_name.setAttribute('required', '');
//
    var val = $(".quotation-status").val(), due_inst = '';

    if(val != 'New'){
        //due_inst += 'datepicker'
    }
    var insured_address = document.createElement('input');
    insured_address.setAttribute('name', 'due_date[' + i + ']');
    insured_address.setAttribute('class', 'form-control due_inst '+due_inst);
    insured_address.setAttribute('required', '');

    var insured_value_ = document.createElement('input');
    insured_value_.setAttribute('name', 'value_[' + i + ']');
    insured_value_.setAttribute('class', 'form-control');
    insured_value_.setAttribute('required', '');
//
//
    var hapus = document.createElement('span');
    b.appendChild(insured_name);
    c.appendChild(insured_address);
    d.appendChild(insured_value_);
    aksi.appendChild(hapus);
    hapus.innerHTML = '<button type="button" class="btn btn-small btn-default"><i class="fa fa-trash"></i></button>';
//                membuat aksi delete element
    hapus.onclick = function () {
        row.parentNode.removeChild(row);
    };
    $('.datepicker').datepicker({
        format: "mm/dd/yyyy"
    });

    i++;
}

function checkQuoteRef(){
    var ref = $(".our-ref").val();
    if(ref == ""){
        alert('No. ref masih kosong!');
        return false;
    }
    $.post(BASE+'/special/quote-ref-check',{ref:$(".our-ref").val()},function(data){
        alert(data)
    })
}

$("#ins_direct_invoice").change(function(){
    var value = $(this).val();
    $.post(BASE+'/special/get_insurer_detail',{value:value},function(data){
        if(!data.success){
            $(".comm").val("");
            $(".comm").prop('disabled',true);
            var result_text = data.message;
            for(var x = 0; x<data.fix_links.length; x++){
                result_text += ' <a href="'+data.fix_links[x]+'" target="_blank">Fix</a> ';
            }
            $(".result-comm").html(result_text);
            return false;
        }
        $('input[name="intermediary_name"]').val(data.intermediary);
        $(".comm").removeAttr('disabled');
        $(".result-comm").html('');
    })
});
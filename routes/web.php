<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/','HomeController@index');
Route::get('reports','ReportController@index');


Route::prefix('quotation')->group(function(){
    Route::resource('quote','Quote\QuoteController');
    Route::post('save_temporary_quote','Quote\HelperController@saveTemporaryQuote');
    Route::post('brokerage_count','Quote\HelperController@brokerage_count');
    Route::post('new_quote_delete_old','Quote\HelperController@action_quote_new');
    Route::get('send_invoice/{id}','Quote\HelperController@sendInvoice');
    Route::get('quotes_draft','Quote\HelperController@quotesDraft');
    Route::post('quotes_publish/{id}',function(\Illuminate\Http\Request $request,$id){
        \App\Quotes::find($id)
            ->update([
                'finish' => 1
            ]);

        return redirect()->route('quote.index')
            ->with('success','publish new quote : success');
    });
    Route::post('save_draft_quote','AjaxController@saveDraft');
    Route::get('quotes_draft_delete/{id}',function($id){
        \App\Quotes::where('id',$id)->update(['is_deleted' => 1]);
        return redirect('quotation/quotes_draft')
            ->with('success','Quote deleted');
    });
    Route::post('get_prorata_days','AjaxController@getProrataDays');
    Route::post('is_approve_quote',function(\Illuminate\Http\Request $request){
        $stats = ['granted','approve'];
        \App\Quotes::find($request->id)
            ->update(['status' => $stats[$request->approve]]);
    });
    Route::get('get_company_assured',function(){
        $data = \App\Insurer::all();
        return response()
            ->json(['data' => $data]);
    });
    Route::post('get_valid_until','AjaxController@getValidUntil');
    Route::post('request_vessel_items','AjaxController@getVesselItems');
    Route::post('request_vessels_detail','AjaxController@getVesselsDetail');
    Route::post('insert_new_vessel','AjaxController@storeVessel');
    Route::prefix('premium')->group(function(){
        Route::get('show_premium_quotes/{id}','Quote\PremiumController@showPremi');
        Route::get('show_premium_quotes_iframe/{id}','Quote\PremiumController@showPremiIframe');
        Route::post('save','Quote\PremiumController@submitQuotePremi');
        Route::post('save_premium_warranty','Quote\PremiumController@submitPremiInstallment');
        Route::get('create_premium_quotes/{id}','Quote\PremiumController@createPremium');
        Route::get('create_premium_warranty/{id}','Quote\PremiumController@createPremiumWarranty');
        Route::post('update_premium_quotes','Quote\PremiumController@updatePremium');
        Route::post('update_premium_quotes_onchange','Quote\PremiumController@updatePremiumOnchange');
        Route::post('create_initial_additional_premium',function(\Illuminate\Http\Request $request){
            $a = \App\QuotesAdditional::create([
                'type_' => '',
                'premium_type' => '',
                'annual_premium' => 0,
                'premium_table_id' => $request->data
            ]);
            return response()
                ->json([
                    'success' => true,
                    'pid' => intval($a->premium_table_id),
                    'id' => $a->id
                ]);
        });
        Route::post('update_hm_premium_quotes','Quote\PremiumController@updateHMPremium');
        Route::post('delete_additional_premium','Quote\PremiumController@deleteAdditionalPremium');
        Route::post('delete_v_premium','Quote\PremiumController@deleteVPremium');
        Route::post('delete_premium_warranty','Quote\PremiumController@deletePremiumWarranty');
    });
});

Route::prefix('inv')->group(function(){
    Route::prefix('notes')->group(function(){
        Route::get('create_credit_note/{id}','CreditNoteController@create');
        Route::post('store_credit_note','CreditNoteController@store');
        Route::get('show/{id}','CreditNoteController@show');
        Route::post('change_credit_note_vessels','CreditNoteController@changeCN');
        Route::post('new_cn_delete_old/{id}','CreditNoteController@NewCNDeleteOld');
    });

    Route::resource('invoice','Invoice\InvoiceController');
    Route::get('create_endorsement/{id}','InvoiceController@createEndorsement');
    Route::post('get_past_endorsement','AjaxController@getVesselPastData');
    Route::post('store_vessel_endorsement','EndorsementController@storeEndorsement');

    Route::get('premi_endorsement/{id}','EndorsementController@premiEndorsement');
    Route::post('update_premium_endorsement','EndorsementController@updatePremium');

    Route::get('endorsements_list','EndorsementController@index');
    Route::get('new_endorsement','EndorsementController@create');
    Route::post('request_data_invoice','AjaxController@reqInvData');
});
/** Sys */
Route::prefix('sys')->group(function(){
    Route::resource('assured','AssuredController');
    Route::get('assured_search',function(){
        $data = \App\Assured::where('assured_name','like','%'.strip_tags($_GET['query']).'%')
            ->where('status',1)
            ->paginate(10);
        return view('pib.assured',compact('data'));
    });
    Route::resource('manage_user','UsersController');
    Route::resource('manage_type','TypeController');
    Route::resource('item_quo','ItemQuoController');
    Route::resource('type_quo','TypeQuoController');
    Route::resource('channel','ChannelController');
    Route::get('channel-search','ChannelController@search');
    Route::resource('coa','CoaController');
    Route::resource('currency','CurrencyController');
    Route::resource('banks','BankController');
    Route::resource('insurer','Insurer\InsurerController');
    Route::prefix('insurer')->group(function(){
        Route::get('vessels/search_rows','VesselController@search');
        Route::post('vessel_detail','Vessel\VesselController@vesselDetail');
        Route::get('vessel_endorsement_view/{id}','EndorsementController@viewEndorsement');
        Route::get('vessel_endorsement_create_detail/{id}','EndorsementController@createVesselEndorsement');
        Route::get('vessel_endorsement_create/{id}','EndorsementController@createEndorsement');
        Route::post('vessel_endorsement_store','EndorsementController@storeEndorsement');
        Route::view('store_endorsement_redirect','pib.vessels.Endorsement.redirect');
        Route::post('check_insured_name','AjaxController@check_insured_name');
    });
});

Route::prefix('voucher')->group(function(){
    Route::resource('premium_receipt_voucher','Voucher\ReceiptVoucherController');
    Route::resource('premium_payment_voucher','Voucher\PaymentVoucherController');
    Route::post('create_premium_payment','Voucher\VoucherController@createPremiumPayment');
    Route::post('request_detail','Voucher\VoucherController@requestDetail');
    Route::post('request_detail_installment','Voucher\VoucherController@requestDetailInstallment');
    Route::get('payment/before-create','Voucher\VoucherController@beforeCreatePV');
    Route::post('payment/get_receipt_voucher','Voucher\VoucherController@getReceiptVoucher');
});

/**
 * Special
 */
Route::prefix('special')->group(function(){
   Route::get('direct-invoice/create','SpecialController@create');
    Route::post('quote-ref-check','SpecialController@quoteRefCheck');
   Route::post('direct-invoice/store','SpecialController@store');
    Route::post('get_insurer_detail','SpecialController@getInsurerDetail');

    Route::get('direct-invoice/premium/{id}','SpecialController@premium');
    Route::get('direct-invoice/installment/{id}','SpecialController@installment');
    Route::post('direct-invoice/installment_go','SpecialController@installment_go');
});

/** Intermediary */
Route::get('intermediary_broker_list','IntermediaryController@brokerList');
Route::get('intermediary_broker_edit/{id}','IntermediaryController@brokerEdit');
Route::get('intermediary_broker_create','IntermediaryController@brokerCreate');
Route::post('intermediary_broker_store','IntermediaryController@brokerStore');
Route::post('intermediary_broker_update','IntermediaryController@brokerUpdate');
Route::get('intermediary_broker_delete/{id}','IntermediaryController@brokerDelete');

Route::get('intermediary_finder_list','IntermediaryController@finderList');
Route::get('intermediary_finder_edit/{id}','IntermediaryController@finderEdit');
Route::get('intermediary_finder_create','IntermediaryController@finderCreate');
Route::post('intermediary_finder_store','IntermediaryController@finderStore');
Route::post('intermediary_finder_update','IntermediaryController@finderUpdate');
Route::get('intermediary_finder_delete/{id}','IntermediaryController@finderDelete');
Route::get('intermediary_list/search_rows','IntermediaryController@search');

/** Company vessel */

Route::post('get_sales_or_marketing',function(\Illuminate\Http\Request $request){
    $data = \App\ChannelGroup::where('type_',$request->channel)
        ->where('status',1)
        ->get();

    return response()
        ->json($data);
});

Route::post('store_new_company_ajax','AjaxController@store_new_company_ajax');


/** Incomes */
Route::get('incomes_list','IncomesController@index');

/** Clients */
Route::get('clients_list','ClientsController@index');
Route::get('create_new_client','ClientsController@createClient');
Route::get('client_edit/{id}','ClientsController@editClient');
Route::post('store_new_client','ClientsController@storeClient');
Route::post('update_client/{id}','ClientsController@updateClient');
Route::post('request_client_insureds','ClientsController@requestClientInsured');
Route::post('clients_list/request_client_insureds','ClientsController@requestClientInsured');

Route::get('clients_list/search_rows','ClientsController@search');

/** Search */

/** Change Password */
Route::view('change-password','pib.change_pass');

Route::get('show_hm_premium_quotes/{id}','ContentController@showHMPremi');
Route::get('show_premium_cargo/{id}','ContentController@showCargoPremi');

Route::post('acc/get_accounting_menu',function(){
   $data = \App\QuoteTypes::whereNotIn('group_name',['QUO','INV'])
       ->get();

    return response()
        ->json($data);
});

